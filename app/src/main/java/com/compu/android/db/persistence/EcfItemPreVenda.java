package com.compu.android.db.persistence;

import java.io.Serializable;
import java.util.Date;

@Deprecated
public class EcfItemPreVenda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long 	IpvPrimary;
	private long	PveSeq;
	private long 	IpvSeq;
	private int ProSeq;
	private int EstSeq;
	private int IpvStatus;
	private double  IpvQtd;
	private double	IpvValUnit;
	private double	IpvValTot;
	private double	IpvValLiq;
	private double	IpvValUnitDigita;
	private double	IpvValDesc;
	private	double	IpvPerDesc;
	private double	IpvValAcres;
	private double	IpvPerAcres;
	private	Date	IpvDatMod;
	private String	IpvObs;
	private EcfItemPreVenda ecfItemPreVenda;
	
	public EcfItemPreVenda(){
		
	}
	
	public EcfItemPreVenda(long IpvPrimary, long PveSeq, long IpvSeq, int ProSeq, int EstSeq,
			int IpvStatus, double IpvQtd, double IpvValUnit, double IpvValtot,
			double IpvValLiq, double IpvvalUnitDigita, double IpvValDesc, double IpvPerDesc,
			double IpvValAcres, double IpvPerAcres, Date IpvDatMod, String IpvObs){
		this.IpvPrimary 		= IpvPrimary;
		this.PveSeq				= PveSeq;
		this.IpvSeq				= IpvSeq;
		this.ProSeq 			= ProSeq;
		this.EstSeq 			= EstSeq;
		this.IpvStatus 			= IpvStatus;
		this.IpvQtd 			= IpvQtd;
		this.IpvValUnit 		= IpvValUnit;
		this.IpvValTot 			= IpvValtot;
		this.IpvValLiq 			= IpvValLiq;
		this.IpvValUnitDigita 	= IpvvalUnitDigita;
		this.IpvValDesc 		= IpvValDesc;
		this.IpvPerDesc 		= IpvPerDesc;
		this.IpvValAcres 		= IpvValAcres;
		this.IpvPerAcres 		= IpvPerAcres;
		this.IpvDatMod 		  	= IpvDatMod;
		this.IpvObs 			= IpvObs;
		
	}
	
	public EcfItemPreVenda(EcfItemPreVenda ecfItemPreVenda){
		this.ecfItemPreVenda = ecfItemPreVenda;
	}
	
	public long getIpvPrimary() {
		return IpvPrimary;
	}
	public void setIpvPrimary(long ipvPrimary) {
		IpvPrimary = ipvPrimary;
	}
	public long getIpvSeq() {
		return IpvSeq;
	}
	public void setIpvSeq(long ipvSeq) {
		IpvSeq = ipvSeq;
	}
	public int getProSeq() {
		return ProSeq;
	}
	public void setProSeq(int proSeq) {
		ProSeq = proSeq;
	}
	public int getEstSeq() {
		return EstSeq;
	}
	public void setEstSeq(int estSeq) {
		EstSeq = estSeq;
	}
	public int getIpvStatus() {
		return IpvStatus;
	}
	public void setIpvStatus(int ipvStatus) {
		IpvStatus = ipvStatus;
	}
	public double getIpvQtd() {
		return IpvQtd;
	}
	public void setIpvQtd(double ipvQtd) {
		IpvQtd = ipvQtd;
	}
	public double getIpvValUnit() {
		return IpvValUnit;
	}
	public void setIpvValUnit(double ipvValUnit) {
		IpvValUnit = ipvValUnit;
	}
	public double getIpvValTot() {
		return IpvValTot;
	}
	public void setIpvValTot(double ipvValTot) {
		IpvValTot = ipvValTot;
	}
	public double getIpvValLiq() {
		return IpvValLiq;
	}
	public void setIpvValLiq(double ipvValLiq) {
		IpvValLiq = ipvValLiq;
	}
	public double getIpvValUnitDigita() {
		return IpvValUnitDigita;
	}
	public void setIpvValUnitDigita(double ipvValUnitDigita) {
		IpvValUnitDigita = ipvValUnitDigita;
	}
	public double getIpvValDesc() {
		return IpvValDesc;
	}
	public void setIpvValDesc(double ipvValDesc) {
		IpvValDesc = ipvValDesc;
	}
	public double getIpvPerDesc() {
		return IpvPerDesc;
	}
	public void setIpvPerDesc(double ipvPerDesc) {
		IpvPerDesc = ipvPerDesc;
	}
	public double getIpvValAcres() {
		return IpvValAcres;
	}
	public void setIpvValAcres(double ipvValAcres) {
		IpvValAcres = ipvValAcres;
	}
	public double getIpvPerAcres() {
		return IpvPerAcres;
	}
	public void setIpvPerAcres(double ipvPerAcres) {
		IpvPerAcres = ipvPerAcres;
	}
	public Date getIpvDatMod() {
		return IpvDatMod;
	}
	public void setIpvDatMod(Date ipvDatMod) {
		IpvDatMod = ipvDatMod;
	}
	public String getIpvObs() {
		return IpvObs;
	}
	public void setIpvObs(String ipvObs) {
		IpvObs = ipvObs;
	}

}
