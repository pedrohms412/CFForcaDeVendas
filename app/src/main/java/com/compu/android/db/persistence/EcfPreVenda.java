package com.compu.android.db.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

@Deprecated
public class EcfPreVenda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long 	PveSeq;
	private int TerSeq;
	private String  PveNum;
	private int PveStatus;
	private Date    PveDat;
	private Date	PveHorPrevEntrg;
	private double  PveQtdItens;
	private double	PveQtdProd;
	private int	PveTipoDesc;
	private double	PveValDesc;
	private int	PveTipoAcres;
	private double	PveValAcres;
	private double	PveValTot;
	private double	PveValLiq;
	private int	CliSeq;
	private int	FunSeqVen;
	private Date	PveDatMod;
	private	double	PveValSinal;
	private	int	EmpSeq;
	private int	PveIndEntrgMerc;
	private String	PveObs;
	private	int	PveIndCupomNF;
	private	long	PveNumSeq;
	private	int	ForSeqCorr;
	private int ForSeqTransp;
	private double	CliPerDescVenda;
	private Date	PveDatPrevEntrg;
	private int	PveTipQuitacao;
	private	int	SyncStatus;
	private HashSet<EcfItemPreVenda> ecfItemPreVenda;
	private int _Index_Item_ = 0;
	
	public EcfPreVenda(){
		this.SyncStatus = 0;
	}
	
	public EcfPreVenda(long PveSeq, int TerSeq, String PveNum, int PveStatus, Date PveDat,
			Date PveHorPrevEntrg, double PveQtdItens, double PveQtdProd, int PveTipoDesc,
			double PveValDesc, int PveTipoAcres, double PveValAcres, double PveValTot,
			double PveValLiq, int CliSeq, int FunSeqVen, Date PveDatMod, double PveValSinal,
			int EmpSeq, int PveIndEntrgMerc, String PveObs, int PveIndCupomNF, long PveNumSeq,
			int ForSeqCorr, int ForSeqTransp, double CliPerDescVenda, Date PveDatPrevEntrg,
			int PveTipQuitacao){
		
		this.PveSeq = PveSeq;
		this.TerSeq = TerSeq;
		this.PveNum = PveNum;
		this.PveStatus = PveStatus;
		this.PveDat = PveDat;
		this.PveHorPrevEntrg = PveHorPrevEntrg;
		this.PveQtdItens = PveQtdItens;
		this.PveQtdProd = PveQtdProd;
		this.PveTipoDesc = PveTipoDesc;
		this.PveValDesc  = PveValDesc;
		this.PveTipoAcres = PveTipoAcres;
		this.PveValTot = PveValTot;
		this.PveValLiq	= PveValLiq;
		this.CliSeq = CliSeq;
		this.FunSeqVen = FunSeqVen;
		this.PveDatMod = PveDatMod;
		this.PveValSinal = PveValSinal;
		this.EmpSeq = EmpSeq;
		this.PveIndEntrgMerc = PveIndEntrgMerc;
		this.PveObs = PveObs;
		this.PveIndCupomNF = PveIndCupomNF;
		this.PveNumSeq = PveNumSeq;
		this.ForSeqCorr = ForSeqCorr;
		this.ForSeqTransp = ForSeqTransp;
		this.CliPerDescVenda = CliPerDescVenda;
		this.PveDatPrevEntrg = PveDatPrevEntrg;
		this.PveTipQuitacao = PveTipQuitacao;	
		this.SyncStatus = 0;
	}
	
	
	public long getPveSeq() {
		return PveSeq;
	}
	public void setPveSeq(long pveSeq) {
		PveSeq = pveSeq;
	}
	public int getTerSeq() {
		return TerSeq;
	}
	public void setTerSeq(int terSeq) {
		TerSeq = terSeq;
	}
	public String getPveNum() {
		return PveNum;
	}
	public void setPveNum(String pveNum) {
		PveNum = pveNum;
	}
	public int getPveStatus() {
		return PveStatus;
	}
	public void setPveStatus(int pveStatus) {
		PveStatus = pveStatus;
	}
	public Date getPveDat() {
		return PveDat;
	}
	public void setPveDat(Date pveDat) {
		PveDat = pveDat;
	}
	public Date getPveHorPrevEntrg() {
		return PveHorPrevEntrg;
	}
	public void setPveHorPrevEntrg(Date pveHorPrevEntrg) {
		PveHorPrevEntrg = pveHorPrevEntrg;
	}
	public double getPveQtdItens() {
		return PveQtdItens;
	}
	public void setPveQtdItens(double pveQtdItens) {
		PveQtdItens = pveQtdItens;
	}
	public double getPveQtdProd() {
		return PveQtdProd;
	}
	public void setPveQtdProd(double pveQtdProd) {
		PveQtdProd = pveQtdProd;
	}
	public int getPveTipoDesc() {
		return PveTipoDesc;
	}
	public void setPveTipoDesc(int pveTipoDesc) {
		PveTipoDesc = pveTipoDesc;
	}
	public double getPveValDesc() {
		return PveValDesc;
	}
	public void setPveValDesc(double pveValDesc) {
		PveValDesc = pveValDesc;
	}
	public int getPveTipoAcres() {
		return PveTipoAcres;
	}
	public void setPveTipoAcres(int pveTipoAcres) {
		PveTipoAcres = pveTipoAcres;
	}
	public double getPveValAcres() {
		return PveValAcres;
	}
	public void setPveValAcres(double pveValAcres) {
		PveValAcres = pveValAcres;
	}
	public double getPveValTot() {
		return PveValTot;
	}
	public void setPveValTot(double pveValTot) {
		PveValTot = pveValTot;
	}
	public double getPveValLiq() {
		return PveValLiq;
	}
	public void setPveValLiq(double pveValLiq) {
		PveValLiq = pveValLiq;
	}
	public int getCliSeq() {
		return CliSeq;
	}
	public void setCliSeq(int cliSeq) {
		CliSeq = cliSeq;
	}
	public int getFunSeqVen() {
		return FunSeqVen;
	}
	public void setFunSeqVen(int funSeqVen) {
		FunSeqVen = funSeqVen;
	}
	public Date getPveDatMod() {
		return PveDatMod;
	}
	public void setPveDatMod(Date pveDatMod) {
		PveDatMod = pveDatMod;
	}
	public double getPveValSinal() {
		return PveValSinal;
	}
	public void setPveValSinal(double pveValSinal) {
		PveValSinal = pveValSinal;
	}
	public int getEmpSeq() {
		return EmpSeq;
	}
	public void setEmpSeq(int empSeq) {
		EmpSeq = empSeq;
	}
	public int getPveIndEntrgMerc() {
		return PveIndEntrgMerc;
	}
	public void setPveIndEntrgMerc(int pveIndEntrgMerc) {
		PveIndEntrgMerc = pveIndEntrgMerc;
	}
	public String getPveObs() {
		return PveObs;
	}
	public void setPveObs(String pveObs) {
		PveObs = pveObs;
	}
	public int getPveIndCupomNF() {
		return PveIndCupomNF;
	}
	public void setPveIndCupomNF(int pveIndCupomNF) {
		PveIndCupomNF = pveIndCupomNF;
	}
	public long getPveNumSeq() {
		return PveNumSeq;
	}
	public void setPveNumSeq(long pveNumSeq) {
		PveNumSeq = pveNumSeq;
	}
	public int getForSeqCorr() {
		return ForSeqCorr;
	}
	public void setForSeqCorr(int forSeqCorr) {
		ForSeqCorr = forSeqCorr;
	}
	public int getForSeqTransp() {
		return ForSeqTransp;
	}
	public void setForSeqTransp(int forSeqTransp) {
		ForSeqTransp = forSeqTransp;
	}
	public double getCliPerDescVenda() {
		return CliPerDescVenda;
	}
	public void setCliPerDescVenda(double cliPerDescVenda) {
		CliPerDescVenda = cliPerDescVenda;
	}
	public Date getPveDatPrevEntrg() {
		return PveDatPrevEntrg;
	}
	public void setPveDatPrevEntrg(Date pveDatPrevEntrg) {
		PveDatPrevEntrg = pveDatPrevEntrg;
	}
	public int getPveTipQuitacao() {
		return PveTipQuitacao;
	}
	public void setPveTipQuitacao(int pveTipQuitacao) {
		PveTipQuitacao = pveTipQuitacao;
	}
	public int getSyncStatus() {
		return SyncStatus;
	}
	public void setSyncStatus(int syncStatus) {
		SyncStatus = syncStatus;
	}

	public HashSet<EcfItemPreVenda> getEcfItemPreVenda() {
		return ecfItemPreVenda;
	}
	
	public int getIndexItem(){
		return _Index_Item_;
	}

	public void setEcfItemPreVenda(HashSet<EcfItemPreVenda> ecfItemPreVenda) {
		this.ecfItemPreVenda = ecfItemPreVenda;
	}
	
	public void addItemPreVenda(EcfItemPreVenda ecfItemPreVenda){
		if(this.ecfItemPreVenda == null)
			this.ecfItemPreVenda = new HashSet<EcfItemPreVenda>();			 
		this.ecfItemPreVenda.add(ecfItemPreVenda);
	}
	
	public void addItemPreVenda(long IpvPrimary, long PveSeq, long IpvSeq, int ProSeq, int EstSeq,
			int IpvStatus, double IpvQtd, double IpvValUnit, double IpvValtot,
			double IpvValLiq, double IpvvalUnitDigita, double IpvValDesc, double IpvPerDesc,
			double IpvValAcres, double IpvPerAcres, Date IpvDatMod, String IpvObs){
		
		if(this.ecfItemPreVenda == null)
			this.ecfItemPreVenda = new HashSet<EcfItemPreVenda>();
		EcfItemPreVenda itemPreVenda = new EcfItemPreVenda(IpvPrimary, PveSeq, IpvSeq, ProSeq, EstSeq,
				IpvStatus, IpvQtd, IpvValUnit, IpvValtot, IpvValLiq, IpvvalUnitDigita, IpvValDesc,
				IpvPerDesc, IpvValAcres, IpvPerAcres, IpvDatMod, IpvObs);
		ecfItemPreVenda.add(itemPreVenda);
		
	}
	
	public EcfItemPreVenda getItemByProSeq(int proSeq){
		if(ecfItemPreVenda == null)
			return null;
		Iterator<EcfItemPreVenda> iterator = ecfItemPreVenda.iterator();
		while(iterator.hasNext()){
			EcfItemPreVenda itemPreVenda = iterator.next();
			if(itemPreVenda.getProSeq() == proSeq){
				return iterator.next();
			}
		}
		return null;		
	}

}
