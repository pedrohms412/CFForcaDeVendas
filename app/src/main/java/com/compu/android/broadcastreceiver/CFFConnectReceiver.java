package com.compu.android.broadcastreceiver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import com.compu.android.application.CFFApplicationContext;
import com.compu.android.config.db.CFFDbConfig;
import com.compu.android.config.db.CFFDbPrePedido;
import com.compu.android.mail.SenderMail;
import com.compu.android.util.FuncoesAndroid;
import com.compu.db.CFFDataSet;
import com.compu.lib.exception.CFFClientException;
import com.compu.lib.funcoes;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.GmailScopes;

import android.accounts.AccountManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class CFFConnectReceiver extends GcmTaskService {
	
	String erroConnection = "Nao foi possivel conectar ao servidor de email";

	public static final String GCM_ONEOFF_TAG = "oneoff|[0,0]";
	public static final String GCM_REPEAT_TAG = "repeat|[7200,1800]";


	@Override
	public int onRunTask(TaskParams taskParams) {

		Handler h = new Handler(getMainLooper());

		if(taskParams.getTag().equals(GCM_ONEOFF_TAG)){
			h.post(new Runnable() {
				@Override
				public void run() {
					EnviaPedido();
					Log.d( funcoes.TAG, "ONEOFF executed");
				}
			});
		} else if(taskParams.getTag().equals(GCM_REPEAT_TAG)) {
			h.post(new Runnable() {
				@Override
				public void run() {
					EnviaPedido();
					Log.d( funcoes.TAG, "REPEATING executed");
				}
			});
		}

		return GcmNetworkManager.RESULT_SUCCESS;
	}

	@Override
	public int onStartCommand(Intent intent, int i, int i1) {
		return super.onStartCommand(intent, i, i1);
	}

	private CFFApplicationContext getApp(){
		return (CFFApplicationContext)getApplication();
	}

	public Integer EnviaPedido() {

		if(!FuncoesAndroid.isDeviceOnline(getApplicationContext())) return null;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));

		boolean enviaemail = false;

		Cursor cParametro = getApp().getDbPrePedido().rawQuery("SELECT * FROM PARAMETRO_EMPRESA where PEM_NOM_PARAM = \'EMP_PAR_11701_UTILIZAR_ENVIO_AUTOMATICO\'", null);

		if(cParametro.getCount() > 0){
			cParametro.moveToFirst();
			if(funcoes.getStringValue(cParametro.getString(CFFDbConfig.Companion.getPARAMETRO_FIELD_PEM_VAL_PARAM())).toUpperCase().equals("SIM"))
				enviaemail = true;
		}

		try {
			CFFDataSet dsParametro = getApp().getAndroidSocket().getQuery(
					String.format(new Locale("pt", "BR"),
							"SELECT * FROM ECF_PARAMETRO_EMPRESA WHERE PEM_NOM_PARAM = \'EMP_PAR_11701_UTILIZAR_ENVIO_AUTOMATICO\' AND EMP_SEQ = %d",
							getApp().getCurrentConfig().getInt(CFFDbConfig.Companion.getCONFIG_FIELD_EMPRESA())), 1);

			if (dsParametro.getCount() > 0) {

				if (cParametro.getCount() == 0) {

					ContentValues values = new ContentValues();
					values.put("EMP_SEQ", dsParametro.getInt("EMP_SEQ"));
					values.put("PEM_SEQ", dsParametro.getInt("PEM_SEQ"));
					values.put("PEM_NOM_PARAM", dsParametro.getString("PEM_NOM_PARAM"));
					values.put("PEM_VAL_PARAM", dsParametro.getString("PEM_VAL_PARAM"));
					values.put("DAT_MOD", sdf.format(new Date()));

					getApp().getDbPrePedido().insert(CFFDbConfig.Companion.getTABLE_PARAMETRO(), null, values);

				} else {
					ContentValues values = new ContentValues();
					values.put("PEM_VAL_PARAM", dsParametro.getString("PEM_VAL_PARAM"));
					values.put("DAT_MOD", sdf.format(new Date()));
					getApp().getDbPrePedido().update(CFFDbConfig.Companion.getTABLE_PARAMETRO(), values,
							String.format(new Locale("pt", "BR"),
									"PEM_SEQ = %d",
									dsParametro.getInt("PEM_SEQ")),
							null);
				}

				if(funcoes.getStringValue(dsParametro.getString("PEM_VAL_PARAM")).toUpperCase().equals("SIM"))
					enviaemail = true;

			}
		} catch (CFFClientException ex) {
			return null;
		}

		if(enviaemail) {

			SharedPreferences prefs = getSharedPreferences(AccountManager.KEY_ACCOUNT_NAME, MODE_PRIVATE);

			if (!prefs.getString(AccountManager.KEY_ACCOUNT_NAME, "").equals("")) {
				String[] SCOPES = new String[]{GmailScopes.GMAIL_SEND};
				GoogleAccountCredential mCredential = GoogleAccountCredential.usingOAuth2(
						getApplicationContext(), Arrays.asList(SCOPES))
						.setBackOff(new ExponentialBackOff());

				mCredential.setSelectedAccountName(prefs.getString(AccountManager.KEY_ACCOUNT_NAME, ""));

				try {
					HttpTransport transport = AndroidHttp.newCompatibleTransport();
					JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
					com.google.api.services.gmail.Gmail mService = new com.google.api.services.gmail.Gmail.Builder(
							transport, jsonFactory, mCredential)
							.setApplicationName("com.compu.android.forcadevendas")
							.build();

					Properties props = new Properties();

					Session session = Session.getDefaultInstance(props, null);

					String emailDestino = "";

					cParametro = getApp().getDbPrePedido().rawQuery("SELECT * FROM PARAMETRO_EMPRESA where PEM_NOM_PARAM = \'EMP_PAR_11711_EMAIL_DESTINATARIO\'", null);
					if (cParametro.getCount() > 0) {
						cParametro.moveToFirst();
						emailDestino = cParametro.getString(CFFDbConfig.Companion.getPARAMETRO_FIELD_PEM_VAL_PARAM());
					}
					try {
						CFFDataSet dsParametro = getApp().getAndroidSocket().getQuery(
								String.format(new Locale("pt", "BR"),
										"SELECT * FROM ECF_PARAMETRO_EMPRESA WHERE PEM_NOM_PARAM = \'EMP_PAR_11711_EMAIL_DESTINATARIO\' AND EMP_SEQ = %d",
										getApp().getCurrentConfig().getInt(CFFDbConfig.Companion.getCONFIG_FIELD_EMPRESA())), 1);

						if (dsParametro.getCount() > 0) {

							if (cParametro.getCount() == 0) {

								ContentValues values = new ContentValues();
								values.put("EMP_SEQ", dsParametro.getInt("EMP_SEQ"));
								values.put("PEM_SEQ", dsParametro.getInt("PEM_SEQ"));
								values.put("PEM_NOM_PARAM", dsParametro.getString("PEM_NOM_PARAM"));
								values.put("PEM_VAL_PARAM", dsParametro.getString("PEM_VAL_PARAM"));
								values.put("DAT_MOD", sdf.format(new Date()));

								getApp().getDbPrePedido().insert(CFFDbConfig.Companion.getTABLE_PARAMETRO(), null, values);

							} else {
								ContentValues values = new ContentValues();
								values.put("PEM_VAL_PARAM", dsParametro.getString("PEM_VAL_PARAM"));
								values.put("DAT_MOD", sdf.format(new Date()));
								getApp().getDbPrePedido().update(CFFDbConfig.Companion.getTABLE_PARAMETRO(), values,
										String.format(new Locale("pt", "BR"),
												"PEM_SEQ = %d",
												dsParametro.getInt("PEM_SEQ")),
										null);
							}

							emailDestino = dsParametro.getString("PEM_VAL_PARAM");

							if (emailDestino.equals(""))
								return null;
						}
					} catch (CFFClientException ex) {
						return null;
					}


					if (!emailDestino.equals("")) {
						Cursor cPreVendaEnvio = getApp().getDbPrePedido().rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_EMAIL_ENVIADO = 0", null);
						int emailsEnviados = 0;
						if(cPreVendaEnvio.getCount() > 0) {

							while(cPreVendaEnvio.moveToNext()) {

								MimeMessage mimeMessage = new MimeMessage(session);
								mimeMessage.setFrom(prefs.getString(AccountManager.KEY_ACCOUNT_NAME, ""));
								mimeMessage.addRecipients(javax.mail.Message.RecipientType.TO, emailDestino);
								mimeMessage.setSubject(String.format(new Locale("pt", "BR"),
										"Pedido Móvel - Vendedor %s ",
										getApp().getCurrentConfig().getString(CFFDbConfig.Companion.getCONFIG_FIELD_NOM_FUNCIONARIO())));

								String bodyMessage = "";


								bodyMessage += String.format(new Locale("pt", "BR"), "Informacoes do pedido de Vendedor %s \n\n", getApp().getCurrentConfig().getString(CFFDbConfig.Companion.getCONFIG_FIELD_NOM_FUNCIONARIO()));
								bodyMessage += String.format(new Locale("pt", "BR"), "Data/Hora Pedido: %s \n", cPreVendaEnvio.getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_DAT()));
								bodyMessage += String.format(new Locale("pt", "BR"), "Cliente: %s \n", cPreVendaEnvio.getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_CLI_NOM()));
								bodyMessage += String.format(new Locale("pt", "BR"), "Quantidade de Itens: %.3f \n", cPreVendaEnvio.getDouble(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_QTD_ITENS()));
								bodyMessage += String.format(new Locale("pt", "BR"), "Valor: %.2f \n", cPreVendaEnvio.getDouble(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_TOT()));


								mimeMessage.setText(bodyMessage);

								ByteArrayOutputStream buffer = new ByteArrayOutputStream();
								mimeMessage.writeTo(buffer);
								byte[] bytes = buffer.toByteArray();
								String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
								com.google.api.services.gmail.model.Message message = new com.google.api.services.gmail.model.Message();
								message.setRaw(encodedEmail);

								mService.users().messages().send("me", message).execute();

								ContentValues valuesEnvio = new ContentValues();
								valuesEnvio.put("PVE_EMAIL_ENVIADO", 1);

								getApp().getDbPrePedido().update("PRE_VENDA", valuesEnvio,
										String.format(new Locale("pt", "BR"),
												"PVE_SEQ = %d",
												cPreVendaEnvio.getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())), null);
								emailsEnviados++;
							}
						}

						return emailsEnviados;
					}

				} catch (MessagingException me) {

					return null;

				} catch (UserRecoverableAuthIOException ex) {

					return null;
				} catch (IOException ioE) {

					return null;

				}

				return null;
			}
		}

		return null;
	}

	public void onReceive(Context context, Intent intent) {
		// TODO: This method is called when the BroadcastReceiver is receiving
		// an Intent broadcast.
		if(intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){
			ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo nInfo =  connManager.getActiveNetworkInfo();
			if(nInfo != null){
				Log.d("CONNECTIONDEBUG", nInfo.getTypeName());
				boolean isWifi = nInfo.getType() == ConnectivityManager.TYPE_WIFI;
				boolean isMobile = nInfo.getType() == ConnectivityManager.TYPE_MOBILE;
				if(isWifi || isMobile){
					try{
						EnviaPedido(context);				
					}catch(Exception ex){
						Log.e(funcoes.TAG, ex.getMessage()!=null?ex.getMessage():erroConnection);
					}
				}
				if(isWifi){
					//CFFDbFuncoes.EfetuaAtualizacaoCarga(context);					
				}
			}else
				Log.d("CONNECTIONDEBUG", "DESCONECTADO DA INTERNET");
			Log.d("CONNECTIONDEBUG", intent.getAction());
			Log.d("CONNECTIONDEBUG", "Connection Changed");
		}
	}
	
	protected void EnviaPedido(Context ctx) {
		// TODO Auto-generated method stub
		StringBuilder mensagemEmail = new StringBuilder("");
		CFFDbPrePedido dbPrePedido = new CFFDbPrePedido(ctx, "compumobile.db");
		try {
			String queryPreVenda = "SELECT P.PVE_SEQ, C.CLI_SEQ, C.CLI_NOM , P.PVE_DAT, C.CLI_NOM_EMAIL, P.PVE_VAL_TOT FROM CLIENTE C, PRE_VENDA P WHERE P.CLI_SEQ = C.CLI_SEQ AND P.PVE_EMAIL_ENVIADO = 0";
			SQLiteDatabase db = dbPrePedido.getDb();
			Cursor cPreVenda = db.rawQuery(queryPreVenda, null);
			if (cPreVenda.getCount() > 0) {
				while (cPreVenda.moveToNext()) {
					String queryItemPreVenda = String
							.format("SELECT P.PRO_DSC_RESUM, I.IPV_QTD, I.IPV_VAL_UNIT, I.IPV_VAL_TOT FROM PRODUTO P, ITEM_PRE_VENDA I WHERE"
									+ "	P.PRO_SEQ = I.PRO_SEQ AND I.PVE_SEQ = %s",
									cPreVenda.getString(0));
					Cursor cItemPrevenda = db.rawQuery(
							queryItemPreVenda, null);
					mensagemEmail.append(String.format(
							"Orcamento dia %s",
							""));
					mensagemEmail.append("\nCliente: "
							+ cPreVenda.getString(1) + " - "
							+ cPreVenda.getString(2));
					Cursor cConfig = db
							.rawQuery(
									"SELECT USR_EMAIL, USR_SENHA_EMAIL, FUNCIONARIO, NOM_FUNCIONARIO, EMP_NOM_EMAIL FROM CONFIG",
									null);
					cConfig.moveToFirst();
					mensagemEmail.append("\nVendedor: "
							+ cConfig.getString(2) + " - "
							+ cConfig.getString(3));
					mensagemEmail
							.append("\n------------------------------------");
					mensagemEmail.append("\nDescricao dos itens:\n");
					mensagemEmail.append("\tDescricao\n");
					if (cItemPrevenda.getCount() > 0) {
						while (cItemPrevenda.moveToNext()) {
							mensagemEmail
									.append(cItemPrevenda.getPosition()
											+ 1
											+ " - "
											+ funcoes.alinhaTexto(
													cItemPrevenda
															.getString(0),
													40, "E")
											+ " "
											+ funcoes.alinhaTexto(
													funcoes.getFloatFormated(
															String.valueOf(cItemPrevenda
																	.getFloat(1)),
															3), 8, "D")
											+ " "
											+ funcoes.alinhaTexto(
													funcoes.getFloatFormated(
															String.valueOf(cItemPrevenda
																	.getFloat(2)),
															2), 8, "D")
											+ " "
											+ funcoes.alinhaTexto(
													funcoes.getFloatFormated(
															String.valueOf(cItemPrevenda
																	.getFloat(3)),
															2), 8, "D")
											+ "\n");
						}
						mensagemEmail
								.append("\n------------------------------------");
						mensagemEmail
								.append("\nFormas/Condicoes de pagamento:\n");
						String qFormaCondicao = String
								.format("SELECT FCP.PVE_SEQ, F.FOR_PAG_DSC, C.CON_PAG_DSC, FCP.FCP_VAL_NEGOC FROM FORMA_COND_PAGAM_PRE_VENDA FCP, FORMA_PAGAMENTO F, CONDICAO_PAGAMENTO C"
										+ "			WHERE FCP.FOR_PAG_SEQ = F.FOR_PAG_SEQ AND"
										+ "				  FCP.CON_PAG_SEQ = C.CON_PAG_SEQ AND"
										+ "				  F.FOR_PAG_SEQ = C.FOR_PAG_SEQ AND FCP.PVE_SEQ = %s",
										cPreVenda.getString(0));
						Cursor cFormaPagamento = db.rawQuery(
								qFormaCondicao, null);
						while (cFormaPagamento.moveToNext()) {
							mensagemEmail.append("\n "
									+ cFormaPagamento.getString(1)
									+ " "
									+ funcoes.getFloatFormated(String
											.valueOf(cFormaPagamento
													.getFloat(3)), 2));
						}
						mensagemEmail.append("\n\nTotal do pedido R$: "
								+ funcoes.getFloatFormated(
										String.valueOf(cPreVenda
												.getFloat(5)), 2));
						if (!(cConfig.getString(0).equals("") || cConfig
								.getString(1).equals(""))) {
							StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
									.permitAll().build();
							StrictMode.setThreadPolicy(policy);
							SenderMail mail = new SenderMail(
									cConfig.getString(0),
									cConfig.getString(1));
							try {
								ArrayList<String> email = new ArrayList<String>();
								if (!cConfig.getString(0).equals(""))
									email.add(cConfig.getString(0));
								if (!cConfig.getString(4).equals(""))
									email.add(cConfig.getString(4));
								if (!cPreVenda.getString(4).equals(""))
									email.add(cPreVenda.getString(4));
								if (!email.isEmpty()) {
									mail.sendMail(
											"Pedido "
													+ cPreVenda
															.getString(0)
													+ " efetuado",
											mensagemEmail.toString(),
											cConfig.getString(0), email);
									mensagemEmail.delete(0,
											mensagemEmail.length());
									ContentValues uEmail = new ContentValues();
									uEmail.put("PVE_EMAIL_ENVIADO", 1);
									db.update(
											"PRE_VENDA",
											uEmail,
											"PVE_SEQ = "
													+ cPreVenda
															.getString(0),
											null);
								}
							} catch (Exception ex) {

							}
						}

					} else {
						try {
							throw new Exception(
									"Erro: Pedido sem itens");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception ex) {
			if (ex.getMessage() != null)
				Log.e("ERRO", ex.getMessage());
		}
	}
}
