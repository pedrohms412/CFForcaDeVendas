package com.compu.android.forcadevendas.adapter;

import android.text.Editable;
import android.text.TextWatcher;

public class ItemPreVendaPositionTextWatcher implements TextWatcher {
    private RecyclerItemPreVendaAdapter.AdapterHolder mHolder;

    public ItemPreVendaPositionTextWatcher(RecyclerItemPreVendaAdapter.AdapterHolder holder){
        setHolder(holder);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public RecyclerItemPreVendaAdapter.AdapterHolder getHolder() {
        return mHolder;
    }

    private void setHolder(RecyclerItemPreVendaAdapter.AdapterHolder holder) {
        this.mHolder = holder;
    }
}
