package com.compu.android.forcadevendas;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.compu.android.adapter.CFFRecyclerCursorAdapter;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.android.lib.NetworkTasks;
import com.compu.android.lib.NetworkTasks.TaskResponse;
import com.compu.db.CFFDataSet;
import com.compu.lib.funcoes;
import com.google.android.material.navigation.NavigationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PesquisaProdutoActivity extends CFFFragmentActivity
                    implements NavigationView.OnNavigationItemSelectedListener{

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private RecyclerView rcPesquisa;
    private EditText edtPesquisa;
    private CFFRecyclerCursorAdapter adapter;
    private boolean efetuandopesquisa;
    private String query;
    private int depSeq;
    private int secSeq;
    private boolean promocao;
    private boolean porCodBarra;

    private String qryDepartamento = "SELECT D.DEP_SEQ AS ID, D.DEP_DSC AS NAME FROM DEPARTAMENTO D";

    private String qrySecao = "SELECT S.SEC_SEQ AS ID, S.SEC_DSC AS NAME FROM SECAO S WHERE S.DEP_SEQ = %d";

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.pesquisa_produto);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        toolbar.setSubtitle("Pesquisa");

        if(getData().getExtras() == null) getData().putExtras(new Bundle());

        depSeq = 0;
        secSeq = 0;
        promocao = false;
        porCodBarra = false;

        query = "SELECT P.PRO_SEQ, " +
                "       IFNULL(P.PRO_DSC_COMPL, P.PRO_DSC_RESUM) AS PRO_DSC, " +
                "       P.PRO_PRC_UNIT, "+
                "       P.PRO_DAT_INI_PROMOCAO, "+
                "       P.PRO_DAT_FIM_PROMOCAO, "+
                "       P.PRO_PRC_PROMOCAO,  " +
                "       P.PES_QTD_EST_PRO, "+
                "       P.IMG_SEQ,  "+
                "       I.IMG_IMAGEM, "+
                "       PPC.PPC_PRC_UNI_CLI, "+
                "       IFNULL(P.PRO_PRC_UNI_ATAC, 0.0) AS PRO_PRC_UNI_ATAC, "+
                "       IFNULL(C.CLI_TIP_SEQ, 0) AS CLI_TIP_SEQ, "+
                "       I.IMG_DAT_MOD, "+
                "       T.ITP_PRC_UNI_PROD"+
                "  FROM PRODUTO P " +
                "        LEFT JOIN CLIENTE C ON (C.CLI_SEQ = %d ) "+
                "        LEFT JOIN ITEM_TAB_PRECO_CLIENTE T ON ( T.CLI_SEQ = C.CLI_SEQ AND P.PRO_SEQ = T.PRO_SEQ AND T.EMP_SEQ = P.EMP_SEQ ) " +
                "        LEFT JOIN PRECO_PRODUTO_CLIENTE PPC ON( PPC.PRO_SEQ = P.PRO_SEQ AND PPC.CLI_SEQ = C.CLI_SEQ ) "+
                "        LEFT JOIN IMAGEM I ON ( I.IMG_SEQ = P.IMG_SEQ)";

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm != null)
                    imm.hideSoftInputFromWindow(drawer.getWindowToken(), 0);
            }
        });
        toggle.syncState();

        rcPesquisa = findViewById(R.id.rcPesquisa);
        rcPesquisa.setLayoutManager(new LinearLayoutManager(this));
        rcPesquisa.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm != null)
                    imm.hideSoftInputFromWindow(rcPesquisa.getWindowToken(), 0);
            }
        });

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View drawerGroup = getLayoutInflater().inflate(R.layout.pesquisa_produto_drawer, null, false);

        navigationView.addView(drawerGroup);


        Spinner spnDepartamento = drawerGroup.findViewById(R.id.spnDepartamento);
        final Spinner spnSecao = drawerGroup.findViewById(R.id.spnSecao);

        Button btnEfetuar = drawerGroup.findViewById(R.id.btnEfetuar);
        Button btnPromocao = drawerGroup.findViewById(R.id.btnPromocao);

        if(spnDepartamento != null && spnSecao != null){
            Record record = new Record(0, "");

            btnEfetuar.setOnClickListener(v -> {
                drawer.closeDrawer(GravityCompat.START);
                adapter = null;
                rcPesquisa.setAdapter(null);
                EfetuaPesquisa(edtPesquisa.getText().toString());
            });

            btnPromocao.setOnClickListener( v->{
                    promocao = true;
                    drawer.closeDrawer(GravityCompat.START);
                    adapter = null;
                    rcPesquisa.setAdapter(null);
                    EfetuaPesquisa(edtPesquisa.getText().toString());
            });

            Cursor cDepartamento = getApp().getDbPrePedido().rawQuery(qryDepartamento, null);
            try {

                if(cDepartamento.getCount() > 0) {
                    ArrayList<Record> departamentos = new ArrayList<>();
                    departamentos.add(record);
                    while(cDepartamento.moveToNext()){
                        departamentos.add(new Record(cDepartamento.getInt(0), cDepartamento.getString(1)));
                    }

                    SpinnerAdapterDrawer adapter = new SpinnerAdapterDrawer(departamentos, R.layout.record_drawer_spinner);
                    adapter.setOnInflateSpinnerListener((v, record1) -> {
                        TextView txvName = v.findViewById(R.id.txvName);
                        txvName.setText(record1.getName());
                    });
                    spnDepartamento.setAdapter(adapter);

                    adapter.notifyDataSetChanged();
                }
            } finally {
                cDepartamento.close();
            }
            spnDepartamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Record record = new Record(0, "");

                    depSeq = ((Record) parent.getSelectedItem()).getId();
                    Cursor cSecao = getApp().getDbPrePedido().rawQuery(String.format(new Locale("pt","BR"), qrySecao, depSeq), null);
                    try {

                        if(cSecao.getCount() > 0) {
                            ArrayList<Record> secoes = new ArrayList<>();
                            secoes.add(record);
                            while(cSecao.moveToNext()){
                                secoes.add(new Record(cSecao.getInt(0), cSecao.getString(1)));
                            }

                            SpinnerAdapterDrawer adapter = new SpinnerAdapterDrawer(secoes, R.layout.record_drawer_spinner);
                            adapter.setOnInflateSpinnerListener((v, record12) -> {
                                TextView txvName =  v.findViewById(R.id.txvName);
                                txvName.setText(record12.getName());
                            });
                            spnSecao.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            spnSecao.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    secSeq = ((Record) parent.getSelectedItem()).getId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    } finally {
                        cSecao.close();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        edtPesquisa = toolbar.findViewById(R.id.edtPesquisa);
        edtPesquisa.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        ImageButton btnScanBar = toolbar.findViewById(R.id.btnScanBar);
        if(btnScanBar != null){
            btnScanBar.setOnClickListener(v -> {
                IntentIntegrator scanIntegrator = new IntentIntegrator(PesquisaProdutoActivity.this);
                scanIntegrator.setPrompt("Scan a Barcode");
                scanIntegrator.setBeepEnabled(true);
                scanIntegrator.setOrientationLocked(false);
                scanIntegrator.setBarcodeImageEnabled(true);
                scanIntegrator.initiateScan();
            });
        }

        EfetuaPesquisa(edtPesquisa.getText().toString());

        efetuandopesquisa = false;

        edtPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!efetuandopesquisa) {
                    adapter = null;
                    rcPesquisa.setAdapter(null);
                    EfetuaPesquisa(edtPesquisa.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void EfetuaPesquisa(String descricao) {
        String queryLocal = String.format(new Locale("pt", "BR"), query, getData().getExtras().getInt("CLI_SEQ", 0));
        if(!porCodBarra) {
            if (!descricao.equals("")) {
                if (depSeq > 0 && secSeq == 0)
                    queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.PRO_DSC_RESUM LIKE(\'%%%s%%\') AND P.DEP_SEQ = %d ", queryLocal, descricao, depSeq);
                else if (depSeq > 0 && secSeq > 0)
                    queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.PRO_DSC_RESUM LIKE(\'%%%s%%\') AND P.DEP_SEQ = %d AND P.SEC_SEQ = %d", queryLocal, descricao, depSeq, secSeq);
                else
                    queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.PRO_DSC_RESUM LIKE(\'%%%s%%\')", queryLocal, descricao);
            } else {
                if (depSeq > 0 && secSeq == 0)
                    queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.DEP_SEQ = %d ", queryLocal, depSeq);
                else if (depSeq > 0 && secSeq > 0)
                    queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.DEP_SEQ = %d AND P.SEC_SEQ = %d", queryLocal, depSeq, secSeq);
            }
            if (promocao) {
                if (queryLocal.contains("WHERE"))
                    queryLocal = String.format(new Locale("pt", "BR"), "%s AND P.PRO_TIP_PRECO = 1", queryLocal);
                else
                    queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.PRO_TIP_PRECO = 1", queryLocal);
                promocao = false;
            }
        } else {
            queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE P.PRO_COD_BARRA = \'%s\'", queryLocal, descricao);
        }

        if(!queryLocal.contains("WHERE")) queryLocal = String.format(new Locale("pt","BR"), "%s WHERE ", queryLocal);

        if(getData().getExtras().getInt("EMP_SEQ", 0) != 0)
            queryLocal = String.format(new Locale("pt","BR"), "%s AND P.EMP_SEQ = %d", queryLocal, getData().getExtras().getInt("EMP_SEQ", 0));

        queryLocal = String.format(new Locale("pt", "BR"), "%s AND P.PRO_STATUS = 1 ORDER BY 2", queryLocal);

        if(queryLocal.substring(queryLocal.indexOf("WHERE")+5, queryLocal.length()).trim().startsWith("AND"))
            queryLocal = String.format(new Locale("pt","BR"), "%s %s",queryLocal.substring(0, queryLocal.indexOf("WHERE") + 5), queryLocal.substring(queryLocal.indexOf("WHERE")+5, queryLocal.length()).trim().replaceFirst("AND", ""));

        Cursor cProduto = getApp().getDbPrePedido().rawQuery(queryLocal, null);
        if(cProduto.getCount() > 0){
            String finalQueryLocal = queryLocal;
            adapter = new CFFRecyclerCursorAdapter(cProduto, R.layout.record_pesquisa_produto) {
                @Override
                public void SetValues(View v, final int Position) {
                    try {
                        getCursor().moveToPosition(Position);
                        TextView txvDescricao = v.findViewById(R.id.txvDescricao);
                        TextView txvPreco = v.findViewById(R.id.txvPreco);
                        TextView txvUnitario = v.findViewById(R.id.txvUnitario);
                        TextView txvEstoque = v.findViewById(R.id.txvEstoque);
                        ImageView imgImagem = v.findViewById(R.id.imgProduto);
                        ProgressBar progressBar = v.findViewById(R.id.progressbar);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
                        Calendar calendar = Calendar.getInstance(new Locale("pt", "BR"));
                        if (txvDescricao != null && getCursor().getString(1) != null)
                            txvDescricao.setText(getCursor().getString(1));
                        if (txvPreco != null && getCursor().getString(2) != null) {
                            try {
                                if ((getCursor().getInt(getCursor().getColumnIndex("CLI_TIP_SEQ")) == 2) && (getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_UNI_ATAC")) > 0)) {
                                    txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_UNI_ATAC"))), 0), 10, "D"));
                                    txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, android.R.color.black));
                                    txvPreco.setVisibility(View.VISIBLE);
                                    txvUnitario.setVisibility(View.INVISIBLE);
                                } else if (getCursor().isNull(getCursor().getColumnIndex("PPC_PRC_UNI_CLI"))) {
                                    if (getCursor().isNull(getCursor().getColumnIndex("ITP_PRC_UNI_PROD"))) {
                                        if (getCursor().getString(getCursor().getColumnIndex("PRO_DAT_INI_PROMOCAO")) != null
                                                && getCursor().getString(getCursor().getColumnIndex("PRO_DAT_FIM_PROMOCAO")) != null) {
                                            Date date1 = sdf.parse(getCursor().getString(getCursor().getColumnIndex("PRO_DAT_INI_PROMOCAO")));
                                            Date date2 = sdf.parse(getCursor().getString(getCursor().getColumnIndex("PRO_DAT_FIM_PROMOCAO")));
                                            if (calendar.getTime().after(date1) && calendar.getTime().before(date2)) {
                                                txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_PROMOCAO"))), 0), 10, "D"));
                                                txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, R.color.steelBlueDark));
                                                txvUnitario.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_UNIT"))), 0), 10, "D"));
                                                txvUnitario.setVisibility(View.VISIBLE);
                                            } else {
                                                txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_UNIT"))), 0), 10, "D"));
                                                txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, android.R.color.black));
                                                txvUnitario.setVisibility(View.INVISIBLE);
                                            }
                                        } else {
                                            txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_UNIT"))), 0), 10, "D"));
                                            txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, android.R.color.black));
                                            txvUnitario.setVisibility(View.INVISIBLE);
                                        }
                                    } else {
                                        txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("ITP_PRC_UNI_PROD"))), 0), 10, "D"));
                                        txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, android.R.color.black));
                                        txvPreco.setVisibility(View.VISIBLE);
                                        txvUnitario.setVisibility(View.INVISIBLE);
                                    }
                                } else {
                                    txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PPC_PRC_UNI_CLI"))), 0), 10, "D"));
                                    txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, android.R.color.black));
                                    txvPreco.setVisibility(View.VISIBLE);
                                    txvUnitario.setVisibility(View.INVISIBLE);
                                }


                            } catch (Exception ignored) {
                                txvPreco.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(getCursor().getColumnIndex("PRO_PRC_UNIT"))), 0), 10, "D"));
                                txvPreco.setTextColor(ContextCompat.getColor(PesquisaProdutoActivity.this, android.R.color.black));
                                txvUnitario.setVisibility(View.INVISIBLE);
                            }
                        }
                        if (txvEstoque != null)
                            txvEstoque.setText(String.format("Est.: %s", funcoes.alinhaTexto(
                                    funcoes.getFloatFormated(String.valueOf(getCursor().getFloat(6)), 1),
                                    10, "C")));

                        if (imgImagem != null && getCursor().getBlob(8) != null) {
                            Bitmap bmpImagem = BitmapFactory.decodeByteArray(getCursor().getBlob(8), 0, getCursor().getBlob(8).length);
                            imgImagem.setImageBitmap(bmpImagem);
                            imgImagem.setVisibility(View.VISIBLE);
//                            NetworkTasks task = new NetworkTasks(
//                                    (aVoid, self) -> {
//                                        getCursor().moveToPosition(Position);
//                                        try {
//                                            if (getCursor().getInt(getCursor().getColumnIndex("PRO_SEQ")) == 773) {
//                                                Log.d(funcoes.TAG, "OK");
//                                            }
//                                            CFFDataSet dsImagem = getApp().getAndroidSocket().getQuery(String.format(new Locale("pt", "BR"), "SELECT I.IMG_SEQ, I.IMG_IMAGEM, I.IMG_DAT_MOD FROM ECF_IMAGEM I WHERE IMG_SEQ = %d AND IMG_DAT_MOD > CONVERT(DATETIME, \'%s\', 103)", getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ")), getCursor().getString(getCursor().getColumnIndex("IMG_DAT_MOD"))), 1);
//                                            if (dsImagem.getCount() == 0) {
//                                                return new NetworkTasks.TaskResponse(null, "Nenhum registro encontrado");
//                                            }
//                                            getApp().getDbPrePedido().delete("IMAGEM", "IMG_SEQ = ?", new String[]{String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ")))});
//                                            ContentValues contentImagem = new ContentValues();
//                                            contentImagem
//                                                    .put("IMG_SEQ", dsImagem.getInteger("IMG_SEQ"));
//                                            contentImagem
//                                                    .put("IMG_IMAGEM", dsImagem.getBlob("IMG_IMAGEM"));
//                                            contentImagem.put("IMG_DAT_MOD",
//                                                    sdf.format(dsImagem.getDate("IMG_DAT_MOD")));
//                                            getApp().getDbPrePedido().insert("IMAGEM", null, contentImagem);
//                                            setCursor(getApp().getDbPrePedido().rawQuery(finalQueryLocal, null));
//                                            getCursor().moveToPosition(Position);
//                                            return new NetworkTasks.TaskResponse(dsImagem, "OK");
//                                        } catch (Exception ex) {
//                                            return new NetworkTasks.TaskResponse(null, funcoes.getStringValue(ex.getMessage()));
//                                        }
//                                    },
//                                    (aVoid, self) -> {
//                                        if (aVoid.getResponse() != null) {
//                                            Cursor cImg = getApp().getDbPrePedido().rawQuery("SELECT IMG_IMAGEM FROM IMAGEM WHERE IMG_SEQ = ?", new String[]{String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ")))});
//                                            Log.d(funcoes.TAG, "POS = " + String.valueOf(Position) + "IMG_SEQ = " + String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ"))));
//                                            if (cImg.getCount() > 0) {
//                                                cImg.moveToFirst();
//                                                if (cImg.getBlob(0) != null) {
//                                                    Bitmap bmp = BitmapFactory
//                                                            .decodeByteArray(cImg.getBlob(0), 0,
//                                                                    cImg.getBlob(0).length);
//                                                    imgImagem.setImageBitmap(bmp);
//                                                    imgImagem.setVisibility(View.VISIBLE);
//                                                } else {
//                                                    imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
//                                                    imgImagem.setVisibility(View.VISIBLE);
//                                                }
//                                            } else {
//                                                imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
//                                                imgImagem.setVisibility(View.VISIBLE);
//                                            }
//                                        }
//                                    }
//                            );
//                            task.Execute();
                        } else {
                            if (imgImagem != null) {

                                if (!getCursor().isNull(getCursor().getColumnIndex("IMG_SEQ"))) {
//                                    getApp().getDbPrePedido().delete("IMAGEM", "IMG_SEQ = ?", new String[]{String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ")))});
//                                    NetworkTasks task = new NetworkTasks(null,
//                                            (aVoid, self) -> {
//                                                getCursor().moveToPosition(Position);
//                                                Log.d(funcoes.TAG, "POS = " + String.valueOf(Position) + "IMG_SEQ = " + String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ"))));
//                                                try {
//                                                    CFFDataSet dsImagem = getApp().getAndroidSocket().getQuery(
//                                                            String.format(new Locale("pt", "BR"),
//                                                                    "SELECT I.IMG_SEQ, I.IMG_IMAGEM, I.IMG_DAT_MOD FROM ECF_IMAGEM I WHERE I.IMG_SEQ = %d",
//                                                                    getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ"))), 1);
//                                                    if (dsImagem != null) {
//                                                        if (dsImagem.getCount() > 0) {
//                                                            getApp().getDbPrePedido().delete("IMAGEM", "IMG_SEQ = ?",
//                                                                    new String[]{dsImagem.getString("IMG_SEQ")});
//                                                            ContentValues contentImagem = new ContentValues();
//                                                            contentImagem
//                                                                    .put("IMG_SEQ", dsImagem.getInteger("IMG_SEQ"));
//                                                            contentImagem
//                                                                    .put("IMG_IMAGEM", dsImagem.getBlob("IMG_IMAGEM"));
//                                                            contentImagem.put("IMG_DAT_MOD",
//                                                                    sdf.format(dsImagem.getDate("IMG_DAT_MOD")));
//                                                            getApp().getDbPrePedido().insert("IMAGEM", null, contentImagem);
//                                                            setCursor(getApp().getDbPrePedido().rawQuery(finalQueryLocal, null));
//                                                            getCursor().moveToPosition(Position);
//                                                        }
//                                                    } else {
//                                                        Log.d(funcoes.TAG, String.format(new Locale("pt", "BR"),
//                                                                "Erro carregando dados da imagem %d",
//                                                                dsImagem.getInt("IMG_SEQ")));
//                                                    }
//                                                    return new TaskResponse(dsImagem, "OK");
//                                                } catch (Exception ex) {
//                                                    return new TaskResponse(null, "OK");
//                                                }
//                                            },
//                                            (aVoid, self) -> {
//                                                if (aVoid.getResponse() != null) {
//                                                    Cursor cImg = getApp().getDbPrePedido().rawQuery("SELECT IMG_IMAGEM FROM IMAGEM WHERE IMG_SEQ = ?", new String[]{String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ")))});
//                                                    Log.d(funcoes.TAG, "POS = " + String.valueOf(Position) + "IMG_SEQ = " + String.valueOf(getCursor().getInt(getCursor().getColumnIndex("IMG_SEQ"))));
//                                                    if (cImg.getCount() > 0) {
//                                                        cImg.moveToFirst();
//                                                        if (cImg.getBlob(0) != null) {
//                                                            Bitmap bmpImagem = BitmapFactory
//                                                                    .decodeByteArray(cImg.getBlob(0), 0,
//                                                                            cImg.getBlob(0).length);
//                                                            imgImagem.setImageBitmap(bmpImagem);
//                                                            imgImagem.setVisibility(View.VISIBLE);
//                                                        } else {
//                                                            imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
//                                                            imgImagem.setVisibility(View.VISIBLE);
//                                                        }
//                                                    } else {
//                                                        imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
//                                                        imgImagem.setVisibility(View.VISIBLE);
//                                                    }
//                                                }
//                                            });
//                                    task.Execute();
                                    imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
                                    imgImagem.setVisibility(View.VISIBLE);

                                } else {
                                    imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
                                    imgImagem.setVisibility(View.VISIBLE);
                                }

                                imgImagem.setImageDrawable(ContextCompat.getDrawable(PesquisaProdutoActivity.this, R.mipmap.ic_sem_imagem));
                                imgImagem.setVisibility(View.VISIBLE);
                            }
                        }
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                        CardView cardView = v.findViewById(R.id.cardview);
                        if (cardView != null)
                            cardView.setOnClickListener(v1 -> {
                                getCursor().moveToPosition(Position);
                                if (getData().getExtras().getInt("CONSULTA", 0) == 0) {
                                    Intent intentResult = new Intent();
                                    intentResult.putExtra("codigo", getCursor().getInt(0));
                                    intentResult.putExtra("campo_descricao", getCursor().getString(1));

                                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("pt", "BR"));
                                    Calendar calendar1 = Calendar.getInstance(new Locale("pt", "BR"));

                                    try {
                                        if (!getCursor().isNull(13)) {
                                            intentResult.putExtra("valor_unitario",
                                                    funcoes.getDoubleValue(getCursor().getDouble(13)));
                                            intentResult.putExtra("promocao", false);
                                        } else if (getCursor().getString(3) != null
                                                && getCursor().getString(4) != null) {
                                            Date date1 = sdf1.parse(getCursor().getString(3));
                                            Date date2 = sdf1.parse(getCursor().getString(4));
                                            if (calendar1.getTime().after(date1) && calendar1.getTime()
                                                    .before(date2)) {
                                                intentResult.putExtra("valor_unitario",
                                                        funcoes.getDoubleValue(getCursor().getDouble(5)));
                                                intentResult.putExtra("valor_unitario_digita",
                                                        funcoes.getDoubleValue(getCursor().getDouble(2)));
                                                intentResult.putExtra("valor_desconto",
                                                        getCursor().getDouble(2) - funcoes
                                                                .getDoubleValue(getCursor().getDouble(5)));
                                                intentResult.putExtra("promocao", true);
                                            } else {
                                                if ((getCursor()
                                                        .getInt(getCursor().getColumnIndex("CLI_TIP_SEQ"))
                                                        == 2) && (getCursor().getFloat(
                                                        getCursor().getColumnIndex("PRO_PRC_UNI_ATAC"))
                                                        > 0)) {
                                                    intentResult.putExtra("valor_unitario", funcoes
                                                            .getDoubleValue(getCursor().getDouble(10)));
                                                    intentResult.putExtra("promocao", false);
                                                } else {
                                                    intentResult.putExtra("valor_unitario", funcoes
                                                            .getDoubleValue(getCursor().getDouble(2)));
                                                    intentResult.putExtra("promocao", false);
                                                }
                                            }
                                        } else {
                                            if ((getCursor()
                                                    .getInt(getCursor().getColumnIndex("CLI_TIP_SEQ")) == 2)
                                                    && (getCursor().getFloat(
                                                    getCursor().getColumnIndex("PRO_PRC_UNI_ATAC")) > 0)) {
                                                intentResult.putExtra("valor_unitario",
                                                        funcoes.getDoubleValue(getCursor().getDouble(10)));
                                                intentResult.putExtra("promocao", false);
                                            } else {
                                                intentResult.putExtra("valor_unitario",
                                                        funcoes.getDoubleValue(getCursor().getDouble(2)));
                                                intentResult.putExtra("promocao", false);
                                            }
                                        }


                                    } catch (Exception ignored) {
                                        intentResult.putExtra("valor_unitario",
                                                funcoes.getDoubleValue(getCursor().getDouble(2)));
                                        intentResult.putExtra("promocao", false);
                                    }

                                    setResult(0, intentResult);
                                    finish();
                                } else {
                                    Intent iProdutoInfo = new Intent(PesquisaProdutoActivity.this, ProdutoInfoActivity.class);
                                    iProdutoInfo.putExtra("PRO_SEQ", getCursor().getInt(0));
                                    iProdutoInfo.putExtra("EMP_SEQ", getIntent().getExtras().getInt("EMP_SEQ"));
                                    startActivity(iProdutoInfo);
                                }
                            });
                    } catch (Exception ex){
                        if(ex.getMessage() != null)
                            Log.e(funcoes.TAG, ex.getMessage());
                    }
                }
            };
            rcPesquisa.setAdapter(adapter);

            rcPesquisa.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if(imm != null)
                        imm.hideSoftInputFromWindow(rcPesquisa.getWindowToken(), 0);
                }
            });
            TextView nodata = findViewById(R.id.nodata);
            if (adapter.getCursor().getCount() > 0) {
                rcPesquisa.setVisibility(View.VISIBLE);
                nodata.setVisibility(View.GONE);
            } else {
                rcPesquisa.setVisibility(View.GONE);
                nodata.setVisibility(View.VISIBLE);
            }
            EditText edtPesquisa = findViewById(R.id.edtPesquisa);
            edtPesquisa.setHint("Buscar Produto");
        } else {
            RecyclerView rcPesquisas = findViewById(R.id.rcPesquisa);
            TextView nodata = findViewById(R.id.nodata);
            rcPesquisas.setVisibility(View.GONE);
            nodata.setVisibility(View.VISIBLE);
        }


    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public class SpinnerAdapterDrawer extends BaseAdapter {

        private List<Record> records;
        private @LayoutRes int layout;
        private OnSpinnerInflateListener oninflatespinnerlistener;

        public SpinnerAdapterDrawer(List<Record> records, @LayoutRes int layout){
            if(records == null)
                setRecords(new ArrayList<Record>());
            else setRecords(records);
            setLayout(layout);
        }

        @Override
        public int getCount() {
            return records.size();
        }

        @Override
        public Record getItem(int position) {
            return records.get(position);
        }

        @Override
        public long getItemId(int position) {
            return records.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if( v == null) {
                v = LayoutInflater.from(PesquisaProdutoActivity.this).inflate(getLayout(), parent, false);
            }

            if(getOnInflateSpinnerListener() != null){
                getOnInflateSpinnerListener().OnInflateView(v, getRecords().get(position));
            }

            return v;
        }

        public List<Record> getRecords() {
            return records;
        }

        public void setRecords(List<Record> records) {
            this.records = records;
        }

        public int getLayout() {
            return layout;
        }

        public void setLayout(int layout) {
            this.layout = layout;
        }

        public OnSpinnerInflateListener getOnInflateSpinnerListener() {
            return oninflatespinnerlistener;
        }

        public void setOnInflateSpinnerListener(OnSpinnerInflateListener oninflatespinnerlistener) {
            this.oninflatespinnerlistener = oninflatespinnerlistener;
        }


    }

    public interface OnSpinnerInflateListener {
        void OnInflateView(View v, Record record);
    }

    public class Record{
        private int id;
        private String name;
        private boolean checked;

        public Record(){

        }

        public Record(int id, String name){
            setId(id);
            setName(name);
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 49374) {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanningResult != null) {
                final String scanContent = scanningResult.getContents();
                if (!funcoes.getStringValue(scanContent).equals("")) {
                    edtPesquisa.setHint("Localizando produto");
                    porCodBarra = true;
                    EfetuaPesquisa(funcoes.getStringValue(scanContent));
                }
            }
        }
    }
}
