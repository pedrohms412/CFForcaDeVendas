package com.compu.android.forcadevendas;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.compu.android.adapter.CFFQueryAdapter;
import com.compu.android.config.db.CFFDbConfig;
import com.compu.android.config.db.CFFDbHelper;

import java.util.ArrayList;
import java.util.List;

public class CFFPesquisaActivity extends ListActivity {

    public static String QUERY = "query";
    public static String COLUMNS = "columns";
    public static String LAYOUTID = "layoutid";
    public static String P_RESULT = "presult";
    public static String R_RESULT = "rresult";

    private String sQuery = "";
    private String sColumnResult;
    private List<String[]> Resultado;
    private List<String> Columns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String erro = "";
        Bundle dados = getIntent().getExtras();
        if(dados == null)
            erro = "Voce deve informar os dados a serem processados( Bundle == null)";
        sColumnResult = getIntent().getExtras().getString(R_RESULT)==null?"":getIntent().getExtras().getString(R_RESULT);
        if(getIntent().getExtras().getString(QUERY) == null)
            erro = "Voce deve informar a query a ser executada";
        if(getIntent().getExtras().getInt(COLUMNS) == 0)
            erro = "Voce deve informar o numero de colunas a serem pesquisados";
        if(getIntent().getExtras().getInt(LAYOUTID) == 0)
            erro = "Voce deve informar o layout a ser enviado para o ListView";
        if(erro.equals("")){
            sQuery = getIntent().getExtras().getString(QUERY);
            CarregaLista();
        }else{
            Intent iResultErro = new Intent();
            Bundle bResultErro = new Bundle();
            bResultErro.putString(P_RESULT, erro);
            iResultErro.putExtras(bResultErro);
            setResult(2000, iResultErro);
            finish();
        }
    }

    private void CarregaLista(){
        Resultado = new ArrayList<String[]>();
        Columns = new ArrayList<String>();
        String query = getIntent().getExtras().getString(QUERY);
        int iColumns = getIntent().getExtras().getInt(COLUMNS),
            LayoutID = getIntent().getExtras().getInt(LAYOUTID);
        CFFDbHelper helper = new CFFDbHelper(this, "compumobile.db", CFFDbConfig.Companion.getDatabaseVersion());
        SQLiteDatabase db = helper.getDb();
        Cursor cPesquisa = db.rawQuery(query, null);
        for(int i = 0 ; i < cPesquisa.getColumnCount() ; i++){
            Columns.add(cPesquisa.getColumnName(i));
        }
        cPesquisa.moveToFirst();
//        CFFAdapterQuery adapter = new CFFAdapterQuery(this);
        CFFQueryAdapter adapter = null;
        if(cPesquisa.getCount() > 0){
            String[] valor = new String[(iColumns>cPesquisa.getColumnCount()?cPesquisa.getColumnCount():iColumns)];
            for(int i = 0 ; i < valor.length; i++){
                valor[i] = cPesquisa.getString(i);
            }
            Resultado.add(valor);
//            adapter.add(LayoutID, valor);
            cPesquisa.moveToNext();
        }
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if(Columns != null){
            Intent iResult = new Intent();
            if(sColumnResult.equals("")){
                iResult.putExtra(P_RESULT, Resultado.get(position)[0]);
            }else{
                iResult.putExtra(P_RESULT, Resultado.get(position)[Columns.indexOf(sColumnResult)]);
            }
            setResult(2001, iResult);
            finish();
        }
    }
}
