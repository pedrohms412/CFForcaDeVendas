package com.compu.android.forcadevendas.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.compu.android.adapter.CFFRecyclerRowAdapter;
import com.compu.android.application.CFFApplicationContext;
import com.compu.android.config.db.CFFDbConfig;
import com.compu.android.config.db.CFFDbPrePedido;
import com.compu.android.forcadevendas.MainActivity;
import com.compu.android.forcadevendas.PesquisaProdutoActivity;
import com.compu.android.forcadevendas.ProdutoInfoActivity;
import com.compu.android.forcadevendas.R;
import com.compu.android.forcadevendas.adapter.ItemPreVendaPositionTextWatcher;
import com.compu.android.forcadevendas.adapter.RecyclerItemPreVendaAdapter;
import com.compu.android.fragments.CFFDialogFragment;
import com.compu.android.fragments.CFFFragment;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.android.interfaces.CFFDialogFragmentListener;
import com.compu.android.persistencia.EcfItemPreVenda;
import com.compu.lib.funcoes;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FragmentItemPedido extends CFFFragment {

    private View MainView;
    private RecyclerItemPreVendaAdapter adapter;
    private boolean Computing = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setMainView(inflater.inflate(R.layout.itempedido, container, false));
        RecyclerView rvItem = getMainView().findViewById(R.id.rvItem);
        rvItem.setLayoutManager(new LinearLayoutManager(getCFFActivity(), LinearLayoutManager.VERTICAL, false));
        CFFRecyclerRowAdapter<String> emptyAdapter = new CFFRecyclerRowAdapter<>();
        emptyAdapter.addRow(R.layout.nenhumregistro_recycler, "", null);
        rvItem.setAdapter(emptyAdapter);

        setHasOptionsMenu(true);

        return getMainView();
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    @Override
    protected void initComponents() {

    }

    public static FragmentItemPedido newInstance(Bundle args){
        if(args == null)
            args = new Bundle();

        FragmentItemPedido fragment = new FragmentItemPedido();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getCFFActivity().getMenuInflater().inflate(R.menu.itempedido, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(MainActivity.preVenda != null){
            int id = item.getItemId();
            if(id == R.id.mni_novo){
                PesquisaNovoProduto();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void PesquisaNovoProduto(){
        if(MainActivity.preVenda.getSyncStatus() == 2) return;
        if(funcoes.getIntValue(MainActivity.preVenda.getCliSeq()) != 0) {
            Intent iItemPesquisa = new Intent(getCFFActivity(), PesquisaProdutoActivity.class);
            iItemPesquisa.putExtra("CLI_SEQ", MainActivity.preVenda.getCliSeq());
            iItemPesquisa.putExtra("EMP_SEQ", MainActivity.preVenda.getEmpSeq());
            startActivityForResult(iItemPesquisa, 0);
        } else {
            Snackbar.make(getCFFActivity().findViewById(R.id.content), "Deve ser informado um cliente primeiro", Snackbar.LENGTH_LONG)
                    .setAction("action",  null)
                    .show();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
    }

    public View getMainView() {
        return MainView;
    }

    public void setMainView(View mainView) {
        MainView = mainView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0){
            if(data != null && data.getExtras() != null){
                if(data.getExtras().getInt("codigo") > 0) {
                    long resultRow = -1;
                    SQLiteDatabase dbPedido = getCFFApp().getDbPrePedido();
                    Cursor cMax = dbPedido.rawQuery("SELECT (SELECT IFNULL(MAX(IPV_PRIMARY), 0) FROM ITEM_PRE_VENDA) AS IPV_PRIMARY, IFNULL(MAX(IPV_SEQ), 0) AS IPV_SEQ FROM ITEM_PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);
                    try {
                        cMax.moveToFirst();
                        EcfItemPreVenda item = new EcfItemPreVenda();
                        item.setIpvPrimary(cMax.getInt(0) + 1);
                        item.setPveSeq(MainActivity.preVenda.getPveSeq());
                        item.setIpvSeq(cMax.getInt(1)+1);
                        item.setProSeq(data.getExtras().getInt("codigo"));
                        item.setEstSeq(getCFFActivity().getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_EST_SEQ(),1));
                        item.setIpvStatus(0);
                        item.setIpvQtd(1.0d);
                        if(data.getExtras().getDouble("valor_unitario_digita") > 0)
                            item.setIpvValUnit(data.getExtras().getDouble("valor_unitario_digita"));
                        else item.setIpvValUnit(data.getExtras().getDouble("valor_unitario"));
                        item.setIpvValTot(item.getIpvValUnit());
                        item.setIpvValLiq(data.getExtras().getDouble("valor_unitario"));
                        item.setIpvValUnitDigita(data.getExtras().getDouble("valor_unitario"));
                        item.setIpvValDesc(data.getExtras().getDouble("valor_desconto"));
                        item.setIpvPerDesc(funcoes.truncaValor((((item.getIpvValLiq() / (item.getIpvValLiq() + item.getIpvValDesc()))*100)-100)*-1,3));
                        if((MainActivity.preVenda.getCliPerDescVenda() != null) && data.getExtras().getDouble("valor_desconto", 0.0d) == 0){
                            if(MainActivity.preVenda.getCliPerDescVenda() > 0.0d){
                                item.setIpvValDesc((data.getExtras().getDouble("valor_unitario")*MainActivity.preVenda.getCliPerDescVenda())/100);
                                item.setIpvValUnitDigita(data.getExtras().getDouble("valor_unitario") - item.getIpvValDesc());
                                item.setIpvValLiq(item.getIpvValUnitDigita());
                                item.setIpvPerDesc(funcoes.truncaValor((((item.getIpvValLiq() / (item.getIpvValLiq() + item.getIpvValDesc()))*100)-100)*-1,3));
                            }
                        }
                        if(item.getIpvValDesc() != 0){
                            item.setIpvIndDescCliente(true);
                            Double valDesc = item.getIpvValDesc();
                            MainActivity.preVenda.setPveValDescItem(0.0d);
                            for(EcfItemPreVenda itemPreDesc : MainActivity.preVenda.getEcfItensPreVenda()){
                                MainActivity.preVenda.setPveValDescItem(itemPreDesc.getIpvValDesc());
                            }
                            MainActivity.preVenda.setPveValDescItem(MainActivity.preVenda.getPveValDescItem() + valDesc);
                        }
                        item.setIpvValAcres(0.0d);
                        item.setIpvPerAcres(0.0d);

                        MainActivity.preVenda.setPveValDesc(0.0d);
                        MainActivity.preVenda.setPveValAcres(0.0d);

                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
                        item.setPromocao(data.getExtras().getBoolean("promocao", false));
                        item.setIpvDatMod(java.util.Calendar.getInstance().getTime());

                        MainActivity.preVenda.getEcfItensPreVenda().add(item);
                        CFFDbPrePedido.Companion.CommitItemPreVenda(dbPedido, item);

                        CarregaItens(item);
                    } finally {
                        cMax.close();
                    }


                    getData().putExtra("FOR_PAG_SEQ", 0);
                    getData().putExtra("CON_PAG_SEQ", 0);

                    ViewGroup contents = getCFFActivity().findViewById(R.id.content);
                    ListView lstFormaPagamento = contents.findViewById(R.id.lstFormaPagamento);
                    if(lstFormaPagamento != null){
                        lstFormaPagamento.setAdapter(null);
                    }
                    MainActivity.preVenda.getEcfFormaCondPagamPreVendas().clear();
                    getCFFApp().getDbPrePedido().delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?", new String[]{MainActivity.preVenda.getPveSeq().toString()});

                    RecyclerView rvItem = getMainView().findViewById(R.id.rvItem);
                    if(rvItem != null){
                        rvItem.scrollToPosition(MainActivity.preVenda.getEcfItensPreVenda().size()-1);
                    }
                }
            }
        }
    }

    public void CarregaItens(EcfItemPreVenda ecfItem) {
//        final String query =
//                "SELECT IP.PVE_SEQ, IP.IPV_SEQ, IP.PRO_SEQ, IP.EST_SEQ, IP.IPV_STATUS, " +
//                        "       IP.IPV_QTD, IP.IPV_VAL_UNIT, IP.IPV_VAL_TOT, IP.IPV_VAL_LIQ,  " +
//                        "       IP.IPV_VAL_UNIT_DIGITA, IP.IPV_VAL_DESC, IP.IPV_PER_DESC,  " +
//                        "       IP.IPV_VAL_ACRES, IP.IPV_PER_ACRES, IP.IPV_DAT_MOD, IP.IPV_OBS, " +
//                        "       IFNULL(P.PRO_DSC_COMPL, P.PRO_DSC_RESUM) AS PRO_DSC_RESUM, P.PRO_COD_BARRA, " +
//                        "       P.IMG_SEQ, I.IMG_IMAGEM " +
//                        "  FROM ITEM_PRE_VENDA IP," +
//                        "       PRODUTO P " +
//                        "         LEFT JOIN IMAGEM I ON ( I.IMG_SEQ = P.IMG_SEQ )" +
//                        " WHERE IP.PRO_SEQ = P.PRO_SEQ" +
//                        "   AND IP.PVE_SEQ = ?";
        final RecyclerView rvItem =  getMainView().findViewById(R.id.rvItem);
        if (getAdapter() == null) {
            setAdapter(new RecyclerItemPreVendaAdapter(MainActivity.preVenda.getEcfItensPreVenda(), R.layout.record_pesquisa_item_prevenda) {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public void SetValues(RecyclerItemPreVendaAdapter.AdapterHolder holder, final int Position) {
                    Computing  = true;
                    final EcfItemPreVenda itemPreVenda = MainActivity.preVenda.getEcfItensPreVenda().get(holder.getHolderPosition());
                    Cursor cItem = getCFFApp().getDbPrePedido().rawQuery("SELECT IFNULL(P.PRO_DSC_COMPL, P.PRO_DSC_RESUM) AS PRO_DSC FROM PRODUTO P WHERE P.PRO_SEQ = ?", new String[]{funcoes.getIntValue(itemPreVenda.getProSeq()).toString()});
                    try {
                        cItem.moveToFirst();
                        TextView txvDescricao = holder.getRootView().findViewById(R.id.txvDescricao);
                        TextView txvValorUnitario =  holder.getRootView().findViewById(R.id.txvValorUnitario);
                        final EditText edtQuantidade = holder.getRootView().findViewById(R.id.edtQuantidade);

                        final TextView txvQuantidade =  holder.getRootView().findViewById(R.id.txvQuantidade);
                        final TextView txvValorTotal =  holder.getRootView().findViewById(R.id.txvValorTotal);
                        if (txvDescricao != null && cItem.getString(0) != null)
                            txvDescricao.setText(
                                String.format(new Locale("pt","BR"), "%d-%s", Position + 1, cItem.getString(0)));
                        if (txvValorUnitario != null && itemPreVenda.getIpvValUnitDigita() != null) {
                            txvValorUnitario.setText(String.format("Valor Unitário: %s", funcoes
                                .alinhaTexto(funcoes
                                    .getFloatFormated(String.valueOf(itemPreVenda.getIpvValUnitDigita()),
                                        0), 10, "E")));
                        }
                        if (txvQuantidade != null && itemPreVenda.getIpvQtd() != null) {
                            txvQuantidade.setText(String.format("Quantidade: %s", funcoes
                                .alinhaTexto(
                                    funcoes.getFloatFormated(String.valueOf(itemPreVenda.getIpvQtd()), 1),
                                    10, "E")));
                            edtQuantidade.setText(String.format("%s", funcoes
                                .alinhaTexto(
                                    funcoes.getFloatFormated(String.valueOf(itemPreVenda.getIpvQtd()), 1),
                                    10, "E")).trim());
                        }
                        if (txvValorTotal != null && itemPreVenda.getIpvValLiq() != null) {
                            txvValorTotal.setText(funcoes.alinhaTexto("R$ " + funcoes.getFloatFormated(String.valueOf(itemPreVenda.getIpvValLiq()), 0), 10, "D"));
                            if(itemPreVenda.getIpvValDesc() > 0)
                                txvValorTotal.setTextColor(ContextCompat.getColor(getCFFActivity(), R.color.steelBlueDark));
                            else txvValorTotal.setTextColor(ContextCompat.getColor(getCFFActivity(), android.R.color.black));
                        }

                        ImageButton imgDetalhes = holder.getRootView().findViewById(R.id.imgDetalhes);
                        if(imgDetalhes != null) {
                            imgDetalhes.setOnClickListener(v12 -> {
                                if(MainActivity.preVenda.getSyncStatus() == 2) return;
                                final CFFDialogFragment.Builder builder = new CFFDialogFragment.Builder();
                                builder.setLayout(R.layout.content_fragment_detalhe_item);
                                builder.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                                builder.setTitle("Detalhes");
                                builder.setListener(new CFFDialogFragmentListener() {
                                    @Override
                                    public void OnDismissListener() {

                                    }

                                    @Override
                                    public void OnShowListener(View v12) {
                                        DisplayMetrics metrics = new DisplayMetrics();
                                        getCFFActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                                        String query = "SELECT P.PRO_SEQ, P.PRO_DSC_RESUM, " +
                                                "       P.PRO_DSC_COMPL, P.PRO_COD_BARRA, " +
                                                "       D.DEP_DSC, S.SEC_DSC, " +
                                                "       P.PRO_PRC_UNIT, " +
                                                "       P.PRO_PRC_PROMOCAO, " +
                                                "       I.IMG_SEQ, I.IMG_IMAGEM,  " +
                                                "       P.PRO_DAT_INI_PROMOCAO, P.PRO_DAT_FIM_PROMOCAO, " +
                                                "       P.PES_QTD_EST_PRO " +
                                                "  FROM PRODUTO P " +
                                                "        LEFT JOIN IMAGEM I ON ( I.IMG_SEQ = P.IMG_SEQ ), " +
                                                "       DEPARTAMENTO D, " +
                                                "       SECAO S " +
                                                "WHERE D.DEP_SEQ = P.DEP_SEQ " +
                                                "  AND S.SEC_SEQ = P.SEC_SEQ " +
                                                "  AND P.PRO_SEQ = ?";
                                        Cursor cProduto = getCFFApp().getDbPrePedido().rawQuery(query, new String[]{itemPreVenda.getProSeq().toString()});
                                        ImageView imgProduto =  v12.findViewById(R.id.imgProduto);
                                        imgProduto.getLayoutParams().width = metrics.widthPixels;
                                        if (imgProduto != null) {
                                            if (cProduto.getCount() > 0) {
                                                cProduto.moveToFirst();
                                                byte[] bImagem = cProduto.getBlob(cProduto.getColumnIndex("IMG_IMAGEM"));
                                                if (bImagem != null) {
                                                    Bitmap bitmapImagem = BitmapFactory.decodeByteArray(bImagem, 0, bImagem.length);
                                                    imgProduto.setImageBitmap(bitmapImagem);
                                                }
                                            }
                                        }
                                        TextView txvProDscResum =  v12.findViewById(R.id.txvDescricao);
                                        TextView txvProDscCompl =  v12.findViewById(R.id.txvDescricaoCompl);
                                        TextView txvProCodBarra =  v12.findViewById(R.id.txvCodBarra);
                                        TextView txvDepDsc =  v12.findViewById(R.id.txvDepartamento);
                                        TextView txvSecDsc =  v12.findViewById(R.id.txvSecao);
                                        TextView txvPreco =  v12.findViewById(R.id.txvPreco);
                                        TextView txvUnitario =  v12.findViewById(R.id.txvUnitario);
                                        TextView txvEstoque =  v12.findViewById(R.id.txvEstoque);

                                        if (txvProDscResum != null)
                                            txvProDscResum.setText(cProduto.getString(cProduto.getColumnIndex("PRO_DSC_RESUM")));
                                        if (txvProDscCompl != null)
                                            txvProDscCompl.setText(cProduto.getString(cProduto.getColumnIndex("PRO_DSC_COMPL")));
                                        if (txvProCodBarra != null)
                                            txvProCodBarra.setText(cProduto.getString(cProduto.getColumnIndex("PRO_COD_BARRA")));
                                        if (txvDepDsc != null)
                                            txvDepDsc.setText(cProduto.getString(cProduto.getColumnIndex("DEP_DSC")));
                                        if (txvSecDsc != null)
                                            txvSecDsc.setText(cProduto.getString(cProduto.getColumnIndex("SEC_DSC")));
                                        if (txvEstoque != null)
                                            txvEstoque.setText(funcoes.getFloatFormated(cProduto.getString(cProduto.getColumnIndex("PES_QTD_EST_PRO")), 1));

                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
                                        Calendar calendar = Calendar.getInstance(new Locale("pt", "BR"));

                                        try {
                                            if (cProduto.getString(cProduto.getColumnIndex("PRO_DAT_INI_PROMOCAO")) != null && cProduto.getString(cProduto.getColumnIndex("PRO_DAT_FIM_PROMOCAO")) != null) {
                                                Date date1 = sdf.parse(cProduto.getString(cProduto.getColumnIndex("PRO_DAT_INI_PROMOCAO")));
                                                Date date2 = sdf.parse(cProduto.getString(cProduto.getColumnIndex("PRO_DAT_FIM_PROMOCAO")));
                                                if (calendar.getTime().after(date1) && calendar.getTime().before(date2)) {
                                                    txvPreco.setText(String.format("R$ %s", funcoes
                                                        .getFloatFormated(cProduto.getString(
                                                            cProduto.getColumnIndex(
                                                                "PRO_PRC_PROMOCAO")), 0)));
                                                    txvPreco.setTextColor(ContextCompat.getColor(getCFFActivity(), R.color.steelBlueDark));
                                                    txvUnitario.setText(String.format("R$ %s",
                                                        funcoes.getFloatFormated(cProduto.getString(
                                                            cProduto
                                                                .getColumnIndex("PRO_PRC_UNIT")),
                                                            0)));
                                                    txvUnitario.setVisibility(View.VISIBLE);
                                                } else {
                                                    txvPreco.setText(String.format("R$ %s", funcoes
                                                        .getFloatFormated(cProduto.getString(
                                                            cProduto
                                                                .getColumnIndex("PRO_PRC_UNIT")),
                                                            0)));
                                                    txvUnitario.setVisibility(View.INVISIBLE);
                                                }
                                            } else {
                                                txvPreco.setText(String.format("R$ %s", funcoes
                                                    .getFloatFormated(cProduto.getString(
                                                        cProduto.getColumnIndex("PRO_PRC_UNIT")),
                                                        0)));
                                                txvUnitario.setVisibility(View.INVISIBLE);
                                            }


                                        } catch (Exception ignored) {
                                            txvPreco.setText(String.format("R$ %s", funcoes
                                                .getFloatFormated(cProduto.getString(
                                                    cProduto.getColumnIndex("PRO_PRC_UNIT")), 0)));
                                            txvUnitario.setVisibility(View.INVISIBLE);
                                        }
                                        final Cursor cItemObs = getCFFApp().getDbPrePedido().rawQuery("SELECT IPV_OBS FROM ITEM_PRE_VENDA WHERE PVE_SEQ = ? AND IPV_SEQ = ?", new String[]{itemPreVenda.getPveSeq().toString(), itemPreVenda.getIpvSeq().toString()});
                                        try {
                                            cItemObs.moveToFirst();
                                            final EditText edtIpeObs = v12.findViewById(R.id.edtObs);
                                            edtIpeObs.setText(cItemObs.getString(0));
                                            Button btnOK =  v12.findViewById(R.id.btnOK);
                                            if (btnOK != null) {
                                                btnOK.setOnClickListener(v1 -> {
                                                    if(MainActivity.preVenda.getSyncStatus() != 2) {
                                                        itemPreVenda.setIpvObs(edtIpeObs.getText().toString());
                                                        CFFDbPrePedido.Companion.CommitItemPreVenda(getCFFApp().getDbPrePedido(), itemPreVenda);
                                                    }
                                                    builder.dismiss();
                                                });
                                            }
                                        } finally {
                                            cItemObs.close();
                                        }
                                    }
                                });
                                builder.show(getFragmentManager(), "FRAG_DETALHES");
                            });
                        }

                        ImageButton btnMais = holder.getRootView().findViewById(R.id.btnMais);
                        if (btnMais != null) {
                            btnMais.setOnClickListener(v13 -> {
                                if(MainActivity.preVenda.getSyncStatus() == 2) return;
                                double valorQtd = itemPreVenda.getIpvQtd() + 1;

                                itemPreVenda.setIpvQtd(valorQtd);
                                itemPreVenda.setIpvValTot(funcoes.truncaValor(itemPreVenda.getIpvValUnit() * valorQtd, 2));
                                itemPreVenda.setIpvValLiq(funcoes.truncaValor(itemPreVenda.getIpvValUnitDigita() * valorQtd, 2));

                                MainActivity.preVenda.setPveQtdItens(MainActivity.preVenda.getPveQtdItens() + 1);
                                MainActivity.preVenda.setPveValTot(MainActivity.preVenda.getPveValTot() + itemPreVenda.getIpvValUnitDigita());


                                if(itemPreVenda.getIpvValDesc() > 0.0d) {
                                    Double valorDesconto = (itemPreVenda.getIpvValUnit() - itemPreVenda.getIpvValUnitDigita())*itemPreVenda.getIpvQtd();

                                    MainActivity.preVenda.setPveValDescItem(0.0d);
                                    for (EcfItemPreVenda itemPreDesc : MainActivity.preVenda
                                        .getEcfItensPreVenda()) {
                                        if(!itemPreDesc.getProSeq().equals(itemPreVenda.getProSeq())) {
                                            MainActivity.preVenda.setPveValDescItem(
                                                MainActivity.preVenda.getPveValDesc() + itemPreDesc
                                                    .getIpvValDesc());
                                        }
                                   }
                                   itemPreVenda.setIpvValDesc(valorDesconto);
                                   MainActivity.preVenda.setPveValDescItem(MainActivity.preVenda.getPveValDesc() + valorDesconto);
                                }

                                CFFDbPrePedido.Companion.CommitItemPreVenda(getCFFApp().getDbPrePedido(), itemPreVenda);
                                CFFDbPrePedido.Companion.CommitPreVenda(getCFFApp().getDbPrePedido(), MainActivity.preVenda);
                                MainActivity.preVenda.getEcfItensPreVenda().set(Position, itemPreVenda);

                                Cursor cPreVenda = getCFFApp().getDbPrePedido().rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);
                                cPreVenda.moveToFirst();

                                CFFFragmentActivity.LoadDataSet(getCFFActivity().findViewById(R.id.content), cPreVenda);

                                getData().putExtra("FOR_PAG_SEQ", 0);
                                getData().putExtra("CON_PAG_SEQ", 0);

                                ViewGroup contents = getCFFActivity().findViewById(R.id.content);
                                ListView lstFormaPagamento = contents.findViewById(R.id.lstFormaPagamento);
                                if(lstFormaPagamento != null){
                                    lstFormaPagamento.setAdapter(null);
                                }
                                getCFFApp().getDbPrePedido().delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?", new String[]{MainActivity.preVenda.getPveSeq().toString()});

                                notifyItemChanged(Position);

                                edtQuantidade.setText(String.format("%s", funcoes
                                .alinhaTexto(
                                    funcoes.getFloatFormated(String.valueOf(itemPreVenda.getIpvQtd()), 1),
                                    10, "E")));

                            });
                        }
                        ImageButton btnMenos = holder.getRootView().findViewById(R.id.btnMenos);
                        if (btnMenos != null) {
                            btnMenos.setOnClickListener(v14 -> {
                                if(MainActivity.preVenda.getSyncStatus() == 2) return;
                                if ((itemPreVenda.getIpvQtd() - 1) > 0) {
                                    double valorQtd = itemPreVenda.getIpvQtd() - 1;

                                    itemPreVenda.setIpvQtd(valorQtd);
                                    itemPreVenda.setIpvValTot(funcoes.truncaValor(itemPreVenda.getIpvValUnit() * valorQtd, 2));
                                    itemPreVenda.setIpvValLiq(funcoes.truncaValor(itemPreVenda.getIpvValUnitDigita() * valorQtd, 2));

                                    MainActivity.preVenda.setPveQtdItens(MainActivity.preVenda.getPveQtdItens() - 1);
                                    MainActivity.preVenda.setPveValTot(MainActivity.preVenda.getPveValTot() - itemPreVenda.getIpvValUnitDigita());
                                    if(MainActivity.preVenda.getCliPerDescVenda() > 0 && itemPreVenda.getIpvValDesc() == 0) {
//                                        MainActivity.preVenda.setPveValDesc(MainActivity.preVenda.getPveValDesc() - (itemPreVenda.getIpvValUnitDigita() * (MainActivity.preVenda.getCliPerDescVenda() / 100)));
//                                        if(MainActivity.preVenda.getPveValTot() > 0)
//                                            MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot() - MainActivity.preVenda.getPveValDesc());
//                                        else MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                         MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                    } else {
//                                        if(MainActivity.preVenda.getPveValTot() > 0)
//                                            MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot() - MainActivity.preVenda.getPveValDesc());
//                                        else MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                        MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                    }

                                    if(itemPreVenda.getIpvValDesc() > 0.0d) {
                                        Double valorDesconto = (itemPreVenda.getIpvValUnit() - itemPreVenda.getIpvValUnitDigita())*itemPreVenda.getIpvQtd();

                                        MainActivity.preVenda.setPveValDescItem(0.0d);
                                        for (EcfItemPreVenda itemPreDesc : MainActivity.preVenda
                                            .getEcfItensPreVenda()) {
                                            if(!itemPreDesc.getProSeq().equals(itemPreVenda.getProSeq())) {
                                                MainActivity.preVenda.setPveValDescItem(
                                                    MainActivity.preVenda.getPveValDesc() + itemPreDesc
                                                        .getIpvValDesc());
                                            }
                                        }
                                        itemPreVenda.setIpvValDesc(valorDesconto);
                                        MainActivity.preVenda.setPveValDescItem(MainActivity.preVenda.getPveValDesc() + valorDesconto);
                                    }


                                    CFFDbPrePedido.Companion.CommitItemPreVenda(getCFFApp().getDbPrePedido(), itemPreVenda);
                                    CFFDbPrePedido.Companion.CommitPreVenda(getCFFApp().getDbPrePedido(), MainActivity.preVenda);
                                    MainActivity.preVenda.getEcfItensPreVenda().set(Position, itemPreVenda);

                                    Cursor cPreVenda = getCFFApp().getDbPrePedido().rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);
                                    cPreVenda.moveToFirst();

                                    CFFFragmentActivity.LoadDataSet( getCFFActivity().findViewById(R.id.content), cPreVenda);

                                    getData().putExtra("FOR_PAG_SEQ", 0);
                                    getData().putExtra("CON_PAG_SEQ", 0);

                                    ViewGroup contents =  getCFFActivity().findViewById(R.id.content);
                                    ListView lstFormaPagamento = contents.findViewById(R.id.lstFormaPagamento);
                                    if(lstFormaPagamento != null){
                                        lstFormaPagamento.setAdapter(null);
                                    }
                                    getCFFApp().getDbPrePedido().delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?", new String[]{MainActivity.preVenda.getPveSeq().toString()});

                                    notifyItemChanged(Position);
                                    edtQuantidade.setText(String.format("%s", funcoes
                                                            .alinhaTexto(
                                                                funcoes.getFloatFormated(String.valueOf(itemPreVenda.getIpvQtd()), 1),
                                                                10, "E")));
                                }
                            });
                        }

                        ImageButton imgDeleteItemPreVenda = holder.getRootView().findViewById(R.id.imgDeleteItemPreVenda);
                        if(imgDeleteItemPreVenda != null){
                            imgDeleteItemPreVenda.setOnClickListener(v15 -> {
                                if(MainActivity.preVenda.getSyncStatus() == 2) return;
                                MainActivity.preVenda.getEcfItensPreVenda().remove(Position);
                                getCFFApp().getDbPrePedido().delete("ITEM_PRE_VENDA", "PVE_SEQ = ? AND IPV_SEQ = ?", new String[]{MainActivity.preVenda.getPveSeq().toString(), itemPreVenda.getIpvSeq().toString()});
                                MainActivity.preVenda.setPveQtdItens(MainActivity.preVenda.getPveQtdItens() - 1);
                                MainActivity.preVenda.setPveQtdProd(MainActivity.preVenda.getPveQtdProd() - itemPreVenda.getIpvQtd());
                                MainActivity.preVenda.setPveValTot(MainActivity.preVenda.getPveValTot() - itemPreVenda.getIpvValLiq());
                                if(MainActivity.preVenda.getCliPerDescVenda() > 0 && itemPreVenda.getIpvValDesc() == 0) {
//                                    MainActivity.preVenda.setPveValDesc(funcoes.truncaValor(MainActivity.preVenda.getPveValDesc(),3) - (itemPreVenda.getIpvValLiq() * (MainActivity.preVenda.getCliPerDescVenda() / 100)));
//                                    if(MainActivity.preVenda.getPveValTot() > 0)
//                                        MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot() - MainActivity.preVenda.getPveValDesc());
//                                    else MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                    MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                } else {
//                                    if(MainActivity.preVenda.getPveValTot() > 0)
//                                        MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot()- MainActivity.preVenda.getPveValDesc());
//                                    else MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                    MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot());
                                }
                                CFFDbPrePedido.Companion.CommitPreVenda(getCFFApp().getDbPrePedido(), MainActivity.preVenda);

                                Cursor cPreVenda = getCFFApp().getDbPrePedido().rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);
                                cPreVenda.moveToFirst();

                                CFFFragmentActivity.LoadDataSet(getCFFActivity().findViewById(R.id.content), cPreVenda);

                                getData().putExtra("FOR_PAG_SEQ", 0);
                                getData().putExtra("CON_PAG_SEQ", 0);

                                ViewGroup contents = getCFFActivity().findViewById(R.id.content);
                                ListView lstFormaPagamento = contents.findViewById(R.id.lstFormaPagamento);
                                if(lstFormaPagamento != null){
                                    lstFormaPagamento.setAdapter(null);
                                }
                                getCFFApp().getDbPrePedido().delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?", new String[]{MainActivity.preVenda.getPveSeq().toString()});

                                notifyDataSetChanged();

                            });
                        }
                        edtQuantidade.addTextChangedListener(new ItemPreVendaPositionTextWatcher(holder) {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i,
                                int i1,
                                int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1,
                                int i2) {
                                if(MainActivity.preVenda.getSyncStatus() == 2) return ;
                                EcfItemPreVenda itemLocal = getAdapter().getItensPreVenda().get(holder.getHolderPosition());
                                if (Computing)
                                    return;

                                double valorQtd = funcoes
                                    .getDoubleValue(charSequence.toString());

                                itemLocal.setIpvQtd(valorQtd);
                                itemLocal.setIpvValTot(
                                    funcoes.truncaValor(itemLocal.getIpvValUnit() * valorQtd,
                                        2));
                                itemLocal.setIpvValLiq(funcoes
                                    .truncaValor(itemLocal.getIpvValUnitDigita() * valorQtd,
                                        2));

                                CFFDbPrePedido.Companion
                                    .CommitItemPreVenda(getCFFApp().getDbPrePedido(),
                                        itemLocal);

                                double totalProdutos = 0.0d;
                                double totalValor = totalProdutos;
                                int totalItens = 0;
                                for (EcfItemPreVenda itemPreVenda : MainActivity.preVenda
                                    .getEcfItensPreVenda()) {
                                    totalProdutos += itemPreVenda.getIpvQtd();
                                    totalValor += itemPreVenda.getIpvValLiq();
                                    totalItens++;
                                }
                                MainActivity.preVenda.setPveQtdProd(totalProdutos);
                                MainActivity.preVenda.setPveQtdItens(totalItens);
                                MainActivity.preVenda.setPveValTot(totalValor);
                                if (MainActivity.preVenda.getCliPerDescVenda() > 0
                                    && itemLocal.getIpvValDesc() == 0) {
                                    MainActivity.preVenda.setPveValDesc(
                                        MainActivity.preVenda.getPveValDesc() + (
                                            itemLocal.getIpvValUnitDigita() * (
                                                MainActivity.preVenda.getCliPerDescVenda()
                                                    / 100)));
                                    MainActivity.preVenda.setPveValLiq(
                                        MainActivity.preVenda.getPveValTot()
                                            - MainActivity.preVenda.getPveValDesc());
                                } else {
                                    MainActivity.preVenda.setPveValLiq(
                                        MainActivity.preVenda.getPveValTot()
                                            - MainActivity.preVenda.getPveValDesc());
                                }

                                CFFDbPrePedido.Companion
                                    .CommitPreVenda(getCFFApp().getDbPrePedido(),
                                        MainActivity.preVenda);
                                MainActivity.preVenda.getEcfItensPreVenda()
                                    .set(holder.getHolderPosition(), itemLocal);

                                Cursor cPreVenda = getCFFApp().getDbPrePedido()
                                    .rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?",
                                        new String[]{
                                            String.valueOf(MainActivity.preVenda.getPveSeq())},
                                        null);
                                cPreVenda.moveToFirst();

                                CFFFragmentActivity
                                    .LoadDataSet(getCFFActivity().findViewById(R.id.content),
                                        cPreVenda);

                                getData().putExtra("FOR_PAG_SEQ", 0);
                                getData().putExtra("CON_PAG_SEQ", 0);

                                ViewGroup contents = getCFFActivity()
                                    .findViewById(R.id.content);
                                ListView lstFormaPagamento = contents
                                    .findViewById(R.id.lstFormaPagamento);
                                if (lstFormaPagamento != null) {
                                    lstFormaPagamento.setAdapter(null);
                                }
                                getCFFApp().getDbPrePedido()
                                    .delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?",
                                        new String[]{
                                            MainActivity.preVenda.getPveSeq().toString()});
                                txvQuantidade.setText(String.format("Quantidade :%s", funcoes
                                    .alinhaTexto(
                                        funcoes
                                            .getFloatFormated(
                                                String.valueOf(itemLocal.getIpvQtd()),
                                                1),
                                        10, "E")));
                                txvValorTotal.setText(funcoes.alinhaTexto("R$ " + funcoes
                                        .getFloatFormated(
                                            String.valueOf(itemLocal.getIpvValLiq()), 0),
                                    10, "D"));
                                if (itemPreVenda.getIpvValDesc() > 0)
                                    txvValorTotal.setTextColor(ContextCompat
                                        .getColor(getCFFActivity(), R.color.steelBlueDark));
                                else
                                    txvValorTotal.setTextColor(ContextCompat
                                        .getColor(getCFFActivity(), android.R.color.black));
                                if (itemPreVenda.getIpvValLiq() != null) {
                                    txvValorTotal.setText(funcoes.alinhaTexto("R$ " + funcoes
                                        .getFloatFormated(
                                            String.valueOf(itemLocal.getIpvValLiq()),
                                            0), 10, "D"));
                                    if (itemPreVenda.getIpvValDesc() > 0)
                                        txvValorTotal.setTextColor(ContextCompat
                                            .getColor(getCFFActivity(), R.color.steelBlueDark));
                                    else
                                        txvValorTotal.setTextColor(ContextCompat
                                            .getColor(getCFFActivity(), android.R.color.black));
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                if(MainActivity.preVenda.getSyncStatus() == 2){
                                    editable.clear();
                                }
                            }
                        });
                        if (txvValorTotal != null) {
                            txvValorTotal.setOnTouchListener((View view, MotionEvent motionEvent) -> {
                                if(itemPreVenda.isIpvIndDescCliente()) return false;
                                if(MainActivity.preVenda.getCliPerDescVenda() != null)
                                    if(MainActivity.preVenda.getCliPerDescVenda() > 0.0d)
                                        return false;
                                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                                    EditText edtValorDigitado = new EditText(getActivity());
                                    edtValorDigitado.setTag("valor");
                                    edtValorDigitado.setKeyListener(DigitsKeyListener.getInstance("0123456789,."));
                                    edtValorDigitado.setRawInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
                                    edtValorDigitado.setText(funcoes.getFloatFormated(itemPreVenda.getIpvValUnitDigita().toString(), 2));
                                    edtValorDigitado.selectAll();
                                    edtValorDigitado.requestFocus();
                                    edtValorDigitado.setOnTouchListener((v, event) -> {
                                        ((EditText) v).setSelectAllOnFocus(true);
                                        ((EditText) v).selectAll();
                                        return false;
                                    });
                                    AlertDialog.Builder dialog = new AlertDialog.Builder(getCFFActivity());
                                    dialog.setTitle("Preço de venda:");
                                    dialog.setView(edtValorDigitado);
                                    dialog.setPositiveButton("OK", (dialogInterface, i) -> {
                                        if(!edtValorDigitado.getText().toString().equals(funcoes.getFloatFormated(itemPreVenda.getIpvValUnit().toString(), 2))){
                                            if(!edtValorDigitado.getText().toString().equals("")){

                                                if(funcoes.getDoubleValue(edtValorDigitado.getText().toString()).equals(itemPreVenda.getIpvValUnitDigita()))
                                                    return;

                                                try{
                                                    if( funcoes.getDoubleValue(edtValorDigitado.getText().toString()) < itemPreVenda.getIpvValUnit()){
                                                        Double valorDesconto = itemPreVenda.getIpvValUnit() - funcoes.getDoubleValue(edtValorDigitado.getText().toString());
                                                        Cursor cProduto = getCFFApp().getDbConfig().rawQuery("SELECT * FROM PRODUTO WHERE PRO_SEQ = ? AND EMP_SEQ = ?", new String[]{itemPreVenda.getProSeq().toString(), MainActivity.preVenda.getEmpSeq().toString()});
                                                        cProduto.moveToFirst();
                                                        if( valorDesconto > cProduto.getDouble(CFFDbConfig.Companion.getPRODUTO_FIELD_PRO_VAL_MAX_DESC())){
                                                            dialogInterface.dismiss();
                                                            AlertDialog.Builder dialogDesc = new AlertDialog.Builder(getCFFActivity());
                                                            dialogDesc.setTitle(R.string.msg_sistema);
                                                            dialogDesc.setMessage("Desconto acima do permitido!");
                                                            dialogDesc.setPositiveButton("OK",
                                                                (dialogInterface1, i12) -> dialogInterface1
                                                                    .dismiss());
                                                            dialogDesc.show();
                                                            return;
                                                        }
                                                        itemPreVenda.setIpvValDesc(valorDesconto*itemPreVenda.getIpvQtd());
                                                        itemPreVenda.setIpvValUnitDigita(funcoes.getDoubleValue(edtValorDigitado.getText().toString()));
                                                        MainActivity.preVenda.setPveValDescItem(0.0d);
                                                        for(EcfItemPreVenda itemPreDesc : MainActivity.preVenda.getEcfItensPreVenda()){
                                                            MainActivity.preVenda.setPveValDescItem(itemPreDesc.getIpvValDesc());
                                                        }
                                                        MainActivity.preVenda.setPveValDescItem(MainActivity.preVenda.getPveValDescItem() + itemPreVenda.getIpvValDesc());
                                                    } else if( funcoes.getDoubleValue(edtValorDigitado.getText().toString()) > itemPreVenda.getIpvValUnit()){
                                                        itemPreVenda.setIpvValAcres(funcoes.getDoubleValue(edtValorDigitado.getText().toString()) - itemPreVenda.getIpvValUnit());
                                                        itemPreVenda.setIpvValUnitDigita(funcoes.getDoubleValue(edtValorDigitado.getText().toString()));
                                                        if(MainActivity.preVenda.getPveValAcresItem() == null)
                                                            MainActivity.preVenda.setPveValAcresItem(0.0d);
                                                        MainActivity.preVenda.setPveValAcresItem(MainActivity.preVenda.getPveValAcresItem() + itemPreVenda.getIpvValDesc());
                                                    }
                                                    itemPreVenda.setIpvValTot(itemPreVenda.getIpvQtd()*itemPreVenda.getIpvValUnitDigita());

                                                    MainActivity.preVenda.getEcfItensPreVenda().set(Position, itemPreVenda);

                                                    double totalLiq = 0.0d;
                                                    double totaltot = 0.0d;
                                                    for (EcfItemPreVenda item : MainActivity.preVenda.getEcfItensPreVenda()) {
                                                        totalLiq += item.getIpvValTot();
                                                        totaltot += item.getIpvValTot();
                                                    }

//                                                    if(MainActivity.preVenda.getPveValDesc() != null) totalLiq = totalLiq - MainActivity.preVenda.getPveValDesc();
//                                                    if(MainActivity.preVenda.getPveValAcres() != null) totalLiq = totalLiq + MainActivity.preVenda.getPveValAcres();

                                                    MainActivity.preVenda.setPveValLiq(totalLiq);
                                                    MainActivity.preVenda.setPveValTot(totaltot);
                                                    MainActivity.preVenda.setPveValDesc(0.0d);
                                                    MainActivity.preVenda.setPveValAcres(0.0d);

                                                    CFFDbPrePedido.Companion.CommitItemPreVenda(getCFFApp().getDbPrePedido(), itemPreVenda);
                                                    CFFDbPrePedido.Companion.CommitPreVenda(getCFFApp().getDbPrePedido(), MainActivity.preVenda);

                                                    if(txvValorUnitario != null)
                                                        txvValorUnitario.setText(String.format("Valor Unitário: %s", funcoes.alinhaTexto(funcoes.getFloatFormated(String.valueOf(itemPreVenda.getIpvValUnitDigita()),0), 10, "E")));
                                                    txvValorTotal.setText("R$ " + funcoes.getFloatFormated(itemPreVenda.getIpvValTot().toString(), 2));

                                                    Cursor cPreVenda = getCFFApp().getDbPrePedido().rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);                                                        cPreVenda.moveToFirst();
                                                    cPreVenda.moveToFirst();

                                                    CFFFragmentActivity.LoadDataSet(getCFFActivity().findViewById(R.id.content), cPreVenda);

                                                    if (itemPreVenda.getIpvValDesc() > 0)
                                                        txvValorTotal.setTextColor(ContextCompat
                                                            .getColor(getCFFActivity(), R.color.steelBlueDark));
                                                    else
                                                        txvValorTotal.setTextColor(ContextCompat
                                                            .getColor(getCFFActivity(), android.R.color.black));

                                                } catch (Exception ex){
                                                    AlertDialog erroDialog = new AlertDialog.Builder(getCFFActivity()).create();
                                                    erroDialog.setTitle(R.string.msg_sistema);
                                                    if(ex.getMessage() != null)
                                                        erroDialog.setMessage(ex.getMessage());
                                                    else erroDialog.setMessage(ex.getClass().getName());
                                                    erroDialog.show();
                                                }
                                            }
                                        }
                                    });
                                    dialog.show();
                                }
                                return false;
                            });
                        }

                    }finally {
                      Computing = false;
                      cItem.close();
                    }

                }
            });

            if (!(rvItem.getAdapter() instanceof RecyclerItemPreVendaAdapter))
                rvItem.setAdapter(getAdapter());
            getAdapter().notifyDataSetChanged();


        } else {
            getAdapter().setItensPreVenda(MainActivity.preVenda.getEcfItensPreVenda());
            if (!(rvItem.getAdapter() instanceof RecyclerItemPreVendaAdapter))
                rvItem.setAdapter(getAdapter());
            getAdapter().notifyDataSetChanged();
        }

        SQLiteDatabase dbPedido = getCFFApp().getDbPrePedido();

        Cursor cTotalValor = dbPedido.rawQuery("SELECT SUM(IP.IPV_QTD) AS IPV_QTD_PROD, " +
                "       SUM(IP.IPV_VAL_UNIT_DIGITA*IP.IPV_QTD) AS IPV_VAL_TOT, " +
                "       SUM(IP.IPV_VAL_LIQ) AS IPV_VAL_LIQ,  " +
                "       IFNULL(SUM(IP.IPV_VAL_DESC),0) AS PVE_VAL_DESC_ITEM, "+
                "       COUNT(IP.IPV_SEQ) AS IPV_QTD_ITENS " +
                "  FROM ITEM_PRE_VENDA IP WHERE IP.PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())});

        try {

            cTotalValor.moveToFirst();

            ContentValues values = new ContentValues();
            values.put("PVE_QTD_PROD", cTotalValor.getDouble(0));
            values.put("PVE_QTD_ITENS", cTotalValor.getInt(4));
            values.put("PVE_VAL_TOT", cTotalValor.getFloat(1));
            values.put("PVE_VAL_DESC_ITEM", cTotalValor.getDouble(3));
            if(ecfItem == null) ecfItem = new EcfItemPreVenda();
            if(MainActivity.preVenda.getCliPerDescVenda() > 0 && funcoes.getDoubleValue(ecfItem.getIpvValDesc()) == 0 && funcoes.getDoubleValue(ecfItem.getIpvValLiq()) > 0) {
//                values.put("PVE_PER_DESC", MainActivity.preVenda.getCliPerDescVenda());
//                values.put("PVE_VAL_DESC", MainActivity.preVenda.getPveValDesc()+(ecfItem.getIpvValLiq()*(MainActivity.preVenda.getCliPerDescVenda()/100)));
                values.put("PVE_VAL_LIQ", cTotalValor.getFloat(1));
//                values.put("PVE_VAL_LIQ", cTotalValor.getFloat(1) - (MainActivity.preVenda.getPveValDesc()+(ecfItem.getIpvValLiq()*(MainActivity.preVenda.getCliPerDescVenda()/100))));
                MainActivity.preVenda.setPveValLiq(cTotalValor.getDouble(1));
//                MainActivity.preVenda.setPveValDesc(MainActivity.preVenda.getPveValDesc()+(ecfItem.getIpvValLiq()*(MainActivity.preVenda.getCliPerDescVenda()/100)));
//                MainActivity.preVenda.setPvePerDesc(MainActivity.preVenda.getCliPerDescVenda());
//                MainActivity.preVenda.setPveValLiq(cTotalValor.getFloat(1) - MainActivity.preVenda.getPveValDesc());
            } else {
                if(cTotalValor.getFloat(1) > 0) {
                    values.put("PVE_VAL_LIQ", cTotalValor.getDouble(1));
                    MainActivity.preVenda.setPveValLiq(cTotalValor.getDouble(1));
//                    values.put("PVE_VAL_LIQ", cTotalValor.getDouble(1) - MainActivity.preVenda.getPveValDesc());
//                    MainActivity.preVenda.setPveValLiq(cTotalValor.getDouble(1) - MainActivity.preVenda.getPveValDesc());
                }else {
                    values.put("PVE_VAL_LIQ", cTotalValor.getDouble(1));
                    MainActivity.preVenda.setPveValLiq(cTotalValor.getDouble(1));
                }
            }

            MainActivity.preVenda.setPveValDescItem(cTotalValor.getDouble(3));
            MainActivity.preVenda.setPveQtdProd(cTotalValor.getDouble(0));
            MainActivity.preVenda.setPveQtdItens(cTotalValor.getInt(4));
            MainActivity.preVenda.setPveValTot(cTotalValor.getDouble(1));



            dbPedido.update("PRE_VENDA", values, "PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())});

            Cursor cPreVenda = dbPedido.rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);
            cPreVenda.moveToFirst();
            CFFFragmentActivity.LoadDataSet( getCFFActivity().findViewById(R.id.content), cPreVenda);
        } finally {
            cTotalValor.close();
        }

    }

    public RecyclerItemPreVendaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerItemPreVendaAdapter adapter) {
        this.adapter = adapter;
    }
}

