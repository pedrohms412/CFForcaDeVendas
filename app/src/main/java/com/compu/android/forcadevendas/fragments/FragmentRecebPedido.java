package com.compu.android.forcadevendas.fragments;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;

import com.compu.lib.funcoes;
import com.compu.android.adapter.ItemAdapter2;
import com.compu.android.adapter.ListTextObjectAdapter2;
import com.compu.android.application.CFFApplicationContext;
import com.compu.android.forcadevendas.R;
import com.compu.android.fragments.CFFFragment;
import com.compu.android.util.CFFActivityUtil;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.view.inputmethod.InputMethodManager;
import android.text.method.DigitsKeyListener;
import android.content.Context;

public class FragmentRecebPedido extends CFFFragment {

	private View recebPedido;
	private DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
	private EditText edtFormaPagamento = null;
	private EditText edtCondPagamento = null;
	private EditText edtRecebTotal = null;
	private EditText edtRecebValor = null;
	private EditText edtValorCondicao = null;
	private ListView lstFormaPagamento = null;
	private Menu menu;
	public static boolean utiliza_condicao_pre_definida;
	
	private OnTouchListener OnTouchGeraPagamento = new OnTouchListener() {
		
		@SuppressLint("NewApi")
        public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if(getData().getExtras().getInt("SYNC_STATUS") == 3)
				return false;
			if(getData().getExtras().getInt("PRO_SEQ") > 0 
					|| getData().getExtras().getInt("IPV_SEQ") > 0){
				Toast.makeText(getActivity(), "Voce tem um produto em edicao", Toast.LENGTH_SHORT).show();
				return false;
			}
			SQLiteDatabase dbTemp = getCFFApp().getDbPrePedido();
			try{
				switch(v.getId()){
				case R.id.edtFormaPagamento:
//					if(getData().getExtras().getInt("PVE_SEQ") == 0)
//						return false;
//					if(getData().getExtras().getDouble("PVE_VAL_TOT") <= 0.0)
//						return false;

					switch(event.getAction()){
						case MotionEvent.ACTION_DOWN:
							if(utiliza_condicao_pre_definida) {
								Cursor cCliente = dbTemp.rawQuery(String.format(new Locale("pt", "BR"), "SELECT * FROM CLIENTE WHERE CLI_SEQ = %d", getData().getExtras().getInt("CLI_SEQ")), null);
								if (cCliente.getCount() > 0) {
									cCliente.moveToFirst();
									if (cCliente.getInt(16) != 0 && cCliente.getInt(17) != 0) {
										getData().putExtra("FOR_PAG_SEQ", cCliente.getInt(16));
										getData().putExtra("CON_PAG_SEQ", cCliente.getInt(17));
										GeraPrazoPagamento(String.valueOf(getData().getExtras().getDouble("PVE_VAL_TOT")));
										getData().putExtra("FOR_PAG_SEQ", 0);
										getData().putExtra("CON_PAG_SEQ", 0);
										utiliza_condicao_pre_definida = false;
										break;
									}
								}
							}
							if((!edtRecebValor.getText().toString().equals("") && !edtRecebValor.getText().toString().equals("")) && NumberFormat.getInstance(new Locale("pt", "BR")).parse(edtRecebValor.getText().toString()).doubleValue() > 0.0) {
								if (!(CarregaFormaPagamento()))
									Toast.makeText(getActivity(), getString(R.string.err_carga_form_pag), Toast.LENGTH_SHORT).show();
							}
							break;
					}
					break;
				case R.id.edtCondPagamento:
					dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
					try {
//						if(getData().getExtras().getInt("FOR_PAG_SEQ") == 0 || getData().getExtras().getInt("PVE_SEQ") == 0 ||
//								getData().getExtras().getDouble("PVE_VAL_TOT") <= 0.0)
//							return false;
					} catch (Exception e1) {
						return false;
					}
					switch(event.getAction()){
					case MotionEvent.ACTION_DOWN:
						if(!(CarregaCondicaoPagamento()))
							Toast.makeText(getActivity(), getString(R.string.err_carga_cond_pag), Toast.LENGTH_SHORT).show();
						break;
					
					}
					break;
				}
			} catch(Exception ex) {
				new CFFActivityUtil().GeraLogErro(getActivity(), ex, this.getClass().toString());
			} finally {
				dbTemp.close();
			}
			return false;
		}
	};

	public static FragmentRecebPedido newInstance(Bundle args){
		FragmentRecebPedido frag = new FragmentRecebPedido();
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.recebimentopedido, menu);
		this.menu = menu;

		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);
//		MenuItem miIncluir = menu.findItem(R.id.mnuIncluirReceb);
//		EditText edtRecebValor = (EditText) recebPedido.findViewById(R.id.edtRecebValor);
//
////		try {
////			if(getData().getExtras().getInt("PVE_SEQ") > 0
////					|| getData().getExtras().getInt("FOR_PAG_SEQ") == 0) {
////				if ((df.parse(edtRecebValor.getText().toString()).doubleValue()<= 0.0)) {
////					miIncluir.setIcon(R.drawable.new_pb);
////				} else {
////					miIncluir.setIcon(R.drawable.new_color);
////				}
////			}else {
////				miIncluir.setIcon(R.drawable.new_pb);
////			}
////
////		} catch (Exception ignored){}
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(getData().getExtras().getInt("SYNC_STATUS") == 3)
			return super.onOptionsItemSelected(item);
		ListView lstFormaPagamento = (ListView)recebPedido.findViewById(R.id.lstFormaPagamento);
		switch(item.getItemId()){
		case R.id.mnuLimpar:
			if(lstFormaPagamento.getCount() > 0){
				limpa();
			}
			break;
		case R.id.mnuIncluirReceb:
			if(getData().getExtras().getInt("FOR_PAG_SEQ") == 0 && getData().getExtras().getInt("CON_PAG_SEQ") == 0 && getData().getExtras().getInt("PVE_SEQ") != 0) {
				if (utiliza_condicao_pre_definida) {
					SQLiteDatabase dbTemp = getCFFApp().getDbPrePedido();
					Cursor cCliente = dbTemp.rawQuery(String.format("SELECT * FROM CLIENTE WHERE CLI_SEQ = %d", getData().getExtras().getInt("CLI_SEQ")), null);
					if (cCliente.getCount() > 0) {
						cCliente.moveToFirst();
						if (cCliente.getInt(16) != 0 && cCliente.getInt(17) != 0) {
							getData().putExtra("FOR_PAG_SEQ", cCliente.getInt(16));
							getData().putExtra("CON_PAG_SEQ", cCliente.getInt(17));
							GeraPrazoPagamento(String.valueOf(getData().getExtras().getDouble("PVE_VAL_TOT")));
							getData().putExtra("FOR_PAG_SEQ", 0);
							getData().putExtra("CON_PAG_SEQ", 0);
							utiliza_condicao_pre_definida = false;
							onPrepareOptionsMenu(menu);
							break;
						}
					}
				}
				if (!edtRecebValor.equals("") && !edtRecebTotal.equals("") ) {
					try {
						if (NumberFormat.getInstance(new Locale("pt", "BR")).parse(edtRecebValor.getText().toString()).doubleValue() > 0.0) {
							if (!(CarregaFormaPagamento()))
								Toast.makeText(getActivity(), getString(R.string.err_carga_form_pag), Toast.LENGTH_SHORT).show();
						}
					} catch ( ParseException pe){
						Toast.makeText(getActivity(), funcoes.getStringValue(pe.getMessage()), Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		recebPedido = inflater.inflate(R.layout.recebimentopedido, null);

		utiliza_condicao_pre_definida = true;

		setHasOptionsMenu(true);

		initComponents();
			
		return recebPedido;
	}

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    public void initComponents(){
		if(recebPedido != null){
			edtFormaPagamento = (EditText)recebPedido.findViewById(R.id.edtFormaPagamento);
			edtCondPagamento = (EditText)recebPedido.findViewById(R.id.edtCondPagamento);
			edtRecebTotal = (EditText)recebPedido.findViewById(R.id.edtRecebTotal);
			edtRecebValor = (EditText)recebPedido.findViewById(R.id.edtRecebValor);
			lstFormaPagamento = (ListView)recebPedido.findViewById(R.id.lstFormaPagamento);
			edtFormaPagamento.setOnTouchListener(OnTouchGeraPagamento );
			edtCondPagamento.setOnTouchListener(OnTouchGeraPagamento);
		}
	}
	
	
	public void GeraPrazoPagamento(String valor){
		SQLiteDatabase dbTemp = null;
		String qDscPagamento = String.format( 
				"SELECT F.FOR_PAG_DSC, C.CON_PAG_DSC" +
				"	FROM FORMA_PAGAMENTO F, CONDICAO_PAGAMENTO C "+
				" WHERE F.FOR_PAG_SEQ = C.FOR_PAG_SEQ"+
				"   AND F.FOR_PAG_SEQ = %s "+
				"   AND C.CON_PAG_SEQ = %s", getData().getExtras().getInt("FOR_PAG_SEQ"), getData().getExtras().getInt("CON_PAG_SEQ") );
		
		dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
		Cursor cDscPagamento = dbTemp.rawQuery(qDscPagamento, null);
		cDscPagamento.moveToFirst();
		if(getData().getExtras().getInt("FOR_PAG_SEQ") > 0 && getData().getExtras().getInt("CON_PAG_SEQ") > 0 && edtValorCondicao == null ){
			valor = edtRecebTotal.getText().toString();
			edtFormaPagamento.setText(cDscPagamento.getString(0));
			edtCondPagamento.setText(cDscPagamento.getString(1));
		}
		NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
		try {
			if(nf.parse(valor).floatValue()
					> nf.parse(edtRecebValor.getText().toString()).floatValue()){
				new CFFActivityUtil().GeraLogErro(getActivity(), new Exception(getString(R.string.err_inf_valor_pagamento_maior)), this.getClass().toString());
				return;
			}
		} catch (ParseException e) {
			new CFFActivityUtil().GeraLogErro(getActivity(), new Exception(getString(R.string.err_parse_float)), this.getClass().toString());
			return;
		}
		List<ListTextObjectAdapter2> lstObject = new ArrayList<ListTextObjectAdapter2>();
		ItemAdapter2 iAdapter = (ItemAdapter2)lstFormaPagamento.getAdapter();
		try{
			String qPrazo =
				"SELECT * FROM PRAZO_PAGAMENTO WHERE CON_PAG_SEQ = " + getData().getExtras().getInt("CON_PAG_SEQ");
			Cursor cPrazo = dbTemp.rawQuery(qPrazo, null);
			while(cPrazo.moveToNext()){
				Double fValorPrazo = 0.0;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				GregorianCalendar calendar = new GregorianCalendar();
				String qFcp =
						"SELECT COUNT(*) FROM FORMA_COND_PAGAM_PRE_VENDA";
				Cursor cFcp = dbTemp.rawQuery(qFcp, null);
				cFcp.moveToFirst();

				ContentValues values = new ContentValues();
				values.put("FCP_PRIMARY", cFcp.getInt(0) + 1);
				values.put("PVE_SEQ", getData().getExtras().getInt("PVE_SEQ"));
				values.put("FCP_SEQ", cFcp.getInt(0) + 1);
				values.put("CON_PAG_SEQ", getData().getExtras().getInt("CON_PAG_SEQ"));
				values.put("FOR_PAG_SEQ", getData().getExtras().getInt("FOR_PAG_SEQ"));
				if(cPrazo.getPosition() == cPrazo.getCount()-1 &&
						nf.parse(valor).doubleValue() == nf.parse(edtRecebValor.getText().toString()).doubleValue() &&
						cPrazo.getCount() > 1){
					Double fValorNegociado =
							nf.parse(valor).doubleValue()*cPrazo.getDouble(4) / 100;
					BigDecimal bd = new BigDecimal(fValorNegociado);
					bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
					Double fDiferenca = 0.0;
					Cursor cTemp = dbTemp.rawQuery(
							"SELECT SUM(FCP_VAL_NEGOC) FROM FORMA_COND_PAGAM_PRE_VENDA " +
							"	WHERE PVE_SEQ = " + getData().getExtras().getInt("PVE_SEQ") + " AND CON_PAG_SEQ = " + getData().getExtras().getInt("CON_PAG_SEQ"), null);
					cTemp.moveToFirst();
					BigDecimal bdValNegoc = new BigDecimal(cTemp.getDouble(0) + bd.doubleValue());
					if(cTemp.getDouble(0)+bd.doubleValue() != bd.doubleValue()){
						if(bdValNegoc.doubleValue()+bd.doubleValue() > nf.parse(valor).doubleValue()){
							fDiferenca = nf.parse(valor).doubleValue() - (bdValNegoc.doubleValue());
						}else if(bdValNegoc.doubleValue()+bd.doubleValue() < nf.parse(valor).doubleValue()){
							fDiferenca = nf.parse(valor).doubleValue() - (bdValNegoc.doubleValue()) ;
						}
					}
					fValorPrazo = bd.doubleValue()+fDiferenca;
					values.put("FCP_VAL_NEGOC",fValorPrazo);
				}else{
					fValorPrazo = nf.parse(valor).doubleValue()*cPrazo.getDouble(4) / 100;
					BigDecimal bdValorPrazo = new BigDecimal(fValorPrazo);
					bdValorPrazo = bdValorPrazo.setScale(2,BigDecimal.ROUND_HALF_EVEN);
					fValorPrazo =  bdValorPrazo.doubleValue();
					values.put("FCP_VAL_NEGOC", bdValorPrazo.doubleValue());
				}
				calendar.add(GregorianCalendar.DAY_OF_MONTH, cPrazo.getInt(3));
				values.put("FCP_DAT_VENC", sdf.format(calendar.getTime()));
				values.put("FCP_NUM_BANCO", 0);
				values.put("FCP_NUM_AGENCIA", 0);
				values.put("FCP_NUM_CONTA", "");
				values.put("FCP_NUM_CHEQUE", "");
				values.put("FCP_NOM_TITULAR", "");
				values.put("FCP_VAL_LIQ_EMP", nf.parse(edtRecebTotal.getText().toString()).doubleValue()/cPrazo.getCount());
				dbTemp.insert("FORMA_COND_PAGAM_PRE_VENDA", null, values);
				if(lstFormaPagamento.getAdapter() == null){
					lstObject.add(new ListTextObjectAdapter2(getData().getExtras().getInt("CON_PAG_SEQ"), cDscPagamento.getString(1),
							funcoes.getFloatFormated(String.valueOf(fValorPrazo),2)));
					iAdapter = new ItemAdapter2(getActivity(), R.layout.listitemprovider, lstObject);
				}else{
					iAdapter = (ItemAdapter2)lstFormaPagamento.getAdapter();
					iAdapter.add(new ListTextObjectAdapter2(getData().getExtras().getInt("CON_PAG_SEQ"), cDscPagamento.getString(1),
							funcoes.getFloatFormated(String.valueOf(fValorPrazo),2)));
				}
			}
			edtRecebValor.setText(String.format(new Locale("pt", "BR"), "%.2f", nf.parse(edtRecebValor.getText().toString()).doubleValue() -
					nf.parse(valor).doubleValue()));


		}catch(Exception ex){
			new CFFActivityUtil().GeraLogErro(getActivity(), ex, this.getClass().toString());
		}finally{
			dbTemp.close();
		}
		lstFormaPagamento.setAdapter(iAdapter);
		edtFormaPagamento.setText("");
		edtCondPagamento.setText("");
	}
	
	public void limpa(){
		SQLiteDatabase dbTemp = null;
		try{
			dbTemp =  ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
			if(getData().getExtras().getInt("PVE_SEQ") == 0 || lstFormaPagamento.getAdapter() == null)
				return;
			Cursor cFormaCondPagamPreVenda = dbTemp.rawQuery("SELECT * FROM FORMA_COND_PAGAM_PRE_VENDA WHERE PVE_SEQ = " + getData().getExtras().getInt("PVE_SEQ"), null);
			if(cFormaCondPagamPreVenda.getCount() > 0){
				dbTemp.delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = " + getData().getExtras().getInt("PVE_SEQ"), null);
			}
			lstFormaPagamento.setAdapter(null);
			edtRecebValor.setText(funcoes.getFloatFormated(String.valueOf(getData().getExtras().getDouble("PVE_VAL_LIQ")),2));
			edtRecebTotal.setText(funcoes.getFloatFormated(String.valueOf(getData().getExtras().getDouble("PVE_VAL_LIQ")),2));
			edtFormaPagamento.setText("");
			edtCondPagamento.setText("");
			getData().putExtra("FOR_PAG_SEQ", 0);
			getData().putExtra("CON_PAG_SEQ", 0);
//			MenuItem miIncluir = menu.findItem(R.id.mnuIncluirReceb);
//			miIncluir.setIcon(R.drawable.new_color);
			
		}catch(Exception ex){
			new CFFActivityUtil().GeraLogErro(getActivity(), ex, this.getClass().toString());
		}finally{
			dbTemp.close();
		}
	}
	
	public boolean CarregaFormaPagamento(){
			SQLiteDatabase dbTemp = null;
			dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
			if(getData().getExtras().getInt("FOR_PAG_SEQ") > 0 && getData().getExtras().getInt("CON_PAG_SEQ") > 0)
				return false;
			String qFormaPagamento = "SELECT FOR_PAG_SEQ, FOR_PAG_DSC FROM FORMA_PAGAMENTO";
			final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			final Cursor cFormaPagamento = dbTemp.rawQuery(qFormaPagamento, null);
			if(cFormaPagamento.getCount() <= 0)
				return false;
			final String[] formasPagamento = new String[cFormaPagamento.getCount()];
			while(cFormaPagamento.moveToNext()){
				formasPagamento[cFormaPagamento.getPosition()] = cFormaPagamento.getString(1);
			}
			dialog.setTitle("Forma Recebimento:");
			dialog.setItems((CharSequence[])formasPagamento, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface arg0, int arg1) {
					cFormaPagamento.moveToPosition(arg1);
					getData().putExtra("FOR_PAG_SEQ",cFormaPagamento.getInt(0));
					edtFormaPagamento.setText(formasPagamento[arg1]);		
//					MenuItem miIncluir = menu.findItem(R.id.mnuIncluirReceb);
//					miIncluir.setIcon(R.drawable.new_pb);
				}
			});
			dialog.show();
			return true;
	}
	
	public boolean CarregaCondicaoPagamento(){
		SQLiteDatabase dbTemp = null;
		dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
		String qCondPagamento =	String.format( 
				"SELECT * FROM CONDICAO_PAGAMENTO WHERE FOR_PAG_SEQ = %s"
				, getData().getExtras().getInt("FOR_PAG_SEQ"));
		final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		final Cursor cCondPagamento = dbTemp.rawQuery(qCondPagamento, null);
		if(cCondPagamento.getCount() <= 0)
			return false;
		final String[] condPagamento = new String[cCondPagamento.getCount()];
		while(cCondPagamento.moveToNext()){
			condPagamento[cCondPagamento.getPosition()] = cCondPagamento.getString(1);
		}
		dialog.setTitle("Condição Recebimento:");
		dialog.setItems((CharSequence[])condPagamento, new DialogInterface.OnClickListener() {
			
			@SuppressWarnings("deprecation")
			public void onClick(DialogInterface arg0, int arg1) {
				edtCondPagamento.setText(condPagamento[arg1]);
				edtValorCondicao = new EditText(getActivity());
				edtValorCondicao.setTag("valor");
				edtValorCondicao.setKeyListener(DigitsKeyListener.getInstance("0123456789,."));
				edtValorCondicao.setText(edtRecebValor.getText());
				edtValorCondicao.setOnTouchListener(new View.OnTouchListener() {
					
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						((EditText) v).setSelectAllOnFocus(true);
						((EditText) v).selectAll();
						return false;
					}
				});
				cCondPagamento.moveToPosition(arg1);
				getData().putExtra("CON_PAG_SEQ", cCondPagamento.getInt(0));
				AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
				dialog.setTitle("Digite o valor da condição");
				dialog.setView(edtValorCondicao);
				edtRecebValor.selectAll();
				edtFormaPagamento.requestFocus();
				dialog.setOnShowListener(new DialogInterface.OnShowListener() {
					
					public void onShow(DialogInterface dialog) {
						// TODO Auto-generated method stub
//							try {
//								MenuItem mnuIncluir = menu.findItem(R.id.mnuIncluirReceb);
//								if (df.parse(edtRecebValor.getText().toString()).doubleValue() <= 0.0) {
//									mnuIncluir.setIcon(R.drawable.new_pb);
//								} else {
//									mnuIncluir.setIcon(R.drawable.new_color);
//								}
//							} catch (Exception ignored){}
						edtValorCondicao.selectAll();
						edtValorCondicao.requestFocus();
						edtValorCondicao.postDelayed(new Runnable() {
							@Override
							public void run() {
								InputMethodManager imm = (InputMethodManager) getCFFActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.showSoftInput(edtValorCondicao, 0);
							}
						}, 50);
					}
				});
				dialog.setButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface arg0, int arg1) {
						try{
							GeraPrazoPagamento(edtValorCondicao.getText().toString());
							InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(edtRecebValor.getWindowToken(), 0);
							edtValorCondicao = null;
							getData().putExtra("FOR_PAG_SEQ", 0);
							getData().putExtra("CON_PAG_SEQ", 0);
						}catch(Exception ex){
							new CFFActivityUtil().GeraLogErro(getActivity(), ex, this.getClass().toString());
						}
					}
				});
				dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						
						getData().putExtra("FOR_PAG_SEQ", 0);
						getData().putExtra("CON_PAG_SEQ", 0);
						edtFormaPagamento.setText("");
						edtCondPagamento.setText("");
					}
				});
				dialog.show();
			}
		});
		dialog.show();
		return true;
	}
	

}
