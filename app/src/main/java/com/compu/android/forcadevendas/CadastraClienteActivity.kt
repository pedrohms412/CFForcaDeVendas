package com.compu.android.forcadevendas

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.widget.Toolbar
import com.compu.android.forcadevendas.fragments.FragmentPedidoGeral
import com.compu.android.fragments.CFFFragmentActivity
import com.compu.android.lib.AndroidSocket
import com.compu.android.util.FuncoesAndroid
import com.compu.android.view.CFFEditText
import com.compu.db.CFFCommitFields
import com.compu.lib.funcoes
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import java.text.SimpleDateFormat
import java.util.*


class CadastraClienteActivity : CFFFragmentActivity() {

    var cidSeq = 0

    lateinit var currentMask : MaskType

    lateinit var btnCancelar :Button
    lateinit var btnSalvar :Button
    lateinit var edtCLI_CEP :CFFEditText
    lateinit var edtCLI_NOM_CIDADE :CFFEditText
    lateinit var edtCLI_UF :CFFEditText
    lateinit var edtCLI_NOM :CFFEditText
    lateinit var edtCLI_NOM_FANTASIA :CFFEditText
    lateinit var edtCLI_NUM_CPF_CNPJ :CFFEditText
    lateinit var edtCLI_NOM_LOGRAD :CFFEditText
    lateinit var edtNumLograd :CFFEditText
    lateinit var edtCLI_DSC_COMPL_LOGRAD :CFFEditText
    lateinit var edtCLI_NOM_BAIRRO :CFFEditText
    lateinit var edtCLI_NUM_FONE_COM :CFFEditText
    lateinit var edtCLI_NUM_CELULAR :CFFEditText
    lateinit var edtCLI_NOM_EMAIL :CFFEditText
    lateinit var rgCLI_TIP_PESSOA :RadioGroup

    override fun HandleCallMessage(msg: Message?) { }

    override fun onCreate(arg0: Bundle?) {
        super.onCreate(arg0)
        setContentView(R.layout.cadastra_cliente_activity);

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        InitInterface(arg0, toolbar, "Dados do Cliente", true, false, true, false)

        btnCancelar = findViewById(R.id.btnCancelar)
        btnSalvar = findViewById(R.id.btnSalvar)
        edtCLI_CEP = findViewById(R.id.edtCLI_CEP)
        edtCLI_NOM_CIDADE = findViewById(R.id.edtCLI_NOM_CIDADE)
        edtCLI_UF = findViewById(R.id.edtCLI_UF)
        edtCLI_NOM = findViewById(R.id.edtCLI_NOM)
        edtCLI_NOM_FANTASIA = findViewById(R.id.edtCLI_NOM_FANTASIA)
        edtCLI_NUM_CPF_CNPJ = findViewById(R.id.edtCLI_NUM_CPF_CNPJ)
        edtCLI_NOM_LOGRAD = findViewById(R.id.edtCLI_NOM_LOGRAD)
        edtNumLograd = findViewById(R.id.edtNumLograd)
        edtCLI_DSC_COMPL_LOGRAD = findViewById(R.id.edtCLI_DSC_COMPL_LOGRAD)
        edtCLI_NOM_BAIRRO = findViewById(R.id.edtCLI_NOM_BAIRRO)
        edtCLI_NUM_FONE_COM = findViewById(R.id.edtCLI_NUM_FONE_COM)
        edtCLI_NUM_CELULAR = findViewById(R.id.edtCLI_NUM_CELULAR)
        edtCLI_NOM_EMAIL = findViewById(R.id.edtCLI_NOM_EMAIL)
        rgCLI_TIP_PESSOA = findViewById(R.id.rgCLI_TIP_PESSOA)
        edtCLI_NUM_FONE_COM.addTextChangedListener(insertMaskTel(edtCLI_NUM_FONE_COM))
        edtCLI_NUM_CELULAR.addTextChangedListener(insertMaskTel(edtCLI_NUM_CELULAR))

        val tipPessoa = findViewById<RadioButton>(rgCLI_TIP_PESSOA.checkedRadioButtonId)
        if(Integer.valueOf(tipPessoa.tag.toString()) == 0)
            currentMask = MaskType.CPF
        else currentMask = MaskType.CNPJ

        edtCLI_NUM_CPF_CNPJ.addTextChangedListener(insertMaskCpfCnpj(edtCLI_NUM_CPF_CNPJ, currentMask))

        rgCLI_TIP_PESSOA.setOnCheckedChangeListener { _, _ ->
            val tipPessoaTemp = findViewById<RadioButton>(rgCLI_TIP_PESSOA.checkedRadioButtonId)
            if(Integer.valueOf(tipPessoaTemp.tag.toString()) == 0)
                currentMask = MaskType.CPF
            else currentMask = MaskType.CNPJ
            edtCLI_NUM_CPF_CNPJ.setText("")
        }

        btnCancelar.setOnClickListener {
            setResult(0)
            finishActivity(0)
            finish()
        }

        btnSalvar.setOnClickListener{



            if( edtCLI_NOM.text.toString().trim().isNotEmpty() && edtCLI_NUM_CPF_CNPJ.text.toString().trim().isNotEmpty()){

                var queryVerifica = "SELECT C.CLI_SEQ, C.CLI_NOM, C.CLI_NUM_CPF_CNPJ, C.CLI_NOM_FANTASIA FROM ECF_CLIENTE C WHERE ";

                queryVerifica = "$queryVerifica C.CLI_NUM_CPF_CNPJ = \'${edtCLI_NUM_CPF_CNPJ.text.toString().trim()}\'"
//                if(edtCLI_NUM_CPF_CNPJ.text.toString().trim().isNotEmpty())
//                    queryVerifica = "$queryVerifica C.CLI_NUM_CPF_CNPJ = \'${edtCLI_NUM_CPF_CNPJ.text.toString().trim()}\'"
//                else if(edtCLI_NUM_FONE_RES.text.toString().trim().isNotEmpty())
//                    queryVerifica = "$queryVerifica C.CLI_NUM_FONE_RES = \'${edtCLI_NUM_FONE_RES.text.toString().trim()}\'"
//                else if(edtCLI_NUM_FONE_COM.text.toString().trim().isNotEmpty())
//                    queryVerifica = "$queryVerifica C.CLI_NUM_FONE_COM = \'${edtCLI_NUM_FONE_COM.text.toString().trim()}\'"
//                else if(edtCLI_NUM_CELULAR.text.toString().trim().isNotEmpty())
//                    queryVerifica = "$queryVerifica C.CLI_NUM_CELULAR = \'${edtCLI_NUM_CELULAR.text.toString().trim()}\'"

                GlobalScope.launch(Dispatchers.Main) {

                    try{
                        val androidSocket = app.androidSocket
                        val dsVerificaCliente = withContext(Dispatchers.IO) {
                            androidSocket.getQuery(
                                queryVerifica,
                                1
                            )
                        }

                        var cliSeq = 0

                        if(dsVerificaCliente.count == 0) {
                            if(FuncoesAndroid.isDeviceOnline(this@CadastraClienteActivity)) {
                                val cffCommitFields =
                                    CFFCommitFields("ECF_CLIENTE", CFFCommitFields.INSERT)

                                val cffCommitVendedor =
                                    CFFCommitFields("ECF_CLIENTE_VENDEDOR", CFFCommitFields.INSERT)

                                cliSeq = withContext(Dispatchers.IO) {
                                    FuncoesAndroid.GetSequence(
                                        androidSocket,
                                        "ECF_CLIENTE",
                                        "CLI_SEQ",
                                        null
                                    )
                                }

                                cffCommitFields.Add("CLI_SEQ", cliSeq, CFFCommitFields.INTEGER)
                                cffCommitFields.Add(
                                    "CLI_NOM",
                                    edtCLI_NOM.text.toString().trim(),
                                    CFFCommitFields.STRING
                                )
                                if (edtCLI_NOM_FANTASIA.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NOM_FANTASIA",
                                        edtCLI_NOM_FANTASIA.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_NUM_CPF_CNPJ.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NUM_CPF_CNPJ",
                                        edtCLI_NUM_CPF_CNPJ.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_NOM_LOGRAD.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NOM_LOGRAD",
                                        edtCLI_NOM_LOGRAD.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtNumLograd.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NUM_LOGRAD",
                                        edtNumLograd.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_DSC_COMPL_LOGRAD.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_DSC_COMPL_LOGRAD",
                                        edtCLI_DSC_COMPL_LOGRAD.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_NOM_BAIRRO.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NOM_BAIRRO",
                                        edtCLI_NOM_BAIRRO.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_UF.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_UF",
                                        edtCLI_UF.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_CEP.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_CEP",
                                        edtCLI_CEP.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_NOM_CIDADE.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NOM_CIDADE",
                                        edtCLI_NOM_CIDADE.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (cidSeq > 0)
                                    cffCommitFields.Add("CID_SEQ", cidSeq, CFFCommitFields.INTEGER)
                                if (edtCLI_NUM_FONE_COM.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NUM_FONE_COM",
                                        edtCLI_NUM_FONE_COM.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_NUM_CELULAR.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NUM_CELULAR",
                                        edtCLI_NUM_CELULAR.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                if (edtCLI_NOM_EMAIL.text.toString().trim().isNotEmpty())
                                    cffCommitFields.Add(
                                        "CLI_NOM_EMAIL",
                                        edtCLI_NOM_EMAIL.text.toString().trim(),
                                        CFFCommitFields.STRING
                                    )
                                val tipPessoa = findViewById<RadioButton>(rgCLI_TIP_PESSOA.checkedRadioButtonId)
                                cffCommitFields.Add("CLI_TIP_PESSOA", Integer.valueOf(tipPessoa.tag.toString()), CFFCommitFields.INTEGER)
                                cffCommitFields.Add("CLI_STATUS", 0, CFFCommitFields.INTEGER)
                                cffCommitFields.Add(
                                    "CLI_DAT_MOD",
                                    Calendar.getInstance().time,
                                    CFFCommitFields.DATE
                                )
                                cffCommitFields.Add(
                                    "CLI_DAT_HOR_CAD",
                                    Calendar.getInstance().time,
                                    CFFCommitFields.DATE
                                )
                                cffCommitFields.Add(
                                    "CLI_DAT_CAD",
                                    FuncoesAndroid.parseDate(Calendar.getInstance().time),
                                    CFFCommitFields.DATE
                                )

                                cffCommitVendedor.Add("CLI_SEQ", cliSeq, CFFCommitFields.INTEGER)
                                cffCommitVendedor.Add(
                                    "FUN_SEQ",
                                    MainActivity.preVenda.funSeqVen,
                                    CFFCommitFields.INTEGER
                                )
                                cffCommitVendedor.Add("CVE_IND_PREF", 1, CFFCommitFields.INTEGER)

                                val returnSaved = withContext(Dispatchers.IO) {
                                    androidSocket.CommitTable(
                                        cffCommitFields,
                                        cffCommitVendedor
                                    )
                                }
                                if (returnSaved.contains("ERRO"))
                                    throw Exception(returnSaved)
                            }

                            val intentData = Intent()
                            intentData.putExtra("CLI_SEQ", cliSeq)

                            val cliNom : String = if(edtCLI_NOM_FANTASIA.text.toString().trim().isEmpty()) edtCLI_NOM.text.toString().trim() else edtCLI_NOM_FANTASIA.text.toString().trim()

                            intentData.putExtra("CLI_NOM",  cliNom)

                            CadastraCliente(cliSeq)

                            setResult(FragmentPedidoGeral.REQUEST_CADASTRA_CLIENTE, intentData)
                            finishActivity(FragmentPedidoGeral.REQUEST_CADASTRA_CLIENTE)
                            finish()

                        } else {
                            val intentData = Intent()
                            intentData.putExtra("CLI_SEQ", dsVerificaCliente.getInteger("CLI_SEQ"))
                            intentData.putExtra("CLI_NOM", if(dsVerificaCliente.getString("CLI_NOM_FANTASIA").trim().isEmpty()) dsVerificaCliente.getString("CLI_NOM").toString().trim() else dsVerificaCliente.getString("CLI_NOM_FANTASIA").toString().trim() )

                            setResult(FragmentPedidoGeral.REQUEST_CADASTRA_CLIENTE, intentData)
                            finishActivity(FragmentPedidoGeral.REQUEST_CADASTRA_CLIENTE)
                            finish()

                        }

                    } catch (ex: Exception){

                        val cliente = CadastraCliente(0)

                        val intentData = Intent()

                        intentData.putExtra("CLI_SEQ", cliente.first)
                        intentData.putExtra("CLI_NOM",  cliente.second)

                        setResult(FragmentPedidoGeral.REQUEST_CADASTRA_CLIENTE, intentData)
                        finishActivity(FragmentPedidoGeral.REQUEST_CADASTRA_CLIENTE)
                        finish()

                    }

                }

            }

        }

        val btnBuscaCep = findViewById<ImageButton>(R.id.btnBuscaCep)
        btnBuscaCep.setOnClickListener {
            if(edtCLI_CEP.text.toString().trim().isNotEmpty()){
                GlobalScope.launch(Dispatchers.Main) {
                    try {
                        val cidReturn = withContext(Dispatchers.IO) {
                            getCidadeExterno(app.androidSocket, edtCLI_CEP.text.toString().trim())
                        }
                        cidReturn?.let {
                            cidSeq = cidReturn.cidSeq
                            edtCLI_NOM_CIDADE.setText(cidReturn.cidNom)
                            edtCLI_UF.setText(cidReturn.cidUf)
                            edtCLI_NOM_LOGRAD.setText(cidReturn.lograd)
                            edtNumLograd.requestFocus()
                        }
                    } catch (ex :Exception){
                        CFFMessageDlg("Ocorreu um erro ao tentar carregar o CEP")
                    }

                }
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            setResult(0)
            finishActivity(0)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    suspend fun CadastraCliente(cliSeq :Int): Pair<Int, String>{

        var cliNom : String = if(edtCLI_NOM_FANTASIA.text.toString().trim().isEmpty()) edtCLI_NOM.text.toString().trim() else edtCLI_NOM_FANTASIA.text.toString().trim()
        var cliSeqReturn = cliSeq
        val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale("pt", "BR"))

        val dbPrePedido = app.config!!.db

        val curVerifica = withContext(Dispatchers.IO) {
            dbPrePedido!!.rawQuery("SELECT CLI_SEQ, CLI_NOM FROM CLIENTE WHERE CLI_NUM_CPF_CNPJ = '${edtCLI_NUM_CPF_CNPJ.text.toString().trim()}'", null)
        }

        if(curVerifica.count == 0) {

            val values = ContentValues()

            if (cliSeqReturn == 0) {
                val curMaxCliSeq = withContext(Dispatchers.IO) {
                    dbPrePedido!!.rawQuery(
                        "select ifnull(min(cli_seq), 0) from cliente",
                        null
                    )
                }
                curMaxCliSeq.moveToFirst()
                if(curMaxCliSeq.getInt(0) >= 0) {
                    values.put("CLI_SEQ", -1)
                    cliSeqReturn = -1
                }else {
                    values.put("CLI_SEQ", curMaxCliSeq.getInt(0) - 1)
                    cliSeqReturn = curMaxCliSeq.getInt(0) - 1
                }
            } else {
                values.put("CLI_SEQ", cliSeqReturn)
            }
            values.put("CLI_NOM", edtCLI_NOM.text.toString().trim())
            values.put("CLI_NOM_FANTASIA", edtCLI_NOM_FANTASIA.text.toString().trim())
            values.put("CLI_NUM_CPF_CNPJ", edtCLI_NUM_CPF_CNPJ.text.toString().trim())

            val tipPessoa = findViewById<RadioButton>(rgCLI_TIP_PESSOA.checkedRadioButtonId)
            Integer.valueOf(tipPessoa.tag.toString())

            values.put("CLI_TIP_PESSOA", Integer.valueOf(tipPessoa.tag.toString()))
            values.put("CLI_UF", edtCLI_UF.text.toString().trim())
            values.put("CLI_CEP", edtCLI_CEP.text.toString().trim())
            values.put("CLI_NOM_CIDADE", edtCLI_NOM_CIDADE.text.toString().trim())
            values.put("CLI_NOM_LOGRAD", edtCLI_NOM_LOGRAD.text.toString().trim())
            values.put("CLI_DSC_COMPL_LOGRAD", edtCLI_DSC_COMPL_LOGRAD.text.toString().trim())
            values.put("CLI_NOM_BAIRRO", edtCLI_NOM_BAIRRO.text.toString().trim())
            values.put("CLI_NUM_FONE_COM", edtCLI_NUM_FONE_COM.text.toString().trim())
            values.put("CLI_NOM_EMAIL", edtCLI_NOM_EMAIL.text.toString().trim())
            values.put("CLI_DAT_MOD", sdf.format(Calendar.getInstance().time))
            values.put("CLI_IND_NEG", 0)

            withContext(Dispatchers.IO) {
                dbPrePedido!!.insertOrThrow(
                    "CLIENTE", null,
                    values
                )
            }
        } else {
            curVerifica.moveToFirst()
            cliSeqReturn = curVerifica.getInt(curVerifica.getColumnIndex("CLI_SEQ"))
            cliNom = curVerifica.getString(curVerifica.getColumnIndex("CLI_NOM"))
        }
        return Pair(cliSeqReturn, cliNom)
    }

    @Throws(Exception::class)
    suspend fun getCidadeExterno(androidSocket :AndroidSocket ,cliCep: String) : Cidade?{

            val wsNomCidade = "localidade"
            val wsNomUF = "uf"
            val wsLograd = "logradouro"

            val client = OkHttpClient()

            val request = withContext(Dispatchers.IO ) { Request.Builder().url("http://viacep.com.br/ws/${funcoes.RemoveCharSpecial(cliCep)}/json/").build() }

            val response = withContext(Dispatchers.IO) { client.newCall(request).execute() }

            if(response.isSuccessful){
                val parser = JsonParser()
                val responseBody = response.body()
                if(responseBody != null) {
                    val jsonObject = parser.parse(responseBody.string()).asJsonObject

                    val dsCidade = withContext(Dispatchers.IO) {
                        androidSocket.getQuery(
                            "SELECT C.CID_SEQ, C.CID_NOM, C.CID_UF FROM ECF_CIDADE C WHERE C.CID_UF = \'${
                                jsonObject.getAsJsonPrimitive(wsNomUF).asString
                            }\' AND C.CID_NOM = \'${jsonObject.getAsJsonPrimitive(wsNomCidade).asString}\'",
                            1
                        )
                    }

                    while (dsCidade.getTable(0).next()) {
                        return Cidade(dsCidade.getInteger("CID_SEQ"), dsCidade.getString("CID_NOM"), dsCidade.getString("CID_UF"), jsonObject.getAsJsonPrimitive(wsLograd).asString)
                    }
                }

            }

            return null
    }

    fun insertMaskTel(editText :EditText): TextWatcher {
        return object :TextWatcher {
            //we need to know if the user is erasing or inputing some new character
            private var backspacingFlag = false

            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private var editedFlag = false

            //we need to mark the cursor position and restore it after the edition
            private var cursorComplement = 0
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length - editText.selectionStart
                //we check if the user ir inputing or erasing a character
                backspacingFlag = count > after
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().length == 2) editText.requestFocus()
            }

            override fun afterTextChanged(s: Editable) {
                val string = s.toString()
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                val phone = string.replace("[^\\d]".toRegex(), "")


                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {
                    var maxlenght = phone.length
                    if (maxlenght > 11) maxlenght = 11
                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length > 10 && !backspacingFlag) {
                        editedFlag = true
                        val ans = "(" + phone.substring(0, 2) + ")" + phone.substring(
                            2,
                            7
                        ) + "-" + phone.substring(7, maxlenght)
                        editText.setText(ans)
                        editText.setSelection(editText.text!!.length - cursorComplement)
                    } else if (phone.length == 10 && backspacingFlag) {
                        editedFlag = true
                        val ans = "(" + phone.substring(0, 2) + ")" + phone.substring(
                            2,
                            6
                        ) + "-" + phone.substring(6, maxlenght)
                        editText.setText(ans)
                        editText.setSelection(editText.text!!.length - cursorComplement)
                    } else if (phone.length >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true
                        //here is the core. we substring the raw digits and add the mask as convenient
                        val ans = "(" + phone.substring(0, 2) + ") " + phone.substring(
                            2,
                            6
                        ) + "-" + phone.substring(6)
                        editText.setText(ans)
                        //we deliver the cursor to its original position relative to the end of the string
                        editText.setSelection(editText.text!!.length - cursorComplement)

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length >= 2 && !backspacingFlag) {
                        editedFlag = true
                        val ans = "(" + phone.substring(0, 2) + ") " + phone.substring(2)
                        editText.setText(ans)
                        editText.setSelection(editText.text!!.length - cursorComplement)
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false
                }
            }
        }
    }
    val CPFMask = "###.###.###-##"
    val CNPJMask = "##.###.###/####-##"

    enum class MaskType {
        CPF,
        CNPJ
    }

    fun unmask(s: String): String {
        return s.replace("[^0-9]*".toRegex(), "")
    }

    fun getDefaultMask(str: String): String {
        var defaultMask = CPFMask
        if (str.length == 14){
            defaultMask = CNPJMask
        }
        return CPFMask
    }
    fun insertMaskCpfCnpj(editText: EditText, maskType :MaskType) : TextWatcher {
        return object : TextWatcher {

            var isUpdating = false
            var oldValue = "";

            override fun onTextChanged(s :CharSequence, start :Int, before :Int, count :Int) {
                var value = unmask(s.toString())
                var mask = String()

                mask = when(currentMask){
                    MaskType.CPF -> CPFMask
                    MaskType.CNPJ -> CNPJMask
                    else -> getDefaultMask(value)
                }

                var maskAux = ""
                if (isUpdating) {
                    oldValue = value
                    isUpdating = false
                    return
                }
                var i = 0
                for( m in mask.toCharArray()) {
                    if ((m != '#' && value.length > oldValue.length) || (m != '#' && value.length < oldValue.length && value.length != i)) {
                        maskAux += m
                        continue
                    }
                    try {
                        maskAux += value[i]
                    } catch (e: Exception) {
                        break
                    }
                    i++
                }

                isUpdating = true;
                editText.setText(maskAux);
                editText.setSelection(maskAux.length);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}
        }
    }

    data class Cidade( val cidSeq: Int , val cidNom :String, val cidUf :String, val lograd :String)

}