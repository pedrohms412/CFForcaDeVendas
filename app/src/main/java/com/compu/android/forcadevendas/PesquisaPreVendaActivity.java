package com.compu.android.forcadevendas;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.compu.android.adapter.CFFRecyclerCursorAdapter;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.android.view.CFFTextView;
import com.compu.lib.funcoes;
import com.google.android.material.navigation.NavigationView;

import java.util.Locale;

public class PesquisaPreVendaActivity extends CFFFragmentActivity
                    implements NavigationView.OnNavigationItemSelectedListener{

    private Toolbar toolbar;
    private NavigationView navigationView;
    private RecyclerView rcPesquisa;
    private EditText edtPesquisa;
    private boolean fantasia = false;
    private CFFRecyclerCursorAdapter adapter;
    private boolean efetuandopesquisa;
    private String query;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.pesquisa);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setSubtitle("Pesquisa");

        query = "SELECT P.PVE_SEQ, P.PVE_DAT, P.PVE_VAL_LIQ, " +
                "       P.SYNC_STATUS, PVE_QTD_ITENS, " +
                "       CASE P.SYNC_STATUS " +
                "        WHEN 0 THEN 'INCOMPLETO' " +
                "        WHEN 1 THEN 'NÃO SINCRONIZADO' " +
                "        WHEN 2 THEN 'SINCRONIZADO' " +
                "       END AS STATUS, " +
                "       C.CLI_NOM, C.CLI_NUM_CPF_CNPJ " +
                "  FROM PRE_VENDA P " +
                "         LEFT JOIN CLIENTE C ON ( P.CLI_SEQ = C.CLI_SEQ )";

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        edtPesquisa = (EditText) toolbar.findViewById(R.id.edtPesquisa);
        edtPesquisa.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edtPesquisa.setHint("Buscar Pedido");

        rcPesquisa = (RecyclerView) findViewById(R.id.rcPesquisa);
        rcPesquisa.setLayoutManager(new LinearLayoutManager(this));
        rcPesquisa.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                rcPesquisa.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(rcPesquisa.getWindowToken(), 0);
                    }
                }, 50);
            }
        });

        rcPesquisa.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rcPesquisa.getWindowToken(), 0);
            }
        }, 50);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().add(R.id.pesquisa_cli_transp, R.id.nav_nome, Menu.NONE, "Nome");
        navigationView.getMenu().add(R.id.pesquisa_cli_transp, R.id.nav_fantasia, Menu.NONE, "Fantasia");

        EfetuaPesquisa(edtPesquisa.getText().toString());

        efetuandopesquisa = false;

        edtPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!efetuandopesquisa) {

                    adapter = null;
                    rcPesquisa.setAdapter(null);
                    EfetuaPesquisa(edtPesquisa.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        navigationView.setCheckedItem(R.id.nav_nome);
        fantasia = false;



    }

    private void EfetuaPesquisa(String descricao) {
        String queryLocal = "";
        if(!descricao.equals("")) {
            if (fantasia) {
                queryLocal = String.format(new Locale("pt", "BR"), "%s AND C.CLI_NOM_FANTASIA LIKE(\'%%%s%%\') ORDER BY C.CLI_NOM_FANTASIA", query, descricao);
            } else {
                queryLocal = String.format(new Locale("pt", "BR"), "%s AND C.CLI_NOM LIKE(\'%%%s%%\') ORDER BY C.CLI_NOM", query, descricao);
            }
        } else {
            if(fantasia){
                queryLocal = String.format(new Locale("pt", "BR"), "%s ORDER BY C.CLI_NOM_FANTASIA", query);
            } else {
                queryLocal = String.format(new Locale("pt", "BR"), "%s ORDER BY C.CLI_NOM", query);
            }
        }
        Cursor cCliente = getApp().getDbPrePedido().rawQuery(queryLocal, null);
        if(cCliente.getCount() > 0){
            adapter = new CFFRecyclerCursorAdapter(cCliente, R.layout.record_pesquisa_pre_venda) {
                @Override
                public void SetValues(View v, final int Position) {
                    getCursor().moveToPosition(Position);

                    CFFFragmentActivity.LoadDataSet((ViewGroup) v, getCursor());

                    CFFTextView txvCliNumCpfCnpj = (CFFTextView) v.findViewById(R.id.txvCliNumCpfCnpj);
                    if(getCursor().getString(7) != null)
                        txvCliNumCpfCnpj.setText(funcoes.formataCpfCnpj(getCursor().getString(7)));

                    CardView cardView = (CardView) v.findViewById(R.id.cardview);
                    if(cardView != null)
                        cardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getCursor().moveToPosition(Position);
                                Intent intentResult = new Intent();
                                intentResult.putExtra("codigo", getCursor().getInt(0));
//                                if(fantasia) intentResult.putExtra("campo_descricao", getCursor().getString(2));
//                                else intentResult.putExtra("campo_descricao", getCursor().getString(1));
//                                intentResult.putExtra("desconto_cliente", funcoes.getDoubleValue(getCursor().getDouble(4)));
                                setResult(0, intentResult);
                                finish();
                            }
                        });
                }
            };
            rcPesquisa.setAdapter(adapter);

            rcPesquisa.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rcPesquisa.getWindowToken(), 0);
                }
            });
            TextView nodata = (TextView) findViewById(R.id.nodata);
            if (adapter.getCursor().getCount() > 0) {
                rcPesquisa.setVisibility(View.VISIBLE);
                nodata.setVisibility(View.GONE);
            } else {
                rcPesquisa.setVisibility(View.GONE);
                nodata.setVisibility(View.VISIBLE);
            }
            EditText edtPesquisa = (EditText) findViewById(R.id.edtPesquisa);
            edtPesquisa.setHint("Buscar Pedido");
        } else {
            RecyclerView rcPesquisas = (RecyclerView) findViewById(R.id.rcPesquisa);
            TextView nodata = (TextView) findViewById(R.id.nodata);
            rcPesquisas.setVisibility(View.GONE);
            nodata.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();

        if(itemId == R.id.nav_nome){
            fantasia = false;
        }

        if(itemId == R.id.nav_fantasia){
            fantasia = true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
}
