package com.compu.android.forcadevendas;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;

public class CFFCustomButton extends Button {
	
	private boolean cheked = false;
	private int resourceStyleDown;
	private int resourceStyleUp;
	private boolean checkable = true;
	
	public CFFCustomButton(Context context){
		super(context);
	}
	
	public CFFCustomButton(Context context, AttributeSet attrs){
		super(context, attrs);
	}
		
	public CFFCustomButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public boolean isCheked() {
		return cheked;
	}

	public void setCheked(boolean cheked) {
		this.cheked = cheked;
		if(resourceStyleDown > 0 && resourceStyleUp > 0);
			if(cheked)
				setBackgroundResource(resourceStyleDown);
			else
				setBackgroundResource(resourceStyleUp);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				if(resourceStyleDown > 0 && resourceStyleUp > 0);
					if(checkable)
						setBackgroundResource(resourceStyleDown);
				break;
			case MotionEvent.ACTION_UP:
				if(resourceStyleDown > 0 && resourceStyleUp > 0);
				if(checkable)
					if(isCheked())
						setCheked(false);
					else
						setCheked(true);
				if(isCheked())
					setBackgroundResource(resourceStyleDown);
				else
					setBackgroundResource(resourceStyleUp);
				break;
			case MotionEvent.ACTION_SCROLL:
				if(resourceStyleDown > 0 && resourceStyleUp > 0);
					if(checkable)
						setBackgroundResource(resourceStyleDown);
				break;
		}
		return super.dispatchTouchEvent(event);
	}
		
	@Override
	protected void dispatchSetPressed(boolean pressed) {
		// TODO Auto-generated method stub
		super.dispatchSetPressed(pressed);
		if(resourceStyleDown > 0 && resourceStyleUp > 0);
			if(pressed)
				if(checkable)
					setBackgroundResource(resourceStyleDown);
			else{
				if(isCheked())
					setBackgroundResource(resourceStyleDown);
				else
					setBackgroundResource(resourceStyleUp);
			}
	}

	public int getResourceStyleDown() {
		return resourceStyleDown;
	}

	public void setResourceStyleDown(int resourceStyle) {
		this.resourceStyleDown = resourceStyle;
	}

	public int getResourceStyleUp() {
		return resourceStyleUp;
	}

	public void setResourceStyleUp(int resourceStyle) {
		this.resourceStyleUp = resourceStyle;
	}

	public boolean isCheckable() {
		return checkable;
	}

	public void setCheckable(boolean checkable) {
		this.checkable = checkable;
	}

}
