package com.compu.android.forcadevendas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.compu.android.config.LoginActivity;
import com.compu.android.fragments.CFFFragmentActivity;

import java.util.ArrayList;
import java.util.Locale;

public class LauncherActivity extends CFFFragmentActivity{

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        if(arg0 == null) arg0 = new Bundle();


        arg0.putString(LoginActivity.Companion.getARG_ACTIVITY(), MainActivity.class.getName());
        arg0.putInt(LoginActivity.Companion.getARG_ICON(), com.compu.android.lib.R.mipmap.ic_launcher_forca_de_vendas);
        arg0.putInt(LoginActivity.Companion.getARG_NAO_EXIGE_LOGIN(), 1);

        ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        ArrayList<String> permissions = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED){
                permissions.add(Manifest.permission.GET_ACCOUNTS);
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                permissions.add(Manifest.permission.CAMERA);
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
                permissions.add(Manifest.permission.READ_PHONE_STATE);
            }

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_BOOT_COMPLETED) != PackageManager.PERMISSION_GRANTED){
                permissions.add(Manifest.permission.RECEIVE_BOOT_COMPLETED);
            }

            if(permissions.size() > 0) {
                ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), 0);
                return;
            }

        }

        if (InitConfig(arg0, null)) {
            Intent mainActivity = new Intent(this, MainActivity.class);
            mainActivity.putExtras(arg0);
            startActivity(mainActivity);
        }
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for(int i = 0 ; i < grantResults.length ; i ++){
            if( grantResults[i] != PackageManager.PERMISSION_GRANTED ){
                AlertDialog.Builder dialog = new AlertDialog.Builder(this, com.compu.android.lib.R.style.CFFTheme_FilterDialogTheme);
                dialog.setMessage(String.format(new Locale("pt", "BR"), "Necessária a permissão %s para o programa funcionar corretamente", permissions[i]));
                dialog.setTitle(getString(R.string.msg_sistema));
                dialog.setPositiveButton("OK", (dialog1, which) -> dialog1.dismiss());
                dialog.setOnDismissListener(dialog12 -> finish());
                dialog.show();
                return;
            }
        }
        Bundle parameters = new Bundle();
        parameters.putString(LoginActivity.Companion.getARG_ACTIVITY(), LauncherActivity.class.getName());
        parameters.putInt(LoginActivity.Companion.getARG_ICON(), com.compu.android.lib.R.mipmap.ic_launcher_forca_de_vendas);
        parameters.putInt(LoginActivity.Companion.getARG_NAO_EXIGE_LOGIN(), 1);
        if (InitConfig(parameters, null)) {
            Intent mainActivity = new Intent(this, LoginActivity.class);
            mainActivity.putExtras(parameters);
            startActivity(mainActivity);
        }
        finish();
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

}
