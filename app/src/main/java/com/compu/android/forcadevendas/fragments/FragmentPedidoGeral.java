package com.compu.android.forcadevendas.fragments;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;
import com.compu.android.config.db.CFFDbConfig;
import com.compu.android.config.db.CFFDbPrePedido;
import com.compu.android.forcadevendas.CadastraClienteActivity;
import com.compu.android.forcadevendas.MainActivity;
import com.compu.android.forcadevendas.PesquisaClienteActivity;
import com.compu.android.forcadevendas.PesquisaTransportadorActivity;
import com.compu.android.forcadevendas.R;
import com.compu.android.fragments.CFFFragment;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.android.persistencia.EcfItemPreVenda;
import com.compu.android.view.CFFEditText;
import com.compu.lib.funcoes;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class FragmentPedidoGeral extends CFFFragment {

    private View MainView;

    public static int REQUEST_PESQUISA_CLIENTE = 0;
    public static int REQUEST_PESQUISA_TRANSPORTADOR = 1;

    public static int REQUEST_CADASTRA_CLIENTE = 2;

    private EditText edtTransportador;
    private EditText edtCliente;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setMainView(inflater.inflate(R.layout.pedidogeral, container, false));

        edtTransportador = getMainView().findViewById(R.id.edtTransportador);
        edtCliente = getMainView().findViewById(R.id.edtCliente);

        ImageButton btnCadastraCliente = getMainView().findViewById(R.id.btnCadastraCliente);
        btnCadastraCliente.setOnClickListener ( view -> {
            if(MainActivity.preVenda != null) {
                Intent iCadastraCliente = new Intent(requireContext(), CadastraClienteActivity.class);
                startActivityForResult(iCadastraCliente, REQUEST_CADASTRA_CLIENTE);
            }
        });

        EditText edtObservacao = getMainView().findViewById(R.id.edtObsPedido);
        edtObservacao.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if(MainActivity.preVenda != null) {
                        if (funcoes.getIntValue(MainActivity.preVenda.getSyncStatus()) == 2) {
                            editable.clear();
                        }
                    }
                }catch(Exception ignored){
                    if(ignored.getMessage() != null)
                        Log.e(funcoes.TAG, ignored.getMessage());
                }

            }
        });

        final CFFEditText edtCliente =  getMainView().findViewById(R.id.edtCliente);
        edtCliente.setOnClickListener(v -> PesquisaCliente());

        CFFEditText edtTransportador = getMainView().findViewById(R.id.edtTransportador);
        edtTransportador.setOnClickListener(v -> {
            if(MainActivity.preVenda == null){
                DrawerLayout drawer = getCFFActivity().findViewById(R.id.drawer_layout);
                if(drawer!=null) {
                    Snackbar.make(drawer, "Deve ser iniciado um novo pedido", Snackbar.LENGTH_LONG)
                            .setAction("action", null)
                            .show();

                    drawer.openDrawer(GravityCompat.START);
                }
                return;
            }
            if(MainActivity.preVenda.getSyncStatus() != 2) {
                Intent iTransportador = new Intent(getCFFActivity(),
                    PesquisaTransportadorActivity.class);
                iTransportador.putExtras(getData());
                startActivityForResult(iTransportador, REQUEST_PESQUISA_TRANSPORTADOR);
            }
        });

        EditText edtValorDesconto = getMainView().findViewById(R.id.edtValorDesconto);
        if(edtValorDesconto != null){
            edtValorDesconto.setOnTouchListener((view, motionEvent) -> {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    AlertDialog.Builder alertDesc = new AlertDialog.Builder(getCFFActivity());
                    alertDesc.setItems(new String[]{"Por Porcentagem", "Por Valor"},
                        (dialogInterface, i) -> {
                            EditText edtValorDigitado = new EditText(getActivity());
                            edtValorDigitado.setTag("valor");
                            edtValorDigitado.setKeyListener(DigitsKeyListener.getInstance("0123456789,."));
                            edtValorDigitado.setRawInputType(
                                InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
                            if(MainActivity.preVenda.getPvePerDesc() != null)
                                edtValorDigitado.setText(funcoes.getFloatFormated(MainActivity.preVenda.getPvePerDesc().toString(), 2));
                            else if(MainActivity.preVenda.getPveValDesc() != null)
                                edtValorDigitado.setText(funcoes.getFloatFormated(MainActivity.preVenda.getPveValDesc().toString(), 2));
                            else edtValorDigitado.setText("");
                            edtValorDigitado.selectAll();
                            edtValorDigitado.requestFocus();
                            edtValorDigitado.setOnTouchListener((v, event) -> {
                                ((EditText) v).setSelectAllOnFocus(true);
                                ((EditText) v).selectAll();
                                return false;
                            });
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getCFFActivity());
                            dialog.setTitle("Preço de venda:");
                            dialog.setView(edtValorDigitado);
                            dialog.setPositiveButton("OK",
                                (dialogInterface1, i12) -> {
                                    if(MainActivity.preVenda == null) return;
                                    if(edtValorDigitado.getText().toString().equals("")) return;

                                    Double valDescPreVenda = funcoes.getDoubleValue(edtValorDigitado.getText().toString());

                                    Boolean erro = false;
                                    ArrayList<EcfItemPreVenda> itens = MainActivity.preVenda.getEcfItensPreVenda();

                                    if(i == 0){
                                        valDescPreVenda = (MainActivity.preVenda.getPveValTot() * valDescPreVenda)/100;
                                    }

                                    for(EcfItemPreVenda itemDesc : itens){
                                        Cursor cItem = getCFFApp().getDbPrePedido().rawQuery("SELECT * FROM PRODUTO WHERE PRO_SEQ = "+itemDesc.getProSeq(), null );
                                        if(cItem.getCount() > 0) {
                                            cItem.moveToFirst();
                                            Double diferencaDesconto =  (cItem.getDouble(CFFDbConfig.Companion.getPRODUTO_FIELD_PRO_VAL_MAX_DESC())- itemDesc.getIpvValDesc())*itemDesc.getIpvQtd();
                                            if(diferencaDesconto < 0){
                                                erro = true;
                                                break;
                                            }
                                            Double descontoLocal;
                                            if(valDescPreVenda > diferencaDesconto)
                                                descontoLocal = valDescPreVenda - diferencaDesconto;
                                            else descontoLocal = valDescPreVenda;
                                            valDescPreVenda -= descontoLocal;
                                        }
                                    }

                                    if(valDescPreVenda > 0) erro = true;
                                    if(valDescPreVenda < 0){
                                        dialogInterface.dismiss();
                                        AlertDialog.Builder dialogDesc = new AlertDialog.Builder(getCFFActivity());
                                        dialogDesc.setTitle(R.string.msg_sistema);
                                        dialogDesc.setMessage("Ocorreu um calculando os descontos !");
                                        dialogDesc.setPositiveButton("OK",
                                            (dialogInterfacel, i13) -> dialogInterfacel
                                                .dismiss());
                                        dialogDesc.show();
                                        return;
                                    }

                                    if(erro){
                                        dialogInterface.dismiss();
                                        AlertDialog.Builder dialogDesc = new AlertDialog.Builder(getCFFActivity());
                                        dialogDesc.setTitle(R.string.msg_sistema);
                                        dialogDesc.setMessage("Desconto acima do permitido!");
                                        dialogDesc.setPositiveButton("OK",
                                            (dialogInterfacel, i13) -> dialogInterfacel
                                                .dismiss());
                                        dialogDesc.show();
                                        return;
                                    }

                                    if( i == 1)
                                        MainActivity.preVenda.setPveValDesc(funcoes.getDoubleValue(edtValorDigitado.getText().toString()));
                                    else MainActivity.preVenda.setPveValDesc(funcoes.getDoubleValue((MainActivity.preVenda.getPveValTot() * funcoes.getDoubleValue(edtValorDigitado.getText().toString())/100)));
                                    MainActivity.preVenda.setPveValLiq(MainActivity.preVenda.getPveValTot() - MainActivity.preVenda.getPveValDesc());

                                    CFFDbPrePedido.Companion.CommitPreVenda(getCFFApp().getDbPrePedido(), MainActivity.preVenda);

                                    Cursor cPreVenda = getCFFApp().getDbPrePedido().rawQuery("SELECT * FROM PRE_VENDA WHERE PVE_SEQ = ?", new String[]{String.valueOf(MainActivity.preVenda.getPveSeq())}, null);                                                        cPreVenda.moveToFirst();
                                    cPreVenda.moveToFirst();

                                    CFFFragmentActivity.LoadDataSet(getCFFActivity().findViewById(R.id.content), cPreVenda);

                                });
                            dialog.show();
                        });
                    alertDesc.setTitle(R.string.msg_sistema);
                    alertDesc.show();
                }
                return false;
            });
        }

        return getMainView();
    }

    public void PesquisaCliente() {
        if(MainActivity.preVenda == null){
            DrawerLayout drawer = getCFFActivity().findViewById(R.id.drawer_layout);
            if(drawer!=null) {
                Snackbar.make(drawer, "Deve ser iniciado um novo pedido", Snackbar.LENGTH_LONG)
                        .setAction("action", null)
                        .show();
                drawer.openDrawer(GravityCompat.START);
            }
            return;
        }
        if(MainActivity.preVenda.getSyncStatus() != 2) {
            Intent iCliente = new Intent(getCFFActivity(), PesquisaClienteActivity.class);
            iCliente.putExtras(getData());
            startActivityForResult(iCliente, REQUEST_PESQUISA_CLIENTE);
        }
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    @Override
    protected void initComponents() {

    }

    public static FragmentPedidoGeral newInstance(Bundle args){
        if(args == null)
            args = new Bundle();

        FragmentPedidoGeral fragment = new FragmentPedidoGeral();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(funcoes.TAG, "OnPause");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_PESQUISA_CLIENTE){
            boolean passed = true;
            if(data == null) passed = false;
            else {
                MainActivity.preVenda.setCliSeq(data.getExtras().getInt("codigo"));
                MainActivity.preVenda.setCliNom(data.getExtras().getString("campo_descricao"));
                ContentValues values = new ContentValues();
                values.put("CLI_SEQ", MainActivity.preVenda.getCliSeq());
                values.put("CLI_NOM", MainActivity.preVenda.getCliNom());
                getCFFApp().getDbPrePedido().update("PRE_VENDA", values, "PVE_SEQ = " + MainActivity.preVenda.getPveSeq(), null );
            }
            if(funcoes.getIntValue(MainActivity.preVenda.getCliSeq()) > 0) {
                if(passed && data.getExtras() != null) {
                    edtCliente.setText(data.getExtras().getString("campo_descricao"));
                    EditText edtDesconto = (EditText) MainView.findViewById(R.id.edtDesconto);
                    edtDesconto.setText(funcoes.getFloatFormated(String.valueOf(data.getExtras().getDouble("desconto_cliente", 0.0f)), 0));
                    MainActivity.preVenda.setCliPerDescVenda(data.getExtras().getDouble("desconto_cliente", 0.0f));
                }
            } else passed = false;

            SharedPreferences prefs = getCFFActivity().getSharedPreferences(MainActivity.PREFS, 0);
            if(!passed && (prefs.getInt(MainActivity.PREF_NOVO_PEDIDO, 0) == 1) && (funcoes.getIntValue(MainActivity.preVenda.getCliSeq()) == 0)) {
                Snackbar.make(getMainView(), "Necessário informar um cliente para continuar o lançamento do pedido", Snackbar.LENGTH_LONG)
                        .setAction("action", null)
                        .show();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 0);
                editor.apply();
            }
            if(prefs.getInt(MainActivity.PREF_NOVO_PEDIDO, 0) == 1){
                ViewPager viewPager = (ViewPager) getCFFActivity().findViewById(R.id.viewpager);
                if(viewPager != null && funcoes.getIntValue(MainActivity.preVenda.getCliSeq()) > 0) {
                    viewPager.setCurrentItem(1);
                }
            }
        }

        if(requestCode == REQUEST_PESQUISA_TRANSPORTADOR){
            if(data != null) MainActivity.preVenda.setForSeqTransp(data.getExtras().getInt("codigo"));
            if(funcoes.getIntValue(MainActivity.preVenda.getForSeqTransp()) > 0){
                if(data.getExtras() != null)
                    edtTransportador.setText(data.getExtras().getString("campo_descricao"));
            }
        }

        if(requestCode == REQUEST_CADASTRA_CLIENTE){
            if(resultCode == REQUEST_CADASTRA_CLIENTE){
                Integer cliSeq = data.getIntExtra("CLI_SEQ", 0);
                if(cliSeq != 0){

                    MainActivity.preVenda.setCliSeq(cliSeq);
                    MainActivity.preVenda.setCliNom(data.getStringExtra("CLI_NOM"));

                    edtCliente.setText(MainActivity.preVenda.getCliNom());
                }
            }
        }
    }

    public View getMainView() {
        return MainView;
    }

    public void setMainView(View mainView) {
        MainView = mainView;
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {
//        if(MainActivity.preVenda != null){
//            if(funcoes.getIntValue(MainActivity.preVenda.getSyncStatus()) != 0){
//                getCFFActivity().getMenuInflater().inflate(R.menu.main, menu);
//            }
//        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if(id == R.id.action_send){
//            ContentValues content = new ContentValues();
//            content.put("PVE_EMAIL_ENVIADO", 0);
//            getCFFApp().getDbPrePedido().update("PRE_VENDA", content, "PVE_SEQ = ?", new String[]{MainActivity.preVenda.getPveSeq().toString()});
//        }
        return super.onOptionsItemSelected(item);
    }
}
