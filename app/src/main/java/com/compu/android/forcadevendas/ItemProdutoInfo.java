package com.compu.android.forcadevendas;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.os.StrictMode;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import com.compu.android.config.db.CFFDbPrePedido;
import com.compu.android.fragments.CFFFragmentActivity;

import com.compu.android.lib.NetworkTasks;
import com.compu.android.util.CFFActivityUtil;
import com.compu.android.util.FuncoesAndroid;
import com.compu.db.CFFDataSet;
import com.compu.lib.funcoes;

import java.util.Locale;

public class ItemProdutoInfo extends CFFFragmentActivity {
	
	private Bundle params;
    private ProgressDialog Progress;
    private AsyncBackground backgroundTask;

    private View.OnClickListener onClickImagem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!FuncoesAndroid.isDeviceOnline(ItemProdutoInfo.this)){

                Toast.makeText(ItemProdutoInfo.this, R.string.msg_dispotivo_sem_rede, Toast.LENGTH_SHORT).show();

                return;
            }
            final byte[] imagem = CarregaImagem(true);
            if(imagem != null) {
                LinearLayout layoutTeste = (LinearLayout)getLayoutInflater().inflate(R.layout.layoutdialogimage, null);
                ImageView imgProduto = (ImageView) layoutTeste.findViewById(R.id.imageView1);
                imgProduto.setImageBitmap(BitmapFactory.decodeByteArray(imagem , 0, imagem .length));
                final AlertDialog.Builder dialogImage = new AlertDialog.Builder(ItemProdutoInfo.this);
                dialogImage.setView(layoutTeste);
                dialogImage.setCancelable(true);
                dialogImage.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        ImageView imgProduto = (ImageView) findViewById(R.id.imgProduto);
                        imgProduto.setImageBitmap(BitmapFactory.decodeByteArray(imagem, 0, imagem.length));
                    }
                });
                dialogImage.show();
            }
        }
    };

    private byte[] CarregaImagem(boolean showMessage){
        String qryImagem = String.format("SELECT I.IMG_SEQ, I.IMG_IMAGEM " +
                "  FROM ECF_IMAGEM I " +
                " WHERE I.IMG_SEQ = (SELECT P.IMG_SEQ " +
                "                      FROM ECF_PRODUTO P" +
                "                     WHERE P.PRO_SEQ = %d)", params.getInt("PRO_SEQ"));
        try{

            Cursor CursorImagem = getApp().
                    getDbPrePedido().
                    rawQuery(String.
                            format("SELECT I.IMG_SEQ, I.IMG_IMAGEM " +
                                    "  FROM IMAGEM I " +
                                    " WHERE I.IMG_SEQ = (SELECT P.IMG_SEQ" +
                                    "                      FROM PRODUTO P" +
                                    "                     WHERE P.PRO_SEQ = %d)", params.getInt("PRO_SEQ")),null);
            if(CursorImagem != null){
                if(CursorImagem.getCount() > 0){
                    CursorImagem.moveToNext();
                    return CursorImagem.getBlob(1);
                }
            }
            byte[] bImagem = null;
            CFFDataSet dsImagem = getApp().getAndroidSocket().getQuery(qryImagem, 1);
            if(dsImagem != null){
                if(dsImagem.getCount() > 0){
                    dsImagem.getTable(0).next();
                    getApp().getDbPrePedido().delete("IMAGEM", "IMG_SEQ = " + dsImagem.getInt("IMG_SEQ"), null);
                    bImagem = dsImagem.getBlob("IMG_IMAGEM");
                    ContentValues cImagem = new ContentValues();
                    cImagem.put("IMG_IMAGEM", bImagem);
                    cImagem.put("IMG_SEQ", dsImagem.getInt("IMG_SEQ"));
                    getApp().getDbPrePedido().insertOrThrow("IMAGEM", "", cImagem);
                    return bImagem;
                } else {
                    if(showMessage)
                        CallbackMessage(4);
                }
            }
            return null;
        }catch(Exception ex){
            CallbackMessage(2);
            if(showMessage)
                Log.e(funcoes.TAG, ex.getMessage()==null?"Ocorreu um erro carregando a imagem":ex.getMessage());
            return null;
        }
    }

	@Override
	public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
		setContentView(R.layout.layoutitempedidoinfo);
        getApp().setPrePedido(new CFFDbPrePedido(this, "compumobile.db"));
		initContext();
		initComponents();
	}

    @Override
    protected void HandleCallMessage(Message msg) {
        if(msg.what == 0){
            if(Progress != null){
                Progress.hide();
            }
        }

        if(msg.what == 1){
            if(Progress == null)
                Progress = new ProgressDialog(this);
            if(msg.getData().getString(MSG) != null){
                Progress.setMessage(msg.getData().getString(MSG));
            }
            Progress.show();
        }

        if(msg.what == 2)
            Toast.makeText(this, "Nao foi possivel baixar a imagem do produto", Toast.LENGTH_SHORT).show();

        if(msg.what == 3){
            byte[] bImagem = null;
            try {
                bImagem = backgroundTask.get();
                LinearLayout layoutTeste = (LinearLayout)getLayoutInflater().inflate(R.layout.layoutteste, null);
                ImageView imgProduto = (ImageView) layoutTeste.findViewById(R.id.imageView1);
                imgProduto.setImageBitmap(BitmapFactory.decodeByteArray(bImagem, 0, bImagem.length));
                final Dialog d = new Dialog(ItemProdutoInfo.this);
                d.setTitle("Imagem do produto");
                d.setContentView(layoutTeste);
                d.setCancelable(true);
                imgProduto.setOnClickListener(new ImageView.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        d.hide();
                    }
                });
                d.show();
            } catch (Exception ex) {
                Toast.makeText(this, "Nao foi possivel baixar a imagem do produto", Toast.LENGTH_SHORT).show();
                Log.e(funcoes.TAG, ex.getMessage()==null?"Ocorreu um erro carregando a imagem":ex.getMessage());
            }
        }

        if(msg.what == 4){
            Toast.makeText(this, getString(R.string.msg_nenhum_registro_encontrado), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater menuInflater = new MenuInflater(this);
		menuInflater.inflate(R.menu.itemprodutoinfo, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			onBackPressed();
			break;
		case R.id.mnuSalvarItemPedidoInfo:
			EditText edtObsItemPedidoInfo = (EditText)findViewById(R.id.edtObsItemPedidoInfo);
			getData().putExtra("IPV_OBS", edtObsItemPedidoInfo.getText().toString());
			Intent iResult = new Intent();
			iResult.putExtra("IPV_OBS", getData().getExtras().getString("IPV_OBS"));
			setResult(100, iResult);
			finish();
			break;
        case R.id.mnuImagemItemPedidoInfo:
            backgroundTask = new AsyncBackground();
            backgroundTask.execute();
            break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	private void initContext() {
		// TODO Auto-generated method stub
		params = getIntent().getExtras();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		SQLiteDatabase dbTemp = null;
		try {
            ActionBar bar = getActionBar();
            if (bar != null) {
                bar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowTitleEnabled(true);

            }

            dbTemp = getApp().getDbPrePedido();
            EditText edtProduto = (EditText) findViewById(R.id.edtItemPedidoInfo);
            EditText edtQtdProdutoInfo = (EditText) findViewById(R.id.edtItemQtdInfo);
            EditText edtObsItemPedidoInfo = (EditText) findViewById(R.id.edtObsItemPedidoInfo);
            TextView lblVlrProdutoUnitInfo = (TextView) findViewById(R.id.lblVlrProdutoUnitInfo);
            TextView lblVlrTotalItemPedidoInfo = (TextView) findViewById(R.id.lblVlrTotalItemPedidoInfo);
            TextView lblDepartamentoInfo = (TextView) findViewById(R.id.lblDepartamentoInfo);
            TextView lblSecaoInfo = (TextView) findViewById(R.id.lblSecaoInfo);
            TextView lblQtdEstoqueProdutoInfo = (TextView) findViewById(R.id.lblQtdEstoqueProdutoInfo);
            TextView lblQtdEstoqueGeralProdutoInfo = (TextView) findViewById(R.id.lblQtdEstoqueGeralProdutoInfo);

            edtProduto.setText(String.valueOf(params.getInt("PRO_SEQ")) + " - " + params.getString("PRO_DSC_RESUM"));
            lblVlrProdutoUnitInfo.setText(funcoes.getFloatFormated(String.valueOf(params.getDouble("PRO_PRC_UNIT")), 0));
            if (params.getDouble("IPV_QTD") > 0.0)
                edtQtdProdutoInfo.setText(funcoes.getFloatFormated(String.valueOf(params.getDouble("IPV_QTD")), 1));
            else
                edtQtdProdutoInfo.setText("1");

            if (params.getString("IPV_OBS") != null)
                edtObsItemPedidoInfo.setText(params.getString("IPV_OBS"));

            lblVlrTotalItemPedidoInfo.setText(funcoes.getFloatFormated(String.valueOf(params.getDouble("IPV_VAL_TOT")), 0));

            Cursor cDep = dbTemp.rawQuery("SELECT DEP_DSC FROM DEPARTAMENTO WHERE DEP_SEQ = " + String.valueOf(params.getInt("DEP_SEQ")), null);
            if (cDep.getCount() > 0) {
                cDep.moveToFirst();
                lblDepartamentoInfo.setText(cDep.getString(0));
            }
            Cursor cSec = dbTemp.rawQuery("SELECT SEC_DSC FROM SECAO WHERE SEC_SEQ = " + String.valueOf(params.getInt("SEC_SEQ")), null);
            if (cSec.getCount() > 0) {
                cSec.moveToFirst();
                lblSecaoInfo.setText(cSec.getString(0));
            }

            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
                String qryEstoque = String.format( new Locale("pt", "BR"),
                        "SELECT (SELECT SUM(PES_QTD_EST) FROM ECF_PRODUTO_ESTOQUE WHERE PRO_SEQ = %d) AS ESTOQUE_GERAL, PES_QTD_EST AS ESTOQUE" +
                                "		FROM ECF_PRODUTO_ESTOQUE WHERE PRO_SEQ = %d AND EST_SEQ = %d",
                        params.getInt("PRO_SEQ"),
                        params.getInt("PRO_SEQ"),
                        params.getInt("EST_SEQ"));
                getApp().getAndroidSocket().setTimeout(7000);
                CFFDataSet dsEstoque = getApp().getAndroidSocket().getQuery(qryEstoque, 1);
                if (dsEstoque != null) {
                    if (dsEstoque.getCount() > 0) {
                        dsEstoque.getTable(0).next();
                        lblQtdEstoqueGeralProdutoInfo.setText(dsEstoque.getString("ESTOQUE_GERAL").equals("") ? "0" : funcoes.getFloatFormated(dsEstoque.getString("ESTOQUE_GERAL"), 2));
                        lblQtdEstoqueProdutoInfo.setText(dsEstoque.getString("ESTOQUE").equals("") ? "0" : funcoes.getFloatFormated(dsEstoque.getString("ESTOQUE"), 2));
                    } else {
                        lblQtdEstoqueGeralProdutoInfo.setText("0");
                        lblQtdEstoqueProdutoInfo.setText("0");
                    }
                }

                final ImageView imgProduto = (ImageView) findViewById(R.id.imgProduto);
                imgProduto.setOnClickListener(onClickImagem);

                NetworkTasks taskCarregaImagem = new NetworkTasks(null,
                        new NetworkTasks.InBackgroundListener() {
                            @Override
                            public NetworkTasks.TaskResponse DoInBackground(AsyncTask self, Object... params2) {

                                byte[] imagem = null;
                                Cursor CursorImagem = getApp().
                                        getDbPrePedido().
                                        rawQuery(String.
                                                format("SELECT I.IMG_SEQ, I.IMG_IMAGEM " +
                                                        "  FROM IMAGEM I " +
                                                        " WHERE I.IMG_SEQ = (SELECT P.IMG_SEQ" +
                                                        "                      FROM PRODUTO P" +
                                                        "                     WHERE P.PRO_SEQ = %d)", params.getInt("PRO_SEQ")), null);
                                if (CursorImagem != null) {
                                    if (CursorImagem.getCount() > 0) {
                                        CursorImagem.moveToNext();
                                        imagem = CursorImagem.getBlob(1);
                                    }
                                }
                                byte[] imagemOnline = null;
                                if(FuncoesAndroid.isDeviceOnline(ItemProdutoInfo.this)) {
                                     imagemOnline = CarregaImagem(false);
                                }
                                if(imagemOnline != null) return new NetworkTasks.TaskResponse(imagemOnline, "OK");

                                return new NetworkTasks.TaskResponse(null, "NULL");
                            }
                        },
                        new NetworkTasks.PostExecuteListener() {
                            @Override
                            public void OnPostExecute(NetworkTasks.TaskResponse aVoid, AsyncTask self) {
                                if(aVoid != null) {
                                    if( aVoid.getStatus().toString().equals("OK")) {
                                        byte[] imagem = (byte[]) aVoid.getResponse();
                                        imgProduto.setImageBitmap(BitmapFactory.decodeByteArray(imagem, 0, imagem.length));
                                    }
                                }
                            }
                        });
                taskCarregaImagem.Execute();


            } catch (Exception ex) {

                CFFActivityUtil.GeraLogErro(getBaseContext(), ex, this.getClass().toString());

            } finally {
                dbTemp.close();
            }

        } finally {

        }
		
	}
    
    private class AsyncBackground extends AsyncTask<Void, Void, byte[]>{

        @Override
        protected byte[] doInBackground(Void... params) {
            byte[] imagem = null;
            CallbackMessage("Carregando Imagem", 1);
            imagem = CarregaImagem(true);
            CallbackMessage(0);
            return imagem;
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            super.onPostExecute(bytes);
            CallbackMessage(3);
        }
    }
	
}
