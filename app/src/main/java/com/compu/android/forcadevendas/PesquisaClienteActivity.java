package com.compu.android.forcadevendas;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.compu.android.adapter.CFFRecyclerCursorAdapter;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.lib.funcoes;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

public class PesquisaClienteActivity extends CFFFragmentActivity{
//                    implements NavigationView.OnNavigationItemSelectedListener

    private Toolbar toolbar;
//    private NavigationView navigationView;
    private RecyclerView rcPesquisa;
    private EditText edtPesquisa;
    private boolean fantasia = false;
    private CFFRecyclerCursorAdapter adapter;
    private boolean efetuandopesquisa;
    private Snackbar snackBar;
    private String query;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.pesquisa_cliente);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        toolbar.setSubtitle("Pesquisa");

        query = "SELECT C.CLI_SEQ, " +
                "       C.CLI_NOM, " +
                "       C.CLI_NOM_FANTASIA,  "+
                "       C.CLI_NUM_CPF_CNPJ,  "+
                "       C.CLI_PER_DESC_VENDA, "+
                "       C.CLI_IND_NEG "+
                "  FROM CLIENTE C ";

//        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();

        rcPesquisa = findViewById(R.id.rcPesquisa);
        rcPesquisa.setLayoutManager(new LinearLayoutManager(this));

//        navigationView = findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//        navigationView.getMenu().add(R.id.pesquisa_cli_transp, R.id.nav_nome, Menu.NONE, "Nome");
//        navigationView.getMenu().add(R.id.pesquisa_cli_transp, R.id.nav_fantasia, Menu.NONE, "Fantasia");

        edtPesquisa = toolbar.findViewById(R.id.edtPesquisa);
        edtPesquisa.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        EfetuaPesquisa(edtPesquisa.getText().toString());

        efetuandopesquisa = false;

        edtPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!efetuandopesquisa) {

                    adapter = null;
                    rcPesquisa.setAdapter(null);
                    EfetuaPesquisa(edtPesquisa.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        navigationView.setCheckedItem(R.id.nav_nome);
        fantasia = false;

        Spinner spnFiltro = findViewById(R.id.spnFiltro);

        ArrayAdapter<String> filtroAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new String[]{"RAZÃO SOCIAL", "NOME FANTASIA"});
        spnFiltro.setAdapter(filtroAdapter);

        spnFiltro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                    fantasia = false;
                    edtPesquisa.setText("");
                }else{
                    fantasia = true;
                    edtPesquisa.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void EfetuaPesquisa(String descricao) {
        String queryLocal = "";
        if(!descricao.equals("")) {
            if (fantasia) {
                queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE C.CLI_NOM_FANTASIA LIKE(\'%%%s%%\') ORDER BY C.CLI_NOM_FANTASIA", query, descricao);
            } else {
                queryLocal = String.format(new Locale("pt", "BR"), "%s WHERE C.CLI_NOM LIKE(\'%%%s%%\') ORDER BY C.CLI_NOM", query, descricao);
            }
        } else {
            if(fantasia){
                queryLocal = String.format(new Locale("pt", "BR"), "%s ORDER BY C.CLI_NOM_FANTASIA", query);
            } else {
                queryLocal = String.format(new Locale("pt", "BR"), "%s ORDER BY C.CLI_NOM", query);
            }
        }
        Cursor cCliente = getApp().getDbPrePedido().rawQuery(queryLocal, null);
        if(cCliente.getCount() > 0){
            adapter = new CFFRecyclerCursorAdapter(cCliente, R.layout.record_pesquisa_cliente_transportador) {
                @Override
                public void SetValues(View v, final int Position) {
                    getCursor().moveToPosition(Position);
                    TextView txvTexto1 = v.findViewById(R.id.txvDescricao);
                    TextView txvTexto2 = v.findViewById(R.id.txvValorUnitario);
                    TextView txvTexto3 = v.findViewById(R.id.txvTexto3);
                    if(txvTexto1 != null && getCursor().getString(1) != null)
                        txvTexto1.setText(getCursor().getString(1));
                    if(txvTexto2 != null && getCursor().getString(2) != null)
                        txvTexto2.setText(getCursor().getString(2));
                    if(txvTexto3 != null && getCursor().getString(3) != null)
                        txvTexto3.setText(funcoes.formataCpfCnpj(getCursor().getString(3)));
                    if(getCursor().getInt(5) == 1){
                        if((txvTexto1 != null) && (txvTexto2 != null) && (txvTexto3 != null) ) {
                            txvTexto1.setTextColor(ContextCompat.getColor(v.getContext(), R.color.red));
                            txvTexto2.setTextColor(ContextCompat.getColor(v.getContext(), R.color.red));
                            txvTexto3.setTextColor(ContextCompat.getColor(v.getContext(), R.color.red));
                        }
                    }
                    CardView cardView = v.findViewById(R.id.cardview);
                    if(cardView != null)
                        cardView.setOnClickListener(v1 -> {
                            getCursor().moveToPosition(Position);
                            if(getData().getExtras().getInt("PESQUISA", 0) == 0) {
                                Intent intentResult = new Intent();
                                intentResult.putExtra("codigo", getCursor().getInt(0));
                                if (fantasia)
                                    intentResult
                                        .putExtra("campo_descricao", getCursor().getString(2));
                                else
                                    intentResult
                                        .putExtra("campo_descricao", getCursor().getString(1));
                                intentResult.putExtra("desconto_cliente",
                                    funcoes.getDoubleValue(getCursor().getDouble(4)));
                                setResult(0, intentResult);
                                finish();
                            } else {
                                Intent iDadosCliente = new Intent(PesquisaClienteActivity.this,
                                    DadosClienteActivity.class);
                                iDadosCliente.putExtra("CLI_SEQ", getCursor().getInt(0));
                                startActivity(iDadosCliente);
                            }
                        });
                }
            };
            rcPesquisa.setAdapter(adapter);

            rcPesquisa.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rcPesquisa.getWindowToken(), 0);
                }
            });
            TextView nodata = findViewById(R.id.nodata);
            if (adapter.getCursor().getCount() > 0) {
                rcPesquisa.setVisibility(View.VISIBLE);
                nodata.setVisibility(View.GONE);
            } else {
                rcPesquisa.setVisibility(View.GONE);
                nodata.setVisibility(View.VISIBLE);
            }
            EditText edtPesquisa = findViewById(R.id.edtPesquisa);
            edtPesquisa.setHint("Buscar Cliente");
        } else {
            RecyclerView rcPesquisas = findViewById(R.id.rcPesquisa);
            TextView nodata = findViewById(R.id.nodata);
            rcPesquisas.setVisibility(View.GONE);
            nodata.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }


//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//
//        int itemId = item.getItemId();
//
//        if(itemId == R.id.nav_nome){
//            fantasia = false;
//        }
//
//        if(itemId == R.id.nav_fantasia){
//            fantasia = true;
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//
//        return true;
//    }
}
