package com.compu.android.forcadevendas;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import com.compu.android.adapter.CFFQueryAdapter;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.android.lib.NetworkTasks;
import com.compu.db.CFFDataSet;
import com.compu.lib.funcoes;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Locale;


public class ProdutoInfoActivity extends CFFFragmentActivity {

    private int proSeq;
    private ProgressDialog progressBar;
    private String query = "SELECT P.PRO_SEQ, P.PRO_DSC_RESUM, P.PRO_DSC_COMPL, P.PRO_COD_BARRA, " +
            "       ISNULL(PP.PRO_PRC_VENDA, 0) AS PRO_PRC_VENDA, PP.PRO_PRC_UNIT, D.DEP_DSC, S.SEC_DSC, I.IMG_IMAGEM, PP.PRO_TIP_PRECO " +
            "  FROM ECF_PRODUTO P " +
            "         LEFT JOIN VIEW_PRECO_PRODUTO PP WITH (NOLOCK) ON (PP.PRO_SEQ = P.PRO_SEQ AND PP.EMP_SEQ = %d )" +
            "         LEFT JOIN ECF_IMAGEM I WITH (NOLOCK) ON (I.IMG_SEQ = P.IMG_SEQ), " +
            "       ECF_DEPARTAMENTO D, " +
            "       ECF_SECAO S " +
            " WHERE P.DEP_SEQ = D.DEP_SEQ " +
            "   AND P.SEC_SEQ = S.SEC_SEQ ";

    private String qryEstoque = "SELECT E.EST_SEQ, E.EST_DSC, " +
            "       ( SELECT P.PRO_SEQ " +
            "           FROM ECF_PRODUTO P      " +
            "          WHERE P.PRO_SEQ = %d) AS PRO_SEQ, " +
            "       ISNULL(( SELECT EP.PES_QTD_EST_PRO " +
            "                  FROM VIEW_ESTOQUE_PRODUTO EP " +
            "                 WHERE EP.PRO_SEQ = %d AND EP.EST_SEQ = E.EST_SEQ ) , 0 ) AS PES_QTD_EST_PRO,  " +
            "       ISNULL(( SELECT EP.PES_QTD_EST_FRA " +
            "                  FROM VIEW_ESTOQUE_PRODUTO EP " +
            "                 WHERE EP.PRO_SEQ = %d AND EP.EST_SEQ = E.EST_SEQ ) , 0 ) AS PES_QTD_EST_FRA,  " +
            "       ISNULL(( SELECT EP.PRO_FAT_CONVERSAO " +
            "                  FROM VIEW_ESTOQUE_PRODUTO EP " +
            "                 WHERE EP.PRO_SEQ = %d AND EP.EST_SEQ = E.EST_SEQ ) , 0 ) AS PRO_FAT_CONVERSAO  " +
            "  FROM ECF_ESTOQUE E ";


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.layout_produto_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setNavigationIcon(com.compu.android.lib.R.mipmap.ic_launcher_consultaproduto);
        InitInterface(arg0, toolbar, "Informações do produto", true, false, false, false);

        proSeq = getIntent().getExtras().getInt("PRO_SEQ");

        progressBar = new ProgressDialog(this);
        progressBar.setIndeterminate(true);
        progressBar.setMessage(getString(R.string.msg_aguarde_carregando_dados));

        ImageView imgProduto = findViewById(R.id.imgProduto);

        int height = (getResources().getDisplayMetrics().heightPixels / 3);

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) imgProduto.getLayoutParams();
        params.height = height;
        imgProduto.setLayoutParams(params);

        NetworkTasks taskProduto = new NetworkTasks(
            self -> {
                if (!progressBar.isShowing())
                    progressBar.show();
            },
            (self, params12) -> {

                try {
                    CFFDataSet dsProdutoInfo = getApp().getAndroidSocket().getQuery(String.format(new Locale("pt","BR"), query, getIntent().getExtras().getInt("EMP_SEQ")) + " AND P.PRO_SEQ = " + proSeq, 1);
                    return new NetworkTasks.TaskResponse(dsProdutoInfo, "OK");

                } catch (Exception ex) {
                    return new NetworkTasks.TaskResponse(null, funcoes.getStringValue(ex.getMessage()));
                }
            },
            (aVoid, self) -> {
                if (aVoid.getResponse() != null) {
                    CFFDataSet dsResultado = (CFFDataSet) aVoid.getResponse();
                    if (dsResultado.getCount() > 0) {
                        TextView txvDescricao = findViewById(R.id.txvDescricao);
                        TextView txvDescricaoCompl = findViewById(R.id.txvDescricaoCompl);
                        TextView txvCodBarra = findViewById(R.id.txvCodBarra);
                        TextView txvPreco = findViewById(R.id.txvPreco);
                        TextView txvUnitario = findViewById(R.id.txvUnitario);
                        TextView txvDepartamento = findViewById(R.id.txvDepartamento);
                        TextView txvSecao = findViewById(R.id.txvSecao);


                        if (dsResultado.getBlob("IMG_IMAGEM") != null) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(dsResultado.getBlob("IMG_IMAGEM"), 0, dsResultado.getBlob("IMG_IMAGEM").length);
                            imgProduto.setImageBitmap(bitmap);
                        }

                        txvDescricao.setText(dsResultado.getString("PRO_DSC_RESUM"));
                        txvDescricaoCompl.setText(dsResultado.getString("PRO_DSC_COMPL"));
                        txvCodBarra.setText(dsResultado.getString("PRO_COD_BARRA"));
                        txvDepartamento.setText(dsResultado.getString("DEP_DSC"));
                        txvSecao.setText(dsResultado.getString("SEC_DSC"));
                        if (dsResultado.getInt("PRO_TIP_PRECO") == 0) {
                            txvUnitario.setVisibility(View.INVISIBLE);
                        } else {
                            txvUnitario.setVisibility(View.VISIBLE);
                            txvUnitario.setText(String.format(new Locale("pt", "BR"), "Preço Original:  R$ %.2f", dsResultado.getDouble("PRO_PRC_UNIT")));
                            txvPreco.setTextColor(ContextCompat.getColor(ProdutoInfoActivity.this, R.color.steelBlueDark));
                        }
                        txvPreco.setText(String.format(new Locale("pt", "BR"), "R$ %.2f", dsResultado.getDouble("PRO_PRC_VENDA")));

                        NetworkTasks taskEstoque = new NetworkTasks(
                            (self1, params1) -> {

                                try {
                                    return new NetworkTasks.TaskResponse(getApp().getAndroidSocket().getQuery(String.format(new Locale("pt", "BR"), qryEstoque, proSeq, proSeq, proSeq, proSeq), 1), "OK");
                                } catch (Exception ignored) {
                                    return new NetworkTasks.TaskResponse(null, funcoes.getStringValue(ignored.getMessage()));
                                }
                            },
                            (aVoid1, self12) -> {

                                if (aVoid1.getResponse() != null) {
                                    CFFDataSet dsResultado1 = (CFFDataSet) aVoid1.getResponse();
                                    if (dsResultado1.getCount() > 0) {

                                        ListView lstEstoque = findViewById(R.id.lstEstoque);
                                        lstEstoque.setDivider(null);
                                        lstEstoque.setDividerHeight(0);
                                        lstEstoque.addHeaderView(getLayoutInflater().inflate(R.layout.record_produto_info_estoque_cabecalho, null, false));
                                        CFFQueryAdapter adapter = new CFFQueryAdapter(ProdutoInfoActivity.this,
                                            dsResultado1, R.layout.record_produto_info_estoque) {
                                            @Override
                                            public void SetValues(View view, int position) {

                                                TextView txvEstDsc = view.findViewById(R.id.txvEstDsc);
                                                TextView txvQtd = view.findViewById(R.id.txvQtd);
                                                TextView txvQtdEmb = view.findViewById(R.id.txvQtdEmb);
                                                TextView txvQtdFrac = view.findViewById(R.id.txvQtdFrac);

                                                if (txvEstDsc != null)
                                                    txvEstDsc.setText(getDsQuery().getString("EST_DSC"));
                                                if (txvQtd != null) txvQtd.setText(
                                                        funcoes.alinhaTexto(String.format(new Locale("pt", "BR"), "%.3f", getDsQuery().getDouble("PES_QTD_EST_PRO")), 11, "D"));
                                                if (txvQtdEmb != null)
                                                    txvQtdEmb.setText(
                                                            funcoes.alinhaTexto(String.format(new Locale("pt", "BR"), "%.3f", getDsQuery().getDouble("PES_QTD_EST_FRA")), 11, "D"));
                                                if (txvQtdFrac != null)
                                                    txvQtdFrac.setText(
                                                            funcoes.alinhaTexto(String.format(new Locale("pt", "BR"), "%.3f", getDsQuery().getDouble("PRO_FAT_CONVERSAO")), 11, "D"));

                                            }
                                        };
                                        lstEstoque.setAdapter(adapter);

                                    }
                                } else {
                                    finish();
                                }

                                if (progressBar.isShowing())
                                    progressBar.hide();

                                NestedScrollView scrollView = findViewById(R.id.nestedScrollView);
                                scrollView.smoothScrollTo(0,0);

                            });
                        taskEstoque.Execute();



                    }
                }

            });
        taskProduto.Execute();
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) finish();

        return super.onOptionsItemSelected(item);
    }

}

