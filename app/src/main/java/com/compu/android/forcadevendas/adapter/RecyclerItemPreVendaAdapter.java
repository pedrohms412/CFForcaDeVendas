package com.compu.android.forcadevendas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;
import com.compu.android.persistencia.EcfItemPreVenda;

import java.util.ArrayList;


public abstract class RecyclerItemPreVendaAdapter extends RecyclerView.Adapter<RecyclerItemPreVendaAdapter.AdapterHolder> {

    private ArrayList<EcfItemPreVenda> mItensPreVenda;
    private @LayoutRes int mLayoutRes;

    public abstract void SetValues(RecyclerItemPreVendaAdapter.AdapterHolder holder, int Position);

    public RecyclerItemPreVendaAdapter (ArrayList<EcfItemPreVenda> itensPreVenda, @LayoutRes int layoutRes){
        setLayoutRes(layoutRes);
        setItensPreVenda(itensPreVenda);
    }

    @Override
    public RecyclerItemPreVendaAdapter.AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false);

        return new RecyclerItemPreVendaAdapter.AdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterHolder holder, int position) {
        if(holder.getRootView() != null) {
            if(getItensPreVenda().size() > 0) {
                holder.setHolderPosition(position);
                SetValues(holder, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        return getItensPreVenda().size();
    }

    public ArrayList<EcfItemPreVenda> getItensPreVenda() {
        return mItensPreVenda;
    }

    public void setItensPreVenda(ArrayList<EcfItemPreVenda> mItensPreVenda) {
        this.mItensPreVenda = mItensPreVenda;
    }

    public int getLayoutRes() {
        return mLayoutRes;
    }

    public void setLayoutRes(int mLayoutRes) {
        this.mLayoutRes = mLayoutRes;
    }

    public class AdapterHolder extends RecyclerView.ViewHolder{

        private View mRootView;
        private int mHolderPosition;

        public AdapterHolder(View itemView) {
            super(itemView);
            setRootView(itemView);        }

        public View getRootView() {
            return mRootView;
        }

        private void setRootView(View mRootView) {
            this.mRootView = mRootView;
        }

        public int getHolderPosition() {
            return mHolderPosition;
        }

        public void setHolderPosition(int mHolderPosition) {
            this.mHolderPosition = mHolderPosition;
        }
    }

}
