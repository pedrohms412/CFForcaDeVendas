package com.compu.android.forcadevendas;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Message;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.compu.android.adapter.CFFCursorAdapter;
import com.compu.android.adapter.CFFFragmentPagerAdapter;
import com.compu.android.adapter.CFFRecyclerRowAdapter;
import com.compu.android.adapter.ItemAdapter2;
import com.compu.android.adapter.ListTextObjectAdapter2;
import com.compu.android.application.CFFApplicationContext;
import com.compu.android.config.LoginActivity;
import com.compu.android.config.db.CFFDbConfig;
import com.compu.android.config.db.CFFDbFuncoes;
import com.compu.android.config.db.CFFDbPrePedido;
import com.compu.android.forcadevendas.fragments.FragmentItemPedido;
import com.compu.android.forcadevendas.fragments.FragmentPedidoGeral;
import com.compu.android.forcadevendas.fragments.FragmentRecebimento;
import com.compu.android.fragments.CFFDialogFragment;
import com.compu.android.fragments.CFFFragmentActivity;
import com.compu.android.interfaces.CFFDialogFragmentListener;
import com.compu.android.lib.AndroidSocket;
import com.compu.android.lib.NetworkTasks;
import com.compu.android.persistencia.EcfFormaCondPagamPreVenda;
import com.compu.android.persistencia.EcfItemPreVenda;
import com.compu.android.persistencia.EcfPreVenda;
import com.compu.android.util.CFFActivityUtil;
import com.compu.android.util.FuncoesAndroid;
import com.compu.android.view.CFFEditText;
import com.compu.db.CFFCommitFields;
import com.compu.db.CFFDataSet;
import com.compu.lib.exception.CFFClientException;
import com.compu.lib.funcoes;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.GmailScopes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class MainActivity extends CFFFragmentActivity
    implements NavigationView.OnNavigationItemSelectedListener {

    public ViewPager viewPager;
    public NavigationView navigationView;
    public Toolbar toolbar;
    private FragmentPedidoGeral pedidoGeral;
    private FragmentRecebimento recebimento;
    private FragmentItemPedido itemPedido;


    public static EcfPreVenda preVenda;
    public static String PREFS = "prevenda";
    public static String PREF_NOVO_PEDIDO = "PREF_NOVO_PEDIDO";
    public static String PREF_ENVIA_EMAIL = "PREF_ENVIA_EMAIL";
//    public static int Empresa = 0;
//    public static int Terminal = 0;
//    public static int Estoque = 0;

    private final int REQUEST_INIT_EMAIL = 2100;
    private final int REQUEST_SEND_EMAIL = 2000;
    private final int REQUEST_COMPARTILHAR = 9595;

    private final String[] tableNames = new String[]{"PRE_VENDA",
        "ITEM_PRE_VENDA", "FORMA_PAGAMENTO", "CONDICAO_PAGAMENTO",
        "CLIENTE", "PRODUTO", "TRANSPORTADOR", "USUARIO",
        "PRECO_PRODUTO_CLIENTE", "FORMA_COND_PAGAM_CLIENTE",
        "FORMA_COND_PAGAM_PRE_VENDA", "PRAZO_PAGAMENTO",
        "IMAGEM", "DEPARTAMENTO", "SECAO"};
    public static String templateEmailCorpo =
        "<html> <head><title>Aguardando Pedido Móvel</title>    <meta charset=\"utf-8\"></head> " +
            "  <body> " +
            "    <table style=\"margin-bottom:3px;text-align:center\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"540\"> "
            +
            "      <tbody> " +
            "      <tr align=\"center\" bgcolor=\"#ffffff\"> " +
            "       <br> " +
            "       <td style=\"font-family:Verdana,Geneva,sans-serif;font-size:20px;font-weight:bold;color:rgb(0,0,0)\" bgcolor=\"#ffffff\">Pedido Móvel</td> "
            +
            "      </tr> " +
            "       </tbody> " +
            "    </table> " +
            "    <table style=\"border:10px solid rgb(227,236,240)\" align=\"center\" bgcolor=\"#e3ecf0\" cellpadding=\"0\" cellspacing=\"0\" width=\"540\"> "
            +
            " <tbody> " +
            "  <tr align=\"left\">  " +
            "<td style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\"><table style=\"border:10px solid rgb(227,236,240)\" align=\"center\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" height=\"auto\" width=\"540\">  "
            +
            "  <tbody>  " +
            "<tr align=\"left\">  " +
            "  <td style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\"> <br>"
            +
            "  Pedido de: [CLIENTE],<br> " +
            " <br> " +
            "  </td> " +
            "</tr> " +
            "<tr align=\"left\" bgcolor=\"#ffffff\"> " +
            "  <td style=\"padding:5px;font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\"><strong style=\"font-size:15px;font-weight:bold;color:#990000;color:rgb(51,51,51)\">Dados do Pedido</strong><br> "
            +
            "<table width=\"100%\" cellpadding=\"1\" style=\"margin-top:5px\"> " +
            "  <tbody><tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" width=\"132\" align=\"left\">&nbsp;Vendedor:</td> " +
            "<td width=\"366\"><strong>&nbsp;[FUN_SEQ_VEN] </strong></td> " +
            "  </tr> " +
            "  <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" align=\"left\">&nbsp;Data Pedido:</td> " +
            "<td>&nbsp;<strong>[PVE_DAT]</strong></td> " +
            "  </tr> " +
            "  <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" align=\"left\">&nbsp;Cliente:</td> " +
            "<td>&nbsp;<strong>[CLI_NOM]</strong></td> " +
            "  </tr> " +
            "  <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" align=\"left\">&nbsp;Quantidade de Itens:</td> " +
            "<td>&nbsp;<strong>[PVE_QTD_ITENS]</strong></td> " +
            "  </tr> " +
            "  <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" align=\"left\">&nbsp;Valor Bruto:</td> " +
            "<td>&nbsp;<strong>[PVE_VAL_TOT]</strong></td> " +
            "  </tr> " +
            "  <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" align=\"left\">&nbsp;Valor Desconto:</td> " +
            "<td>&nbsp;<strong>[PVE_VAL_DESC]</strong></td> " +
            "  </tr> " +
            "  <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "<td height=\"25\" align=\"left\">&nbsp;Valor Liquido:</td> " +
            "<td>&nbsp;<strong>[PVE_VAL_LIQ]</strong></td> " +
            "  </tr> " +
            "</tbody></table><br> " +
            "   </td> " +
            "   </tr> " +
            "  </tbody> " +
            "  <tbody>";

    public static String templateEmailRodape =
        "   <tr>" +
            "    <td style=\"font-family:Verdana,Geneva,sans-serif;font-size:11px;color:rgb(51,51,51)\"> "
            +
            "     ESTE É UM E-MAIL AUTOMÁTICO, FAVOR NÃO RESPONDER <br> " +
            " </td>  " +
            "</tr></tbody></table></td></tr> " +
            "</tbody></table> " +
            "</body> " +
            "</html>";

    private static String templateItemDetalheEmail =
        "          <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "           <td height=\"25\" width=\"366\" align=\"left\"><strong>&nbsp;[PRO_DSC_RESUM]</strong></td> "
            +
            "           <td width=\"132\"><strong>&nbsp;[IPV_QTD] </strong></td>" +
            "           <td width=\"132\"><strong>&nbsp;[IPV_VAL_LIQ] </strong></td>" +
            "          </tr>\n";


    private static String templateItemEmailCabecalho =
        "<table style=\"border:10px solid rgb(227,236,240)\" align=\"center\" bgcolor=\"#e3ecf0\" cellpadding=\"0\" cellspacing=\"0\" width=\"540\"> "
            +
            " <tbody> " +
//			"  <tr align=\"left\">  " +
//			"   <td style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\">"+
//			"    <table style=\"border:10px solid rgb(227,236,240)\" align=\"center\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" height=\"auto\" width=\"540\">  " +
//			"     <tbody>  " +
//			"      <tr align=\"left\">  " +
//			"       <td style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\"> <br>" +
//			"        Aguardando pedido do cliente: [CLIENTE],<br><br> " +
//			"       </td> " +
//			"      </tr> " +
            "      <tr align=\"left\" bgcolor=\"#ffffff\"> " +
            "       <td style=\"padding:5px;font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\"><strong style=\"font-size:15px;font-weight:bold;color:#990000;color:rgb(51,51,51)\">Itens</strong><br> "
            +
            "        <table width=\"100%\" cellpadding=\"1\" style=\"margin-top:5px\"> " +
            "         <tbody> " +
            "          <tr style=\"font-family:Verdana,Geneva,sans-serif;font-size:13px;color:rgb(51,51,51)\" bgcolor=\"#add8e6\"> "
            +
            "           <td height=\"25\" width=\"366\" align=\"left\"><strong>&nbsp;Descrição</strong></td> "
            +
            "           <td width=\"132\"><strong>&nbsp;Quantidade </strong></td>" +
            "           <td width=\"132\"><strong>&nbsp;Valor Total </strong></td>" +
            "          </tr>\n";

    private static String templateItemEmailRodape =
        "         </tbody> " +
            "        </table><br> " +
            "       </td> " +
            "      </tr> " +
//			"     </tbody> " +
//			"    </table>"+
//			"   </td>"+
//			"  </tr> " +
            " </tbody>" +
            "</table> ";

    NetworkTasks taskMail = new NetworkTasks(null,
        (self, p) -> {

            if (!FuncoesAndroid.isDeviceOnline(MainActivity.this)) {
                return new NetworkTasks.TaskResponse(null, "Dispositivo offline");
            }

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                new Locale("pt", "BR"));

            boolean enviaemail = false;

            Cursor cParametro = getApp().getDbPrePedido().rawQuery(
                "SELECT * FROM PARAMETRO_EMPRESA where PEM_NOM_PARAM = \'EMP_PAR_11701_UTILIZAR_ENVIO_AUTOMATICO\'",
                null);

            if (cParametro.getCount() > 0) {
                cParametro.moveToFirst();
                if (funcoes.getStringValue(
                    cParametro.getString(CFFDbConfig.Companion.getPARAMETRO_FIELD_PEM_VAL_PARAM()))
                    .toUpperCase().equals("SIM")) {
                    enviaemail = true;
                }
            }

            try {
                CFFDataSet dsParametro = getApp().getAndroidSocket().getQuery(
                    String.format(new Locale("pt", "BR"),
                        "SELECT * FROM ECF_PARAMETRO_EMPRESA WHERE PEM_NOM_PARAM = \'EMP_PAR_11701_UTILIZAR_ENVIO_AUTOMATICO\' AND EMP_SEQ = %d",getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_EMPRESA(),1)), 1);

                if (dsParametro.getCount() > 0) {

                    if (cParametro.getCount() == 0) {

                        ContentValues values = new ContentValues();
                        values.put("EMP_SEQ", dsParametro.getInt("EMP_SEQ"));
                        values.put("PEM_SEQ", dsParametro.getInt("PEM_SEQ"));
                        values.put("PEM_NOM_PARAM", dsParametro.getString("PEM_NOM_PARAM"));
                        values.put("PEM_VAL_PARAM", dsParametro.getString("PEM_VAL_PARAM"));
                        values.put("DAT_MOD", sdf.format(new Date()));

                        getApp().getDbPrePedido()
                            .insert(CFFDbConfig.Companion.getTABLE_PARAMETRO(), null, values);

                    } else {
                        ContentValues values = new ContentValues();
                        values.put("PEM_VAL_PARAM", dsParametro.getString("PEM_VAL_PARAM"));
                        values.put("DAT_MOD", sdf.format(new Date()));
                        getApp().getDbPrePedido()
                            .update(CFFDbConfig.Companion.getTABLE_PARAMETRO(), values,
                                String.format(new Locale("pt", "BR"),
                                    "PEM_SEQ = %d",
                                    dsParametro.getInt("PEM_SEQ")),
                                null);
                    }

                    if (funcoes.getStringValue(dsParametro.getString("PEM_VAL_PARAM")).toUpperCase()
                        .equals("SIM")) {
                        enviaemail = true;
                    }

                }
            } catch (CFFClientException ex) {
                if (cParametro.getCount() == 0) {
                    return new NetworkTasks.TaskResponse(null,
                        "nao existem parapemetros cadastrados");
                }
            }

            if (enviaemail) {

                SharedPreferences prefs = getSharedPreferences(AccountManager.KEY_ACCOUNT_NAME,
                    MODE_PRIVATE);

                if (!prefs.getString(AccountManager.KEY_ACCOUNT_NAME, "").equals("")) {
                    String[] SCOPES = new String[]{GmailScopes.GMAIL_SEND};
                    GoogleAccountCredential mCredential = GoogleAccountCredential.usingOAuth2(
                        getApplicationContext(), Arrays.asList(SCOPES))
                        .setBackOff(new ExponentialBackOff());

                    mCredential.setSelectedAccountName(
                        prefs.getString(AccountManager.KEY_ACCOUNT_NAME, ""));

                    try {
                        HttpTransport transport = AndroidHttp.newCompatibleTransport();
                        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
                        com.google.api.services.gmail.Gmail mService = new com.google.api.services.gmail.Gmail.Builder(
                            transport, jsonFactory, mCredential)
                            .setApplicationName("com.compu.android.forcadevendas")
                            .build();

                        Properties props = new Properties();

                        Session session = Session.getDefaultInstance(props, null);

                        String emailDestino = "";

                        cParametro = getApp().getDbPrePedido().rawQuery(
                            "SELECT * FROM PARAMETRO_EMPRESA where PEM_NOM_PARAM = \'EMP_PAR_11711_EMAIL_DESTINATARIO\'",
                            null);
                        if (cParametro.getCount() > 0) {
                            cParametro.moveToFirst();
                            emailDestino = cParametro.getString(
                                CFFDbConfig.Companion.getPARAMETRO_FIELD_PEM_VAL_PARAM());
                        }
                        try {
                            CFFDataSet dsParametro = getApp().getAndroidSocket().getQuery(
                                String.format(new Locale("pt", "BR"),
                                    "SELECT * FROM ECF_PARAMETRO_EMPRESA WHERE PEM_NOM_PARAM = \'EMP_PAR_11711_EMAIL_DESTINATARIO\' AND EMP_SEQ = %d", getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_EMPRESA(),1)),
                                1);

                            if (dsParametro.getCount() > 0) {

                                if (cParametro.getCount() == 0) {

                                    ContentValues values = new ContentValues();
                                    values.put("EMP_SEQ", dsParametro.getInt("EMP_SEQ"));
                                    values.put("PEM_SEQ", dsParametro.getInt("PEM_SEQ"));
                                    values.put("PEM_NOM_PARAM",
                                        dsParametro.getString("PEM_NOM_PARAM"));
                                    values.put("PEM_VAL_PARAM",
                                        dsParametro.getString("PEM_VAL_PARAM"));
                                    values.put("DAT_MOD", sdf.format(new Date()));

                                    getApp().getDbPrePedido()
                                        .insert(CFFDbConfig.Companion.getTABLE_PARAMETRO(), null,
                                            values);

                                } else {
                                    ContentValues values = new ContentValues();
                                    values.put("PEM_VAL_PARAM",
                                        dsParametro.getString("PEM_VAL_PARAM"));
                                    values.put("DAT_MOD", sdf.format(new Date()));
                                    getApp().getDbPrePedido()
                                        .update(CFFDbConfig.Companion.getTABLE_PARAMETRO(), values,
                                            String.format(new Locale("pt", "BR"),
                                                "PEM_SEQ = %d",
                                                dsParametro.getInt("PEM_SEQ")),
                                            null);
                                }

                                emailDestino = dsParametro.getString("PEM_VAL_PARAM");

//                                if (emailDestino.equals("")) {
//                                    return new NetworkTasks.TaskResponse(null,
//                                        "sem email de destino cadastrado");
//                                }
                            }
                        } catch (CFFClientException ex) {
//                            if (emailDestino.equals("")) {
//                                return new NetworkTasks.TaskResponse(null,
//                                    "sem email de destino cadastrado");
//                            }
                        }

                        Cursor cPreVendaEnvio = getApp().getDbPrePedido().rawQuery(
                            "SELECT P.*, C.CLI_NOM, C.CLI_NOM_EMAIL FROM PRE_VENDA P, CLIENTE C WHERE C.CLI_SEQ = P.CLI_SEQ AND P.PVE_EMAIL_ENVIADO = 0 AND P.SYNC_STATUS > 0",
                            null);
                        int emailsEnviados = 0;
                        if (cPreVendaEnvio.getCount() > 0) {


                            while (cPreVendaEnvio.moveToNext()) {
                                if(!emailDestino.equals("") || !funcoes.getStringValue(cPreVendaEnvio.getString(cPreVendaEnvio.getColumnIndex("CLI_NOM_EMAIL"))).equals("") ) {
                                    MimeMessage mimeMessage = new MimeMessage(session);
                                    mimeMessage.setFrom(
                                            prefs.getString(AccountManager.KEY_ACCOUNT_NAME, ""));
                                    if(!emailDestino.equals(""))
                                        mimeMessage.addRecipients(javax.mail.Message.RecipientType.TO,
                                                emailDestino);
                                    if(!funcoes.getStringValue(cPreVendaEnvio.getString(cPreVendaEnvio.getColumnIndex("CLI_NOM_EMAIL"))).equals(""))
                                        mimeMessage.addRecipients(javax.mail.Message.RecipientType.TO,
                                                cPreVendaEnvio.getString(cPreVendaEnvio.getColumnIndex("CLI_NOM_EMAIL")));
                                    mimeMessage.setSubject(String.format(new Locale("pt", "BR"),
                                            "Pedido Móvel - Vendedor %s ",
                                            getData().getExtras().getString(
                                                    CFFApplicationContext.Companion.getARG_FUN_NOM())));

                                    String bodyMessage = templateEmailCorpo;

                                    bodyMessage = bodyMessage.replace("[CLIENTE]", cPreVendaEnvio
                                            .getString(cPreVendaEnvio.getColumnIndex("CLI_NOM")))
                                            .replace("[FUN_SEQ_VEN]", getData().getStringExtra(
                                                    CFFApplicationContext.Companion.getARG_FUN_NOM()))
                                            .replace("[PVE_DAT]", cPreVendaEnvio.getString(
                                                    CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_DAT()))
                                            .replace("[CLI_NOM]", cPreVendaEnvio
                                                    .getString(cPreVendaEnvio.getColumnIndex("CLI_NOM")))
                                            .replace("[PVE_QTD_ITENS]", String
                                                    .format(new Locale("pt", "BR"), "%.2f", cPreVendaEnvio
                                                            .getDouble(CFFDbPrePedido.Companion
                                                                    .getPREVENDA_FIELD_PVE_QTD_ITENS())))
                                            .replace("[PVE_VAL_TOT]", String
                                                    .format(new Locale("pt", "BR"), "%.2f", cPreVendaEnvio
                                                            .getDouble(CFFDbPrePedido.Companion
                                                                    .getPREVENDA_FIELD_PVE_VAL_TOT())))
                                            .replace("[PVE_VAL_DESC]", String
                                                    .format(new Locale("pt", "BR"), "%.2f", cPreVendaEnvio
                                                            .getDouble(CFFDbPrePedido.Companion
                                                                    .getPREVENDA_FIELD_PVE_VAL_DESC())))
                                            .replace("[PVE_VAL_LIQ]", String
                                                    .format(new Locale("pt", "BR"), "%.2f", cPreVendaEnvio
                                                            .getDouble(CFFDbPrePedido.Companion
                                                                    .getPREVENDA_FIELD_PVE_VAL_LIQ())));

                                    String queryItemPreVenda = String
                                            .format(
                                                    "SELECT P.PRO_DSC_RESUM, I.IPV_QTD, I.IPV_VAL_UNIT, I.IPV_VAL_LIQ FROM PRODUTO P, ITEM_PRE_VENDA I WHERE"
                                                            + "	P.PRO_SEQ = I.PRO_SEQ AND P.EST_SEQ = I.EST_SEQ AND I.PVE_SEQ = %s",
                                                    cPreVendaEnvio.getString(CFFDbPrePedido.Companion
                                                            .getPREVENDA_FIELD_PVE_SEQ()));
                                    Cursor cItemPrevenda = getApp().getDbPrePedido().rawQuery(
                                            queryItemPreVenda, null);

                                    if (cItemPrevenda.getCount() <= 0) {
                                        return new NetworkTasks.TaskResponse(null,
                                                "pedido se item cadastrado");
                                    }

                                    String itemDetalheBodyMessage = "";

                                    while (cItemPrevenda.moveToNext()) {
                                        itemDetalheBodyMessage += templateItemDetalheEmail;
                                        itemDetalheBodyMessage = itemDetalheBodyMessage
                                                .replace("[PRO_DSC_RESUM]", cItemPrevenda.getString(0))
                                                .replace("[IPV_QTD]", String
                                                        .format(new Locale("pt", "BR"), "%.3f",
                                                                cItemPrevenda.getDouble(1)))
                                                .replace("[IPV_VAL_LIQ]", String
                                                        .format(new Locale("pt", "BR"), "%.3f",
                                                                cItemPrevenda.getDouble(3)));
                                    }

                                    String itemBodyMessage =
                                            templateItemEmailCabecalho + itemDetalheBodyMessage
                                                    + templateItemEmailRodape;

                                    bodyMessage =
                                            bodyMessage + itemBodyMessage + templateEmailRodape;

                                    mimeMessage.setContent(bodyMessage, "text/html; charset=UTF-8");

                                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                                    mimeMessage.writeTo(buffer);
                                    byte[] bytes = buffer.toByteArray();
//                                    byte[] bytes = mimeMessage.getContent().toString().getBytes();
                                    String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
                                    com.google.api.services.gmail.model.Message message = new com.google.api.services.gmail.model.Message();
                                    message.setRaw(encodedEmail);

                                    mService.users().messages().send("me", message).execute();

                                    ContentValues valuesEnvio = new ContentValues();
                                    valuesEnvio.put("PVE_EMAIL_ENVIADO", 1);

                                    getApp().getDbPrePedido().update("PRE_VENDA", valuesEnvio,
                                            String.format(new Locale("pt", "BR"),
                                                    "PVE_SEQ = %d",
                                                    cPreVendaEnvio.getInt(CFFDbPrePedido.Companion
                                                            .getPREVENDA_FIELD_PVE_SEQ())), null);
                                    emailsEnviados++;
                                }
                            }
                        }

                        return new NetworkTasks.TaskResponse(emailsEnviados, "OK");

                    } catch (MessagingException me) {

                        return new NetworkTasks.TaskResponse(null,
                            funcoes.getStringValue(me.getMessage()));

                    } catch (UserRecoverableAuthIOException ex) {

                        startActivityForResult(ex.getIntent(), 2001);

                        return new NetworkTasks.TaskResponse(null,
                            funcoes.getStringValue(ex.getMessage()));
                    } catch (IOException ioE) {

                        return new NetworkTasks.TaskResponse(null,
                            funcoes.getStringValue(ioE.getMessage()));

                    }

                }
            }

            return new NetworkTasks.TaskResponse(null, "desconhecido");
        },
        (aVoid, self) -> {

            if (aVoid.getResponse() != null) {
                Integer iResponse = (Integer) aVoid.getResponse();
                if (iResponse > 0) {
                    Toast.makeText(MainActivity.this, String
                        .format(new Locale("pt", "BR"), "%d email(s) enviado(s) com sucesso",
                            iResponse), Toast.LENGTH_LONG).show();
                }
            }
        });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (getData().getExtras() == null) {
            InitConfig(getIntent().getExtras(), null);
        }

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open,
            R.string.navigation_drawer_close) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                TextView txvPedidos = navigationView.getHeaderView(0)
                    .findViewById(R.id.txvPedidos);
                if (txvPedidos != null) {
                    Cursor cPreVendasSinc = getApp().getDbPrePedido()
                        .rawQuery("SELECT COUNT(PVE_SEQ) FROM PRE_VENDA WHERE SYNC_STATUS <> 2",
                            null);
                    try {
                        if (cPreVendasSinc.getCount() > 0) {
                            cPreVendasSinc.moveToFirst();
                            txvPedidos.setText(String
                                .format(new Locale("pt", "BR"), "%d Pedido(s) não sincronizados",
                                    cPreVendasSinc.getInt(0)));
                        }
                    } finally {
                        cPreVendasSinc.close();
                    }
                }
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        Menu menu = navigationView.getMenu();
        if (menu != null) {
            MenuItem menuItem = menu.getItem(3);
            if (menuItem != null) {
                SubMenu subMenu = menuItem.getSubMenu();
                if (subMenu != null) {
                    MenuItem itemCarga = subMenu.getItem(0);
                    itemCarga.setActionView(R.layout.carga_menu);
                    ViewGroup viewGroup = (ViewGroup) itemCarga.getActionView();
                    TextView txvCarga = viewGroup.findViewById(R.id.txvDataCarga);
                    if (txvCarga != null) {
                        Cursor config = getApp().getCurrentConfig();
                        config.moveToFirst();
                        String datSync = config.getString(config.getColumnIndex("DAT_SYNC"));
                        if (datSync != null) {
                            txvCarga.setText(datSync);
                            txvCarga.setTextColor(ContextCompat.getColor(this, R.color.Black));
                        }
                    }
                }
            }
        }

        TextView txvPedidos = navigationView.getHeaderView(0).findViewById(R.id.txvPedidos);
        if (txvPedidos != null) {
            Cursor cPreVendasSinc = getApp().getDbPrePedido()
                .rawQuery("SELECT COUNT(PVE_SEQ) FROM PRE_VENDA WHERE SYNC_STATUS <> 2", null);
            try {
                if (cPreVendasSinc.getCount() > 0) {
                    cPreVendasSinc.moveToFirst();
                    txvPedidos.setText(String
                        .format(new Locale("pt", "BR"), "%d Pedido(s) não sincronizados",
                            cPreVendasSinc.getInt(0)));
                }
            } finally {
                cPreVendasSinc.close();
            }
        }

        ImageButton imgNovo = navigationView.getHeaderView(0).findViewById(R.id.imgNovo);
        imgNovo.setOnClickListener(v -> {
            if (preVenda == null) {
//                Estoque = 0;
//                Empresa = 0;
//                Terminal = 0;
                drawer.closeDrawer(GravityCompat.START);
                if (getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_EMPRESA(),1) != 0 && getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_TER_SEQ(),1) != 0) {
                    Cursor config = getApp().getCurrentConfig();
                    config.moveToFirst();
                    if (config.getString(config.getColumnIndex("DAT_SYNC")) != null) {

                        preVenda = NovoPedido();
                        SharedPreferences prefs = getSharedPreferences(
                                MainActivity.PREFS,
                                0);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 1);
                        editor.apply();
                        EditText edtObs = pedidoGeral.getMainView()
                                .findViewById(R.id.edtObsPedido);
                        if (edtObs != null) {
                            edtObs.setFocusable(true);
                            edtObs.setFocusableInTouchMode(true);
                        }
                        pedidoGeral.PesquisaCliente();
                    } else {
                        Snackbar
                                .make(navigationView,
                                        "Necessário efetuar pelo menos uma carga no sistema.",
                                        Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                    }
                }
//                CFFDialogFragment.Builder builder = new CFFDialogFragment.Builder();
//                builder.setLayout(R.layout.dialog_nova_empresa);
//                builder.setListener(new CFFDialogFragmentListener() {
//                    public int mEmpresa = 0;
//
//                    @Override
//                    public void OnDismissListener() {
//
//                    }
//
//                    @Override
//                    public void OnShowListener(View v) {
//                        ListView lstEmpresas = v.findViewById(R.id.lstEmpresas);
//
//                        Cursor cursor = getApp().getDbConfig()
//                            .rawQuery("SELECT EMP_SEQ, EMP_NOM FROM EMPRESA", null);
//                        if (cursor.getCount() == 0) {
//                            Snackbar.make(getWindow().peekDecorView(), "Nenhum registro encontrado",
//                                Snackbar.LENGTH_LONG)
//                                .setAction("action", null)
//                                .show();
//                            return;
//                        }
//                        if(cursor.getCount() == 1){
//                            cursor.moveToFirst();
//                            DialogTerminal(cursor
//                                    .getInt(CFFDbConfig.Companion.getEMPRESA_FIELD_EMP_SEQ()));
//                            return;
//                        }
//                        CFFCursorAdapter adapter = new CFFCursorAdapter(MainActivity.this, cursor,
//                            R.layout.record_dialog_nova_empresa) {
//                            @Override
//                            public void SetValues(View view, int position) {
//                                getCursor().moveToPosition(position);
//                                TextView txvEmpresa = view.findViewById(R.id.txvEmpresa);
//                                txvEmpresa.setText(getCursor()
//                                    .getString(CFFDbConfig.Companion.getEMPRESA_FIELD_EMP_NOM()));
//                            }
//                        };
//                        lstEmpresas.setAdapter(adapter);
//                        lstEmpresas.setDividerHeight(0);
//                        lstEmpresas.setOnItemClickListener((adapterView, view, i, l) -> {
//                            builder.dismiss();
//                            CFFCursorAdapter localAdapter = (CFFCursorAdapter) adapterView
//                                .getAdapter();
//                            localAdapter.getCursor().moveToPosition(i);
//                            Empresa = localAdapter.getCursor()
//                                .getInt(CFFDbConfig.Companion.getEMPRESA_FIELD_EMP_SEQ());
//                            mEmpresa = Empresa;
//                            DialogTerminal(mEmpresa);
//                        });
//                    }
//                });
//                builder.show(getSupportFragmentManager(), "TAG_EMPRESA");
            } else {
                Snackbar
                    .make(navigationView, "Já existe um pedido em edição.", Snackbar.LENGTH_LONG)
                    .setAction("action", null)
                    .show();
            }
        });

        ImageButton imgRemover = navigationView.getHeaderView(0).findViewById(R.id.imgRemover);
        imgRemover.setOnClickListener(v -> {
            if(MainActivity.preVenda.getSyncStatus() == 2) return ;
            if (MainActivity.preVenda != null) {
                if (MainActivity.preVenda.getSyncStatus() != 2) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                    alert.setMessage("Confirma a exclusão do pedido ?");
                    alert.setTitle(getString(R.string.msg_sistema));
                    alert.setPositiveButton("Sim", (dialog, which) -> {
                        drawer.closeDrawer(GravityCompat.START);
                        SharedPreferences prefs = getSharedPreferences(MainActivity.PREFS, 0);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 0);
                        editor.apply();

                        LimpaCampos();

                        getApp().getDbPrePedido().delete("PRE_VENDA", "PVE_SEQ = ?",
                            new String[]{MainActivity.preVenda.getPveSeq().toString()});
                        getApp().getDbPrePedido().delete("ITEM_PRE_VENDA", "PVE_SEQ = ?",
                            new String[]{MainActivity.preVenda.getPveSeq().toString()});
                        getApp().getDbPrePedido()
                            .delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?",
                                new String[]{MainActivity.preVenda.getPveSeq().toString()});

                        viewPager.setCurrentItem(0);

                        itemPedido.setAdapter(null);

                        MainActivity.preVenda = null;

                        EditText edtObs = pedidoGeral.getMainView()
                            .findViewById(R.id.edtObsPedido);
                        if (edtObs != null) {
                            edtObs.setFocusable(false);
                            edtObs.setFocusableInTouchMode(false);
                        }
                    });
                    alert.setNegativeButton("Não", (dialog, which) -> dialog.dismiss());
                    alert.show();

                } else {
                    Snackbar.make(findViewById(R.id.content), "Pedido ja sincronizado.",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("action", null)
                        .show();
                }

            }
        });

        ImageButton imgCancelar = navigationView.getHeaderView(0).findViewById(R.id.imgCancelar);
        imgCancelar.setOnClickListener(v -> {
            if (MainActivity.preVenda != null) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setMessage("Gostaria de abandonar as alterações ?");
                alert.setTitle(getString(R.string.msg_sistema));
                alert.setPositiveButton("Sim", (dialog, which) -> {
                    drawer.closeDrawer(GravityCompat.START);
                    SharedPreferences prefs = getSharedPreferences(MainActivity.PREFS, 0);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 0);
                    editor.apply();

                    LimpaCampos();

                    viewPager.setCurrentItem(0);

                    if (MainActivity.preVenda.getSyncStatus() == 0) {
                        getApp().getDbPrePedido().delete("PRE_VENDA", "PVE_SEQ = ?",
                            new String[]{MainActivity.preVenda.getPveSeq().toString()});
                        getApp().getDbPrePedido().delete("ITEM_PRE_VENDA", "PVE_SEQ = ?",
                            new String[]{MainActivity.preVenda.getPveSeq().toString()});
                        getApp().getDbPrePedido()
                            .delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?",
                                new String[]{MainActivity.preVenda.getPveSeq().toString()});
                    }

                    itemPedido.setAdapter(null);

                    MainActivity.preVenda = null;

//                    Estoque = 0;
//                    Empresa = 0;
//                    Terminal = 0;

                    EditText edtObs = pedidoGeral.getMainView()
                        .findViewById(R.id.edtObsPedido);
                    if (edtObs != null) {
                        edtObs.setFocusable(false);
                        edtObs.setFocusableInTouchMode(false);
                    }
                });
                alert.setNegativeButton("Não", (dialog, which) -> dialog.dismiss());
                alert.show();
            }
        });

        ImageButton imgSalvar = navigationView.getHeaderView(0).findViewById(R.id.imgSalvar);
        imgSalvar.setOnClickListener(v -> {
            if(MainActivity.preVenda.getSyncStatus() == 2) {
                Snackbar.make(findViewById(R.id.content), "Pedido sincronizado não pode ser alterado", Snackbar.LENGTH_LONG)
                    .setAction("action" ,null)
                    .show();
                return;
            }
            if (MainActivity.preVenda != null) {
                if (MainActivity.preVenda.getEcfItensPreVenda().size() > 0) {
                    if (MainActivity.preVenda.getEcfFormaCondPagamPreVendas().size() > 0) {
                        if (MainActivity.preVenda.getSyncStatus() != 2) {

                            EditText edtObs = pedidoGeral.getMainView()
                                .findViewById(R.id.edtObsPedido);
                            MainActivity.preVenda.setSyncStatus(1);
                            MainActivity.preVenda.setPveObs(edtObs.getText().toString());

                            Cursor cParametroStatus = getApp().getDbPrePedido().rawQuery(
                                String.format(new Locale("pt","BR"),"SELECT * FROM PARAMETRO_EMPRESA WHERE EMP_SEQ = %d AND TER_SEQ = %d and PEM_NOM_PARAM = \'%s\'", MainActivity.preVenda.getEmpSeq(), MainActivity.preVenda.getTerSeq(), "TER_PAR_19601_SITUACAO_PADRAO_AO_INCLUI"), null);

                            if(cParametroStatus.getCount() > 0){
                                cParametroStatus.moveToFirst();
                                if(cParametroStatus.getString(CFFDbConfig.Companion.getPARAMETRO_FIELD_PEM_VAL_PARAM()).equals("Cadastrada"))
                                    MainActivity.preVenda.setPveStatus("3");
                            }

                            CFFDbPrePedido.Companion
                                .CommitPreVenda(getApp().getDbPrePedido(), MainActivity.preVenda);
                            drawer.closeDrawer(GravityCompat.START);
                            SharedPreferences prefs = getSharedPreferences(MainActivity.PREFS, 0);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 0);
                            editor.apply();
                            LimpaCampos();
                            MainActivity.preVenda = null;
                            viewPager.setCurrentItem(0);
                            edtObs.setFocusable(false);
                            edtObs.setFocusableInTouchMode(false);

//                            TextView txvPedidos1 = navigationView.getHeaderView(0).findViewById(R.id.txvPedidos);
                            if (txvPedidos != null) {
                                Cursor cPreVendasSinc = getApp().getDbPrePedido().rawQuery(
                                    "SELECT COUNT(PVE_SEQ) FROM PRE_VENDA WHERE SYNC_STATUS <> 2",
                                    null);
                                try {
                                    if (cPreVendasSinc.getCount() > 0) {
                                        cPreVendasSinc.moveToFirst();
                                        txvPedidos.setText(String.format(new Locale("pt", "BR"),
                                            "%d Pedido(s) não sincronizados",
                                            cPreVendasSinc.getInt(0)));
                                    }
                                } finally {
                                    cPreVendasSinc.close();
                                }
                            }

                            taskMail.Execute();

                        } else {
                            Snackbar.make(findViewById(R.id.content), "Pedido ja sincronizado.",
                                Snackbar.LENGTH_INDEFINITE)
                                .setAction("action", null)
                                .show();
                            drawer.closeDrawer(GravityCompat.START);
                        }
                    } else {
                        Snackbar.make(findViewById(R.id.content),
                            "Pedido deve conter forma de recebimento.", Snackbar.LENGTH_INDEFINITE)
                            .setAction("action", null)
                            .show();
                        viewPager.setCurrentItem(2);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                } else {
                    Snackbar
                        .make(findViewById(R.id.content), "Pedido deve conter pelo menos 1 item.",
                            Snackbar.LENGTH_INDEFINITE)
                        .setAction("action", null)
                        .show();
                    viewPager.setCurrentItem(1);
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });

        viewPager = findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                int positionOffsetPixels) {
                if (position == 0) {
                    toolbar.setSubtitle("Geral");
                    navigationView.setCheckedItem(R.id.nav_nome);
                }
                if (position == 1) {
                    toolbar.setSubtitle("Item");
                    navigationView.setCheckedItem(R.id.nav_fantasia);
                    SharedPreferences prefs = getSharedPreferences(MainActivity.PREFS, 0);
                    if (prefs.getInt(MainActivity.PREF_NOVO_PEDIDO, 0) == 1) {

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 0);
                        editor.apply();

                        itemPedido.PesquisaNovoProduto();
                    }
                }
                if (position == 2) {
                    toolbar.setSubtitle("Recebimento");
                    navigationView.setCheckedItem(R.id.nav_receb);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        pedidoGeral = FragmentPedidoGeral.newInstance(getData().getExtras());
        itemPedido = FragmentItemPedido.newInstance(getData().getExtras());
        recebimento = FragmentRecebimento.newInstance(getData().getExtras());
        CFFFragmentPagerAdapter pagerAdapter = new CFFFragmentPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment("GERAL", pedidoGeral);
        pagerAdapter.addFragment("ITEM", itemPedido);
        pagerAdapter.addFragment("RECEBIMENTO", recebimento);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(4);

        navigationView.setCheckedItem(R.id.nav_nome);

        drawer.openDrawer(GravityCompat.START);

        RemovePedidoIncompleto();

        NetworkTasks tskInicial = new NetworkTasks(
            (self, p) -> {
                if (!FuncoesAndroid.isDeviceOnline(MainActivity.this)) {
                    return null;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                    new Locale("pt", "BR"));
                boolean enviaemail = false;

                Cursor cParametro = getApp().getDbPrePedido().rawQuery(
                    "SELECT * FROM PARAMETRO_EMPRESA where PEM_NOM_PARAM = \'EMP_PAR_11701_UTILIZAR_ENVIO_AUTOMATICO\'",
                    null);

                if (cParametro.getCount() > 0) {
                    cParametro.moveToFirst();
                    if (funcoes.getStringValue(cParametro
                        .getString(CFFDbConfig.Companion.getPARAMETRO_FIELD_PEM_VAL_PARAM()))
                        .toUpperCase().equals("SIM")) {
                        enviaemail = true;
                    }
                }

                SharedPreferences prefPreVenda = getSharedPreferences(PREF_NOVO_PEDIDO,
                    MODE_PRIVATE);

                if (enviaemail && prefPreVenda.getBoolean(PREF_ENVIA_EMAIL, true)) {
                    SharedPreferences prefs = getSharedPreferences(AccountManager.KEY_ACCOUNT_NAME,
                        MODE_PRIVATE);
                    if (prefs.getString(AccountManager.KEY_ACCOUNT_NAME, "").equals("")) {
                        if (isGooglePlayServicesAvailable()) {

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.clear();
                            editor.apply();

                            String[] SCOPES = new String[]{GmailScopes.GMAIL_SEND};

                            GoogleAccountCredential mCredential = GoogleAccountCredential
                                .usingOAuth2(
                                    getApplicationContext(), Arrays.asList(SCOPES))
                                .setBackOff(new ExponentialBackOff());

                            mCredential.setSelectedAccountName("");

                            String[] accountTypes = new String[]{"com.google"};
                            Intent intent = AccountPicker
                                .newChooseAccountIntent(null, null, accountTypes, false, null, null,
                                    null, null);
                            startActivityForResult(intent, REQUEST_INIT_EMAIL);
                        }

                    } else {
                        taskMail.Execute();
                    }

//							PeriodicTask periodicTask = new PeriodicTask.Builder()
//									.setService(CFFConnectReceiver.class)
//									.setTag(CFFConnectReceiver.GCM_REPEAT_TAG)
//									.setPersisted(true)
//									.setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
//									.setPeriod(10000)
//									.setFlex(0)
//									.build();
//
//							GcmNetworkManager.getInstance(MainActivity.this).schedule(periodicTask);
                }

                return new NetworkTasks.TaskResponse(null, "OK");
            });
        tskInicial.Execute();


    }

//    public void DialogTerminal(int empresa){
//        CFFDialogFragment.Builder builderTerminal = new CFFDialogFragment.Builder();
//        builderTerminal.setLayout(R.layout.dialog_nova_empresa);
//        builderTerminal.setListener(new CFFDialogFragmentListener() {
//
//            @Override
//            public void OnDismissListener() {
//
//            }
//
//            @Override
//            public void OnShowListener(View v) {
//                TextView txvTitulo = v.findViewById(R.id.txvTitulo);
//                txvTitulo.setText("Selecione o terminal");
//                ListView lstTerminal = v.findViewById(R.id.lstEmpresas);
//                lstTerminal.setDividerHeight(0);
//
//                Cursor cursorTerminal = getApp().getDbConfig()
//                        .rawQuery(String.format(new Locale("pt","BR"),"SELECT TER_SEQ, EMP_SEQ, TER_DSC, EST_SEQ FROM TERMINAL WHERE EMP_SEQ = %d", empresa), null);
//                if (cursorTerminal.getCount() == 0) {
//                    Snackbar.make(getWindow().peekDecorView(), "Nenhum registro encontrado",
//                            Snackbar.LENGTH_LONG)
//                            .setAction("action", null)
//                            .show();
//                    return;
//                }
//                CFFCursorAdapter adapterTerminal = new CFFCursorAdapter(MainActivity.this, cursorTerminal,
//                        R.layout.record_dialog_nova_empresa) {
//                    @Override
//                    public void SetValues(View view, int position) {
//                        getCursor().moveToPosition(position);
//                        TextView txvEmpresa = view.findViewById(R.id.txvEmpresa);
//                        txvEmpresa.setText(getCursor()
//                                .getString(CFFDbConfig.Companion.getTERMINAL_FIELD_TER_DSC()));
//                    }
//                };
//                lstTerminal.setAdapter(adapterTerminal);
//                lstTerminal.setOnItemClickListener(
//                        (adapterView1, view1, i1, l1) -> {
//                            builderTerminal.dismiss();
//                            CFFCursorAdapter terminalAdapter = (CFFCursorAdapter) adapterView1
//                                    .getAdapter();
//                            terminalAdapter.getCursor().moveToPosition(i1);
//                            Terminal = terminalAdapter.getCursor()
//                                    .getInt(CFFDbConfig.Companion.getTERMINAL_FIELD_TER_SEQ());
//                            Estoque = terminalAdapter.getCursor()
//                                    .getInt(CFFDbConfig.Companion.getTERMINAL_FIELD_EST_SEQ());
//                            if (empresa != 0 && Terminal != 0) {
//                                Cursor config = getApp().getCurrentConfig();
//                                config.moveToFirst();
//                                if (config.getString(config.getColumnIndex("DAT_SYNC")) != null) {
//
//                                    preVenda = NovoPedido();
//                                    SharedPreferences prefs = getSharedPreferences(
//                                            MainActivity.PREFS,
//                                            0);
//                                    SharedPreferences.Editor editor = prefs.edit();
//                                    editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 1);
//                                    editor.apply();
//                                    EditText edtObs = pedidoGeral.getMainView()
//                                            .findViewById(R.id.edtObsPedido);
//                                    if (edtObs != null) {
//                                        edtObs.setFocusable(true);
//                                        edtObs.setFocusableInTouchMode(true);
//                                    }
//                                    pedidoGeral.PesquisaCliente();
//                                } else {
//                                    Snackbar
//                                            .make(navigationView,
//                                                    "Necessário efetuar pelo menos uma carga no sistema.",
//                                                    Snackbar.LENGTH_LONG)
//                                            .setAction("action", null)
//                                            .show();
//                                }
//                            }
//                        });
//            }
//        });
//        builderTerminal.show(getSupportFragmentManager(), "BUILDER_TERMINAL");
//    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        if (!drawer.isDrawerOpen(GravityCompat.START)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.msg_sistema);
            dialog.setMessage("Gostaria de sair da aplicação ?");
            dialog.setPositiveButton("Sim", (dialog1, which) -> {
                dialog1.dismiss();
                finish();
            });
            dialog.setNegativeButton("Não", (dialog12, which) -> dialog12.dismiss());
            dialog.show();
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
            GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
            apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
            GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
            apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    static final int REQUEST_GOOGLE_PLAY_SERVICES = 2004;

    void showGooglePlayServicesAvailabilityErrorDialog(
        final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
            MainActivity.this,
            connectionStatusCode,
            REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (viewPager != null) {
            if (id == R.id.nav_nome) {
                viewPager.setCurrentItem(0);
                toolbar.setSubtitle("Geral");
            }
            if (id == R.id.nav_fantasia) {
                viewPager.setCurrentItem(1);
                toolbar.setSubtitle("Item");
            }
            if (id == R.id.nav_receb) {
                viewPager.setCurrentItem(2);
                toolbar.setSubtitle("Recebimento");
            }
            if (id == R.id.nav_compartilhar){
                if(preVenda==null){
                    Intent iPreVenda = new Intent(this, PesquisaPreVendaActivity.class);
                    startActivityForResult(iPreVenda, REQUEST_COMPARTILHAR);
                } else {
                    onClickShare(OrcamentoString());
                }
            }
            if (id == R.id.nav_carga) {
                if(FuncoesAndroid.isDeviceOnline(this)) {
                    final ProgressDialog progressDialog = new ProgressDialog(this);

                    NetworkTasks task = new NetworkTasks(
                            self -> {
                                progressDialog.setMessage(getString(R.string.msg_carga_sistema));
                                progressDialog.setTitle("Mensagem do Sistema.");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                getWindow().addFlags(
                                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                            },
                            (self, params) -> {
                                try {
                                    CFFApplicationContext.Companion
                                            .BackupDatabase(getApp().getApplicationContext());
                                } catch (Exception io) {
                                    System.out.println();
                                }
                                SQLiteDatabase dbCarga = getApp().getDbConfig();
                                if (dbCarga == null)
                                    return new NetworkTasks.TaskResponse(null,
                                            funcoes.getStringValue("Ocorreu um erro ao carregar o banco local"));
                                try {
                                    Cursor currentConfig = getApp().getCurrentConfig();
                                    String qEmpresas = "SELECT E.EMP_SEQ, E.EMP_NOM, E.EMP_NOM_FANTASIA, E.IMG_SEQ FROM ECF_EMPRESA E";
                                    String qTerminal = "SELECT T.TER_SEQ, T.EMP_SEQ, T.TER_DSC, T.EST_SEQ FROM ECF_TERMINAL T";
                                    CFFDataSet dsEmpresa = getApp().getAndroidSocket().getQuery(qEmpresas, 1);

                                    if (dsEmpresa != null) {
                                        if (dsEmpresa.getCount() > 0) {
                                            dbCarga.execSQL("DELETE FROM EMPRESA");
                                            while (dsEmpresa.getTable(0).next()) {
                                                ContentValues values = new ContentValues();
                                                values.put("EMP_SEQ", dsEmpresa.getInteger("EMP_SEQ"));
                                                values.put("EMP_NOM", dsEmpresa.getString("EMP_NOM"));
                                                values.put("EMP_NOM_FANTASIA", dsEmpresa.getString("EMP_NOM_FANTASIA"));
                                                values.put("IMG_SEQ", dsEmpresa.getInteger("IMG_SEQ"));
                                                dbCarga.insert(CFFDbConfig.Companion.getTABLE_EMPRESA(), null, values);
                                            }
                                        }
                                    }

                                    CFFDataSet dsTerminal = getApp().getAndroidSocket().getQuery(qTerminal, 1);
                                    if (dsTerminal != null) {
                                        if (dsTerminal.getCount() > 0) {
                                            dbCarga.execSQL("DELETE FROM TERMINAL");
                                            while (dsTerminal.getTable(0).next()) {
                                                ContentValues values = new ContentValues();
                                                values.put("TER_SEQ", dsTerminal.getInteger("TER_SEQ"));
                                                values.put("EMP_SEQ", dsTerminal.getInteger("EMP_SEQ"));
                                                values.put("TER_DSC", dsTerminal.getString("TER_DSC"));
                                                values.put("EST_SEQ", dsTerminal.getInteger("EST_SEQ"));
                                                dbCarga.insert(CFFDbConfig.Companion.getTABLE_TERMINAL(), null, values);
                                            }
                                        }
                                    }

                                    Cursor curEmpresas = getApp().getDbConfig().rawQuery("SELECT DISTINCT E.EMP_SEQ, T.EST_SEQ FROM EMPRESA E, TERMINAL T WHERE E.EMP_SEQ = T.EMP_SEQ AND E.EMP_SEQ = " + getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_EMPRESA(), 1), null);
                                    while (curEmpresas.moveToNext()) {
                                        CFFDbFuncoes.Companion
                                                .Carga(getApp().getBaseContext(),
                                                        getApp().getAndroidSocket(),
                                                        currentConfig
                                                                .getInt(CFFDbConfig.Companion
                                                                        .getCONFIG_FIELD_FUNCIONARIO()),
                                                        curEmpresas.getInt(0),
                                                        curEmpresas.getInt(1));
                                    }
                                    CFFApplicationContext.Companion
                                            .BackupDatabase(getApp().getApplicationContext());

                                    return new NetworkTasks.TaskResponse("", "OK");
                                } catch (Exception ex) {
                                    return new NetworkTasks.TaskResponse(null,
                                            funcoes.getStringValue(ex.getMessage()));
                                } finally {
                                    dbCarga.close();
                                }
                            },
                            (aVoid, self) -> {
                                progressDialog.dismiss();
                                if (aVoid.getStatus().equals("OK")) {
                                    Snackbar.make(viewPager, R.string.process_sucess_carga,
                                            Snackbar.LENGTH_LONG)
                                            .setAction("action", null)
                                            .show();
                                    Menu menu = navigationView.getMenu();
                                    if (menu != null) {
                                        MenuItem menuItem = menu.getItem(3);
                                        if (menuItem != null) {
                                            SubMenu subMenu = menuItem.getSubMenu();
                                            if (subMenu != null) {
                                                MenuItem itemCarga = subMenu.getItem(0);
                                                itemCarga.setActionView(R.layout.carga_menu);
                                                ViewGroup viewGroup = (ViewGroup) itemCarga.getActionView();
                                                TextView txvCarga = (TextView) viewGroup
                                                        .findViewById(R.id.txvDataCarga);
                                                if (txvCarga != null) {
                                                    Cursor config = getApp().getCurrentConfig();
                                                    config.moveToFirst();
                                                    String datSync = config
                                                            .getString(config.getColumnIndex("DAT_SYNC"));
                                                    if (datSync != null) {
                                                        txvCarga.setText(datSync);
                                                        txvCarga.setTextColor(ContextCompat
                                                                .getColor(MainActivity.this, R.color.Black));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    Snackbar.make(viewPager, aVoid.getResponse().toString(),
                                            Snackbar.LENGTH_LONG)
                                            .setAction("action", null)
                                            .show();
                                }
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                            }
                    );
                    task.Execute();
                } else {
                    Snackbar.make(viewPager, "Dispositivo sem acesso a internet", Snackbar.LENGTH_LONG)
                            .setAction("action", null)
                            .show();
                }

            }
            if (id == R.id.nav_sinc) {
                if(preVenda == null) {
                    if(FuncoesAndroid.isDeviceOnline(this)) {
                        SincronizaCliente();
                        SincronizaPreVenda();
                    } else {
                        Snackbar.make(viewPager, "Dispositivo sem acesso a internet", Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                    }
                } else {
                    Snackbar.make(viewPager, "Pedido em edição", Snackbar.LENGTH_LONG)
                            .setAction("action", null)
                            .show();
                }
            }
            if (id == R.id.nav_pesquisar) {
                Intent iPreVenda = new Intent(this, PesquisaPreVendaActivity.class);
                startActivityForResult(iPreVenda, 0);
            }
            if (id == R.id.nav_configurar) {
                Bundle args = new Bundle();

                args.putInt(LoginActivity.Companion.getARG_ICON(),
                    com.compu.android.lib.R.mipmap.ic_launcher_forca_de_vendas);
                args.putInt(LoginActivity.Companion.getARG_NAO_EXIGE_LOGIN(), 1);
                Intent iConfig = new Intent(this, LoginActivity.class);
                iConfig.putExtras(args);
                startActivityForResult(iConfig, 0);
            }
            if (id == R.id.nav_limpar_email) {
                SharedPreferences prefs = getSharedPreferences(AccountManager.KEY_ACCOUNT_NAME,
                    MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(AccountManager.KEY_ACCOUNT_NAME, "");
                editor.clear();
                editor.apply();
                SharedPreferences prefsPedido = getSharedPreferences(PREF_NOVO_PEDIDO,
                    MODE_PRIVATE);
                SharedPreferences.Editor editorPedido = prefsPedido.edit();
                editorPedido.putBoolean(PREF_ENVIA_EMAIL, true);
                editorPedido.apply();
                if (isGooglePlayServicesAvailable()) {

                    String[] SCOPES = new String[]{GmailScopes.GMAIL_SEND};

                    GoogleAccountCredential mCredential = GoogleAccountCredential.usingOAuth2(
                        getApplicationContext(), Arrays.asList(SCOPES))
                        .setBackOff(new ExponentialBackOff());

                    mCredential.setSelectedAccountName("");

                    Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                        new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, true, null, null, null,
                        null);
                    startActivityForResult(intent, REQUEST_INIT_EMAIL);
                }
            }
            if(id == R.id.nav_tira_teima){

                CFFDialogFragment.Builder builder = new CFFDialogFragment.Builder();
                builder.setLayout(R.layout.dialog_nova_empresa);
                builder.setListener(new CFFDialogFragmentListener() {
                    public int mEmpresa = 0;

                    @Override
                    public void OnDismissListener() {

                    }

                    @Override
                    public void OnShowListener(View v) {
                        ListView lstEmpresas = v.findViewById(R.id.lstEmpresas);

                        Cursor cursor = getApp().getDbConfig()
                            .rawQuery("SELECT EMP_SEQ, EMP_NOM FROM EMPRESA", null);
                        if (cursor.getCount() == 0) {
                            Snackbar.make(getWindow().peekDecorView(), "Nenhum registro encontrado",
                                Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                            return;
                        }
                        CFFCursorAdapter adapter = new CFFCursorAdapter(MainActivity.this, cursor,
                            R.layout.record_dialog_nova_empresa) {
                            @Override
                            public void SetValues(View view, int position) {
                                getCursor().moveToPosition(position);
                                TextView txvEmpresa = view.findViewById(R.id.txvEmpresa);
                                txvEmpresa.setText(getCursor()
                                    .getString(CFFDbConfig.Companion.getEMPRESA_FIELD_EMP_NOM()));
                            }
                        };
                        lstEmpresas.setAdapter(adapter);
                        lstEmpresas.setDividerHeight(0);
                        lstEmpresas.setOnItemClickListener((adapterView, view, i, l) -> {
                            builder.dismiss();
                            CFFCursorAdapter localAdapter = (CFFCursorAdapter) adapterView
                                .getAdapter();
                            localAdapter.getCursor().moveToPosition(i);
//                            Empresa = localAdapter.getCursor()
//                                .getInt(CFFDbConfig.Companion.getEMPRESA_FIELD_EMP_SEQ());
                            Intent iItemPesquisa = new Intent(MainActivity.this, PesquisaProdutoActivity.class);
                            iItemPesquisa.putExtra("CLI_SEQ", 1 );
                            iItemPesquisa.putExtra("CONSULTA", 1);
                            iItemPesquisa.putExtra("EMP_SEQ", getIntent().getIntExtra(CFFApplicationContext.Companion.getARG_EMPRESA(),1));
                            startActivityForResult(iItemPesquisa, 0);
                        });
                    }
                });
                builder.show(getSupportFragmentManager(), "TAG_EMPRESA");


            }
            if(id == R.id.nav_consulta_cliente){
                Intent iCliente = new Intent(this, PesquisaClienteActivity.class);
                iCliente.putExtras(getData());
                iCliente.putExtra("PESQUISA", 1);
                startActivity(iCliente);
            }

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

            return true;
        } else {
            return false;
        }

    }

    private Pair<Boolean, String> Sincroniza(CFFApplicationContext app) {

        SQLiteDatabase dbPrePedido = app.getDbPrePedido();

        try {

            String qPreVendaSync = "SELECT * FROM PRE_VENDA WHERE SYNC_STATUS = 1";
            Cursor cPreVendaSync = dbPrePedido
                .rawQuery(qPreVendaSync, null);

            if (cPreVendaSync.getCount() == 0) {
                return new Pair(false, "Nao existe pedido a ser sincronizado");
            }

            while (cPreVendaSync.moveToNext()) {
                if(cPreVendaSync.getInt(cPreVendaSync.getColumnIndex("CLI_SEQ")) <= 0){
                    return new Pair(false, String.format(new Locale("pt","BR"), "Pedido %d sem cliente sincronizado", cPreVendaSync.getInt(cPreVendaSync.getColumnIndex("PVE_SEQ"))));
                }
                ArrayList<CFFCommitFields> commitFields = new ArrayList<>();
                int pveSeq = 0;

                CFFDataSet dsAutoIncPveSeq = app.getAndroidSocket().getQuery(
                    "SELECT NOM_TABELA, NUM_ULTIMO_REGIST FROM CFF_AUTOINC WHERE NOM_TABELA = \'ECF_PRE_VENDA\' AND NOM_CAMPO = \'PVE_SEQ\'",
                    1);

                CFFCommitFields commitFieldsAutoInc = new CFFCommitFields("CFF_AUTOINC",
                    CFFCommitFields.INSERT,
                    "NOM_TABELA = \'ECF_PRE_VENDA\' AND NOM_CAMPO = \'PVE_SEQ\'");
                commitFieldsAutoInc.Add("NOM_TABELA", "ECF_PRE_VENDA", CFFCommitFields.STRING);
                commitFieldsAutoInc.Add("NOM_CAMPO", "PVE_SEQ", CFFCommitFields.STRING);
                commitFieldsAutoInc.Add("SQL_WHERE", " ", CFFCommitFields.STRING);
                if (dsAutoIncPveSeq.getCount() == 0) {

                    dsAutoIncPveSeq = app.getAndroidSocket()
                        .getQuery("SELECT MAX(ISNULL(PVE_SEQ, 0)) AS PVE_SEQ FROM ECF_PRE_VENDA",
                            1);

                    if (dsAutoIncPveSeq.getCount() > 0) {
                        dsAutoIncPveSeq.getTable(0).next();
                        pveSeq = dsAutoIncPveSeq.getInt("PVE_SEQ") + 1;

                    } else {
                        pveSeq = 1;
                    }
                } else {
                    dsAutoIncPveSeq.getTable(0).next();
                    pveSeq = dsAutoIncPveSeq.getInt("NUM_ULTIMO_REGIST") + 1;
                    commitFieldsAutoInc.setType(CFFCommitFields.UPDATE);
                }

                commitFieldsAutoInc.Add("NUM_ULTIMO_REGIST", pveSeq, CFFCommitFields.INTEGER);

                CFFCommitFields commitFieldsPreVenda = new CFFCommitFields("ECF_PRE_VENDA",
                    CFFCommitFields.INSERT);
                commitFieldsPreVenda.Add("PVE_SEQ", pveSeq, CFFCommitFields.INTEGER);
                if (cPreVendaSync.isNull(CFFDbPrePedido.Companion.getPREVENDA_FIELD_TER_SEQ())) {
                    commitFieldsPreVenda.Add("TER_SEQ", app.getCurrentConfig()
                            .getInt(CFFDbConfig.Companion.getCONFIG_FIELD_TERMINAL()),
                        CFFCommitFields.INTEGER);
                } else {
                    commitFieldsPreVenda.Add("TER_SEQ",
                        cPreVendaSync.getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_TER_SEQ()),
                        CFFCommitFields.INTEGER);
                }
                commitFieldsPreVenda.Add("PVE_NUM", "001001", CFFCommitFields.STRING);
                commitFieldsPreVenda.Add("PVE_NUM_VENDEDOR", "001", CFFCommitFields.STRING);
                commitFieldsPreVenda.Add("PVE_NUM_VENDA_VENDEDOR", "001", CFFCommitFields.STRING);
                commitFieldsPreVenda.Add("PVE_STATUS", cPreVendaSync
                        .getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_STATUS()),
                    CFFCommitFields.STRING);
                commitFieldsPreVenda.Add("PVE_DAT", FuncoesAndroid.parseDate(
                    cPreVendaSync.getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_DAT())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_HOR_PREV_ENTRG", FuncoesAndroid.parseDateTime(
                    cPreVendaSync.getString(
                        CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_HOR_PREV_ENTRG())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_QTD_ITENS", cPreVendaSync
                        .getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_QTD_ITENS()),
                    CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_QTD_PROD", cPreVendaSync
                        .getFloat(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_QTD_PROD()),
                    CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_TIPO_DESC", cPreVendaSync
                        .getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_TIPO_DESC()),
                    CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_VAL_DESC", cPreVendaSync
                        .getFloat(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_DESC()),
                    CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_TIPO_ACRES", cPreVendaSync
                        .getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_TIPO_ACRES()),
                    CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_VAL_ACRES", cPreVendaSync
                        .getFloat(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_ACRES()),
                    CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_VAL_TOT", cPreVendaSync
                        .getFloat(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_TOT()),
                    CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_VAL_LIQ", cPreVendaSync
                        .getFloat(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_LIQ()),
                    CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("CLI_SEQ",
                    cPreVendaSync.getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_CLI_SEQ()),
                    CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("FUN_SEQ_VEN",
                    cPreVendaSync.getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_FUN_SEQ_VEN()),
                    CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_DAT_MOD", FuncoesAndroid.parseDate(cPreVendaSync
                        .getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_DAT_MOD())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_VAL_SINAL", cPreVendaSync
                        .getFloat(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_SINAL()),
                    CFFCommitFields.FLOAT);
                if (cPreVendaSync.isNull(CFFDbPrePedido.Companion.getPREVENDA_FIELD_EMP_SEQ())) {
                    commitFieldsPreVenda.Add("EMP_SEQ", app.getCurrentConfig()
                            .getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_EMP_SEQ()),
                        CFFCommitFields.INTEGER);
                } else {
                    commitFieldsPreVenda.Add("EMP_SEQ",
                        cPreVendaSync.getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_EMP_SEQ()),
                        CFFCommitFields.INTEGER);
                }
                commitFieldsPreVenda.Add("PVE_IND_ENTRG_MERC", cPreVendaSync
                        .getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_IND_ENTRG_MERC()),
                    CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_OBS",
                    cPreVendaSync.getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_OBS()),
                    CFFCommitFields.STRING);
                commitFieldsPreVenda.Add("PVE_IND_CUPOM_NF", cPreVendaSync
                        .getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_IND_CUPOM_NF()),
                    CFFCommitFields.STRING);
                if (!cPreVendaSync.isNull(24)) {
                    commitFieldsPreVenda.Add("FOR_SEQ_TRANSP", cPreVendaSync
                            .getInt(CFFDbPrePedido.Companion.getPREVENDA_FIELD_FOR_SEQ_TRANSP()),
                        CFFCommitFields.INTEGER);
                }
                commitFieldsPreVenda.Add("PVE_DAT_PREV_ENTRG", FuncoesAndroid.parseDate(
                    cPreVendaSync.getString(
                        CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_DAT_PREV_ENTRG())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_TIP_QUITACAO", 0, CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_IND_PVE_ORC", 2, CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_HOR_CAD", FuncoesAndroid.parseDateTime(cPreVendaSync
                        .getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_HOR_CAD())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_HOR_PRE_PED", FuncoesAndroid.parseDateTime(
                    cPreVendaSync
                        .getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_HOR_PRE_PED())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_DAT_PRE_PED", FuncoesAndroid.parseDate(cPreVendaSync
                        .getString(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_DAT_PRE_PED())),
                    CFFCommitFields.DATE);
                commitFieldsPreVenda.Add("PVE_VAL_FRE", 0.0, CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_VAL_TAC", 0.0, CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_STA_EXE_SERV", 0, CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_IND_PROJ", 0, CFFCommitFields.INTEGER);
                commitFieldsPreVenda.Add("PVE_PER_ACRES", 0.0, CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_PER_DESC", cPreVendaSync
                        .getDouble(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_PER_DESC()),
                    CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_VAL_DESC_ITEM", cPreVendaSync.getDouble(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_DESC_ITEM()), CFFCommitFields.FLOAT);
                commitFieldsPreVenda.Add("PVE_VAL_ACRES_ITEM", cPreVendaSync.getDouble(CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_VAL_ACRES_ITEM()), CFFCommitFields.FLOAT);

                commitFields.add(commitFieldsPreVenda);

                String qItemPreVendaSync = "SELECT * FROM ITEM_PRE_VENDA WHERE PVE_SEQ = "
                    + cPreVendaSync.getString(0);
                Cursor cItemPreVendaSync = dbPrePedido.rawQuery(
                    qItemPreVendaSync, null);
                while (cItemPreVendaSync.moveToNext()) {

                    CFFCommitFields commitFieldsItemPreVenda = new CFFCommitFields(
                        "ECF_ITEM_PRE_VENDA", CFFCommitFields.INSERT);
                    commitFieldsItemPreVenda.Add("PVE_SEQ", pveSeq, CFFCommitFields.INTEGER);
                    commitFieldsItemPreVenda
                        .Add("IPV_SEQ", cItemPreVendaSync.getInt(2), CFFCommitFields.INTEGER);
                    commitFieldsItemPreVenda
                        .Add("PRO_SEQ", cItemPreVendaSync.getInt(3), CFFCommitFields.INTEGER);
                    commitFieldsItemPreVenda
                        .Add("EST_SEQ", cItemPreVendaSync.getInt(4), CFFCommitFields.INTEGER);
                    commitFieldsItemPreVenda
                        .Add("IPV_STATUS", cItemPreVendaSync.getInt(5), CFFCommitFields.INTEGER);
                    commitFieldsItemPreVenda
                        .Add("IPV_QTD", cItemPreVendaSync.getFloat(6), CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda
                        .Add("IPV_VAL_UNIT", cItemPreVendaSync.getFloat(7), CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda
                        .Add("IPV_VAL_TOT", cItemPreVendaSync.getFloat(8), CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda
                        .Add("IPV_VAL_LIQ", cItemPreVendaSync.getFloat(9), CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda
                        .Add("IPV_VAL_UNIT_DIGITA", cItemPreVendaSync.getFloat(10),
                            CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda
                        .Add("IPV_VAL_DESC", cItemPreVendaSync.getFloat(11), CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda
                        .Add("IPV_PER_DESC", cItemPreVendaSync.getFloat(12), CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda.Add("IPV_VAL_ACRES", cItemPreVendaSync.getFloat(13),
                        CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda.Add("IPV_PER_ACRES", cItemPreVendaSync.getFloat(14),
                        CFFCommitFields.FLOAT);
                    commitFieldsItemPreVenda.Add("IPV_DAT_MOD",
                        FuncoesAndroid.parseDate(cItemPreVendaSync.getString(15)),
                        CFFCommitFields.DATE);
                    commitFieldsItemPreVenda
                        .Add("IPV_OBS", cItemPreVendaSync.getString(16), CFFCommitFields.STRING);
                    commitFields.add(commitFieldsItemPreVenda);

                }
                String qFormaCondPagamSync =
                    "SELECT * FROM FORMA_COND_PAGAM_PRE_VENDA WHERE PVE_SEQ = "
                        + cPreVendaSync.getString(0);
                Cursor cFormaCondPagamSync = dbPrePedido.rawQuery(
                    qFormaCondPagamSync, null);
                while (cFormaCondPagamSync.moveToNext()) {

                    CFFCommitFields commitFieldsCondPagam = new CFFCommitFields(
                        "ECF_FORMA_COND_PAGAM_PRE_VENDA", CFFCommitFields.INSERT);
                    commitFieldsCondPagam.Add("PVE_SEQ", pveSeq, CFFCommitFields.INTEGER);
                    commitFieldsCondPagam
                        .Add("FCP_SEQ", cFormaCondPagamSync.getInt(2), CFFCommitFields.INTEGER);
                    commitFieldsCondPagam
                        .Add("CON_PAG_SEQ", cFormaCondPagamSync.getInt(3), CFFCommitFields.INTEGER);
                    commitFieldsCondPagam
                        .Add("FOR_PAG_SEQ", cFormaCondPagamSync.getInt(4), CFFCommitFields.INTEGER);
                    commitFieldsCondPagam.Add("FCP_VAL_NEGOC", cFormaCondPagamSync.getFloat(5),
                        CFFCommitFields.FLOAT);
                    commitFieldsCondPagam.Add("FCP_DAT_VENC",
                        FuncoesAndroid.parseDate(cFormaCondPagamSync.getString(6)),
                        CFFCommitFields.DATE);
                    commitFieldsCondPagam.Add("FCP_NUM_BANCO", cFormaCondPagamSync.getString(7),
                        CFFCommitFields.STRING);
                    commitFieldsCondPagam.Add("FCP_NUM_AGENCIA", cFormaCondPagamSync.getString(8),
                        CFFCommitFields.STRING);
                    commitFieldsCondPagam.Add("FCP_NUM_CONTA", cFormaCondPagamSync.getString(9),
                        CFFCommitFields.STRING);
                    commitFieldsCondPagam.Add("FCP_NUM_CHEQUE", cFormaCondPagamSync.getString(10),
                        CFFCommitFields.STRING);
                    commitFieldsCondPagam.Add("FCP_NOM_TITULAR", cFormaCondPagamSync.getString(11),
                        CFFCommitFields.STRING);
                    commitFieldsCondPagam.Add("FCP_VAL_LIQ_EMP", cFormaCondPagamSync.getFloat(12),
                        CFFCommitFields.FLOAT);

                    commitFields.add(commitFieldsCondPagam);

                }

                commitFields.add(commitFieldsAutoInc);

                CFFCommitFields[] commit = new CFFCommitFields[commitFields.size()];
                commitFields.toArray(commit);
                app.getAndroidSocket().CommitTable(commit);

                ContentValues valuesPve = new ContentValues();
                valuesPve.put("SYNC_STATUS", 2);
                dbPrePedido.update("PRE_VENDA", valuesPve, "PVE_SEQ = "
                    + String.valueOf(cPreVendaSync.getInt(0)), null);
            }
        } catch (Exception ex) {
            CFFActivityUtil.GeraLogErro(getApp().getBaseContext(), ex, this.getClass().toString());
            return new Pair(false, "Ocorreu um erro sincronizando o pedido");
        }

        return new Pair(true, "Pedidos sincronizado com sucesso");

    }

    private EcfPreVenda NovoPedido() {

        String queryMax = "SELECT IFNULL(MAX(PVE_SEQ), 0)+1 AS PVE_SEQ FROM PRE_VENDA";

        Cursor cMax = getApp().getDbPrePedido().rawQuery(queryMax, null);
        cMax.moveToFirst();
        EcfPreVenda ecfPreVenda = new EcfPreVenda();
        try {
            Calendar calendar = Calendar.getInstance(new Locale("pt", "BR"));
            String currentDate = String
                .format(new Locale("pt", "BR"), "%02d/%02d/%02d %02d:%02d:%02d",
                    calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1,
                    calendar.get(Calendar.YEAR), 0, 0, 0);

            ecfPreVenda.setPveDat(FuncoesAndroid.formataData(currentDate));
            ecfPreVenda.setPveIndProj(0);
            ecfPreVenda.setPveSeq(cMax.getInt(0));
            ecfPreVenda.setPveNum("001001");
            ecfPreVenda.setEmpSeq(getData().getExtras().getInt(CFFApplicationContext.Companion.getARG_EMPRESA()));
            ecfPreVenda.setPveDatPrePed(ecfPreVenda.getPveDat());
            ecfPreVenda.setPveHorPrePed(calendar.getTime());
            ecfPreVenda.setPveQtdItens(0);
            ecfPreVenda.setPveQtdProd(0.0d);
            ecfPreVenda.setPveDatMod(ecfPreVenda.getPveDat());
            ecfPreVenda.setPveEmailEnviado(0);
            ecfPreVenda.setPveHorCad(calendar.getTime());
            ecfPreVenda.setPveIndCupomNf("2");
            ecfPreVenda.setPveStatus("8");
            ecfPreVenda.setPveValFre(0.0d);
            ecfPreVenda.setPveValSinal(0.0d);
            ecfPreVenda.setPveValLiq(0.0d);
            ecfPreVenda.setPveValTot(0.0d);

            ecfPreVenda.setTerSeq(getData().getExtras().getInt(CFFApplicationContext.Companion.getARG_TER_SEQ()));
            ecfPreVenda.setPveValDesc(0.0d);
            ecfPreVenda.setCliPerDescVenda(0.0d);
            ecfPreVenda.setSyncStatus(0);
            ecfPreVenda.setFunSeqVen(
                    getData().getExtras().getInt(CFFApplicationContext.Companion.getARG_FUN_SEQ()));

        } finally {
            cMax.close();
        }

        CFFDbPrePedido.Companion.CommitPreVenda(getApp().getDbPrePedido(), ecfPreVenda);

        ViewGroup content = findViewById(R.id.content);

        Cursor cPreVenda = getApp().getDbPrePedido().rawQuery(String
            .format(new Locale("pt", "BR"), "SELECT * FROM PRE_VENDA WHERE PVE_SEQ = %d",
                ecfPreVenda.getPveSeq()), null);
        if (cPreVenda.getCount() > 0) {
            cPreVenda.moveToFirst();
            LoadDataSet(content, cPreVenda);
        }

        return ecfPreVenda;

    }

    private void LimpaCampos() {

        TextView lblCodigoPedido = (TextView) pedidoGeral.getMainView()
            .findViewById(R.id.lblCodigoPedido);
        TextView lblDataPedido = (TextView) pedidoGeral.getMainView()
            .findViewById(R.id.lblDataPedido);

        lblCodigoPedido.setText("");
        lblDataPedido.setText("");

        EditText edtCliente = (EditText) pedidoGeral.getMainView().findViewById(R.id.edtCliente);
        EditText edtTransportador = (EditText) pedidoGeral.getMainView()
            .findViewById(R.id.edtTransportador);
        EditText edtTotalBruto = (EditText) pedidoGeral.getMainView()
            .findViewById(R.id.edtTotalBruto);
        EditText edtDesconto = (EditText) pedidoGeral.getMainView().findViewById(R.id.edtDesconto);
        EditText edtValorDesconto = (EditText) pedidoGeral.getMainView()
            .findViewById(R.id.edtValorDesconto);
        EditText edtValorTotalPedido = (EditText) pedidoGeral.getMainView()
            .findViewById(R.id.edtValorTotalPedido);
        EditText edtObsPedido = (EditText) pedidoGeral.getMainView()
            .findViewById(R.id.edtObsPedido);

        edtTotalBruto.setText("");
        edtDesconto.setText("");
        edtValorDesconto.setText("");
        edtValorTotalPedido.setText("");
        edtCliente.setText("");
        edtTransportador.setText("");
        edtObsPedido.setText("");

        EditText edtQuantidadeTotalPedido = (EditText) itemPedido.getMainView()
            .findViewById(R.id.edtQuantidadeTotalPedido);
        EditText edtValorTotalItens = (EditText) itemPedido.getMainView()
            .findViewById(R.id.edtValorTotalItens);

        edtQuantidadeTotalPedido.setText("");
        edtValorTotalItens.setText("");

        EditText edtRecebValor = (EditText) recebimento.getMainView()
            .findViewById(R.id.edtRecebValor);
        EditText edtRecebTotal = (EditText) recebimento.getMainView()
            .findViewById(R.id.edtRecebTotal);

        edtRecebValor.setText("");
        edtRecebTotal.setText("");

        RecyclerView rvItem = (RecyclerView) itemPedido.getMainView().findViewById(R.id.rvItem);
        rvItem.setLayoutManager(
            new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false));
        CFFRecyclerRowAdapter<String> emptyAdapter = new CFFRecyclerRowAdapter<>();
        emptyAdapter.addRow(R.layout.nenhumregistro_recycler, "", null);
        rvItem.setAdapter(emptyAdapter);
        emptyAdapter.notifyDataSetChanged();

        ListView lstFormaPagamento = (ListView) recebimento.getMainView()
            .findViewById(R.id.lstFormaPagamento);
        lstFormaPagamento.setAdapter(null);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainActivity.preVenda = null;
        SharedPreferences prefs = getSharedPreferences(MainActivity.PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(MainActivity.PREF_NOVO_PEDIDO, 0);
        editor.apply();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (data != null) {
                    if (data.getExtras() != null) {
                        CarregaPreVenda(data);
                    }
                }
                break;
            case 2002:

                GoogleApiAvailability apiAvailability =
                    GoogleApiAvailability.getInstance();
                int connectionStatusCode =
                    apiAvailability.isGooglePlayServicesAvailable(this);
                showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);

                break;

            case 2001:

                if (resultCode == RESULT_OK) {

                    if (FuncoesAndroid.isDeviceOnline(this)) {
                        taskMail.Execute();
                    }
                }

                break;

            case REQUEST_SEND_EMAIL:

                if (resultCode == RESULT_OK) {

                    SharedPreferences prefs = getSharedPreferences(AccountManager.KEY_ACCOUNT_NAME,
                        MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(AccountManager.KEY_ACCOUNT_NAME,
                        data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
                    editor.apply();

                    if (FuncoesAndroid.isDeviceOnline(this)) {
                        taskMail.Execute();
                    }
                }
                break;

            case REQUEST_INIT_EMAIL:

                if (resultCode == RESULT_OK) {
                    SharedPreferences prefs = getSharedPreferences(AccountManager.KEY_ACCOUNT_NAME,
                        MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(AccountManager.KEY_ACCOUNT_NAME,
                        data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
                    editor.apply();
                }
                if (resultCode == 0) {
                    SharedPreferences prefPreVenda = getSharedPreferences(PREF_NOVO_PEDIDO,
                        MODE_PRIVATE);
                    SharedPreferences.Editor editorPedido = prefPreVenda.edit();
                    editorPedido.putBoolean(PREF_ENVIA_EMAIL, false);
                    editorPedido.apply();
                }

                break;
            case REQUEST_COMPARTILHAR:
                if (data != null) {
                    if (data.getExtras() != null) {
                        CarregaPreVenda(data);
                        onClickShare(OrcamentoString());
                    }
                }
                break;
        }
    }

    private void CarregaPreVenda(Intent data) {
        int pveSeq = data.getIntExtra("codigo", 0);
        if (pveSeq != 0) {
            Cursor cPreVenda = getApp().getDbPrePedido().rawQuery(String
                .format(new Locale("pt", "BR"),
                    "SELECT P.*, C.CLI_NOM, F.FOR_NOM FROM PRE_VENDA P LEFT JOIN TRANSPORTADOR F ON ( F.FOR_SEQ = P.FOR_SEQ_TRANSP ), CLIENTE C WHERE C.CLI_SEQ = P.CLI_SEQ AND P.PVE_SEQ = %d",
                    pveSeq), null);
            Cursor cItemPreVenda = getApp().getDbPrePedido().rawQuery(String
                .format(new Locale("pt", "BR"), "SELECT I.*, P.PRO_DSC_RESUM FROM ITEM_PRE_VENDA I, PRODUTO P WHERE P.PRO_SEQ = I.PRO_SEQ AND PVE_SEQ = %d",
                    pveSeq), null);
            Cursor cFormaCond = getApp().getDbPrePedido().rawQuery(String
                .format(new Locale("pt", "BR"),
                    "SELECT * FROM FORMA_COND_PAGAM_PRE_VENDA WHERE PVE_SEQ = %d", pveSeq), null);
            try {
                if (cPreVenda.getCount() > 0) {
                    cPreVenda.moveToFirst();
                    EcfPreVenda preVenda = CFFDbPrePedido.Companion
                        .LoadPreVenda(cPreVenda, cItemPreVenda, cFormaCond);
                    if (preVenda != null) {
                        preVenda.setCliNom(cPreVenda.getString(cPreVenda.getColumnIndex("CLI_NOM")));
                        MainActivity.preVenda = preVenda;
                        LoadDataSet(findViewById(R.id.content), cPreVenda);
                        itemPedido.CarregaItens(null);
                        if (MainActivity.preVenda.getEcfFormaCondPagamPreVendas().size() > 0) {
                            List<ListTextObjectAdapter2> lstObject = new ArrayList<ListTextObjectAdapter2>();

                            for (EcfFormaCondPagamPreVenda forma : MainActivity.preVenda
                                .getEcfFormaCondPagamPreVendas()) {
                                String qryFormas = "SELECT F.FOR_PAG_DSC, C.CON_PAG_DSC " +
                                    " FROM CONDICAO_PAGAMENTO C, " +
                                    "             FORMA_PAGAMENTO F " +
                                    " WHERE C.FOR_PAG_SEQ = F.FOR_PAG_SEQ " +
                                    "   AND F.FOR_PAG_SEQ = ? " +
                                    "   AND C.CON_PAG_SEQ = ? " +
                                    " ORDER BY F.FOR_PAG_DSC, C.CON_PAG_DSC";
                                Cursor cPagamentos = getApp().getDbPrePedido().rawQuery(qryFormas,
                                    new String[]{forma.getForPagSeq().toString(),
                                        forma.getConPagSeq().toString()});
                                if(cPagamentos.getCount() > 0) {
                                    cPagamentos.moveToFirst();
                                    ListTextObjectAdapter2 adapter2 = new ListTextObjectAdapter2(
                                            forma.getFcpSeq(), cPagamentos.getString(1),
                                            funcoes.getFloatFormated(forma.getFcpValNegoc().toString(), 0));
                                    lstObject.add(adapter2);
                                }
                            }
                            ItemAdapter2 adapter = new ItemAdapter2(recebimento.getCFFActivity(),
                                R.layout.listitemprovider, lstObject);
                            ListView lstFormaPagamento = recebimento.getMainView()
                                .findViewById(R.id.lstFormaPagamento);
                            if (lstFormaPagamento != null) {
                                lstFormaPagamento.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                                CFFEditText edtRecebValor = recebimento.getMainView()
                                    .findViewById(R.id.edtRecebValor);
                                if (edtRecebValor != null) {
                                    edtRecebValor.setText(funcoes.getFloatFormated("0.00", 0));
                                }
                            }

                            EditText edtObs = pedidoGeral.getMainView()
                                .findViewById(R.id.edtObsPedido);
                            if (edtObs != null) {
                                edtObs.setFocusable(true);
                                edtObs.setFocusableInTouchMode(true);
                            }

                        }
                    }
                }
            } finally {
                cPreVenda.close();
                cItemPreVenda.close();
                cFormaCond.close();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    public Pair<Boolean, String> CadastraCliente(){
        SQLiteDatabase dbPrePedido = getApp().getDbPrePedido();
        AndroidSocket androidSocket = getApp().getAndroidSocket();
        try {

            String qClienteSync = "SELECT * FROM CLIENTE WHERE CLI_SEQ < 0";
            Cursor cClienteSync = dbPrePedido
                    .rawQuery(qClienteSync, null);

            if(cClienteSync.getCount() > 0 ) {
                while(cClienteSync.moveToNext()){
                    CFFDataSet dsVerificaCliente = androidSocket.getQuery(String.format(new Locale("pt","BR") ,"SELECT CLI_SEQ, CLI_NUM_CPF_CNPJ FROM ECF_CLIENTE WHERE CLI_NUM_CPF_CNPJ = '%d'", cClienteSync.getInt(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CID_SEQ())), 1);
                    if(dsVerificaCliente.getCount() > 0 ) {
                        CFFCommitFields cffCommitFields = new
                                CFFCommitFields("ECF_CLIENTE", CFFCommitFields.INSERT);

                        CFFCommitFields cffCommitVendedor = new
                                CFFCommitFields("ECF_CLIENTE_VENDEDOR", CFFCommitFields.INSERT);

                        int cliSeq = FuncoesAndroid.GetSequence(
                                androidSocket,
                                "ECF_CLIENTE",
                                "CLI_SEQ",
                                null
                        );

                        cffCommitFields.Add("CLI_SEQ", cliSeq, CFFCommitFields.INTEGER);
                        cffCommitFields.Add(
                                "CLI_NOM",
                                cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM()),
                                CFFCommitFields.STRING
                        );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_FANTASIA()) != null)
                            cffCommitFields.Add(
                                    "CLI_NOM_FANTASIA",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_FANTASIA()),
                                    CFFCommitFields.STRING
                            );
                        cffCommitFields.Add(
                                "CLI_NUM_CPF_CNPJ",
                                cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_CPF_CNPJ()),
                                CFFCommitFields.STRING
                        );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_LOGRAD()) != null)
                            cffCommitFields.Add(
                                    "CLI_NOM_LOGRAD",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_LOGRAD()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_LOGRAD()) != null)
                            cffCommitFields.Add(
                                    "CLI_NUM_LOGRAD",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_LOGRAD()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_DSC_COMPL_LOGRAD()) != null)
                            cffCommitFields.Add(
                                    "CLI_DSC_COMPL_LOGRAD",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_DSC_COMPL_LOGRAD()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_BAIRRO()) != null)
                            cffCommitFields.Add(
                                    "CLI_NOM_BAIRRO",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_BAIRRO()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_UF()) != null)
                            cffCommitFields.Add(
                                    "CLI_UF",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_UF()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_CEP()) != null)
                            cffCommitFields.Add(
                                    "CLI_CEP",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_CEP()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_CIDADE()) != null)
                            cffCommitFields.Add(
                                    "CLI_NOM_CIDADE",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_CIDADE()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getInt(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CID_SEQ()) > 0)
                            cffCommitFields.Add("CID_SEQ", cClienteSync.getInt(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CID_SEQ()), CFFCommitFields.INTEGER);
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_FONE_COM()) != null)
                            cffCommitFields.Add(
                                    "CLI_NUM_FONE_COM",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_FONE_COM()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_CELULAR()) != null)
                            cffCommitFields.Add(
                                    "CLI_NUM_CELULAR",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NUM_CELULAR()),
                                    CFFCommitFields.STRING
                            );
                        if (cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_EMAIL()) != null)
                            cffCommitFields.Add(
                                    "CLI_NOM_EMAIL",
                                    cClienteSync.getString(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_NOM_EMAIL()),
                                    CFFCommitFields.STRING
                            );
                        cffCommitFields.Add("CLI_TIP_PESSOA", cClienteSync.getInt(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_TIP_PESSOA()), CFFCommitFields.INTEGER);
                        cffCommitFields.Add("CLI_STATUS", 0, CFFCommitFields.INTEGER);
                        cffCommitFields.Add(
                                "CLI_DAT_MOD",
                                Calendar.getInstance().getTime(),
                                CFFCommitFields.DATE
                        );
                        cffCommitFields.Add(
                                "CLI_DAT_HOR_CAD",
                                Calendar.getInstance().getTime(),
                                CFFCommitFields.DATE
                        );
                        cffCommitFields.Add(
                                "CLI_DAT_CAD",
                                FuncoesAndroid.parseDate(Calendar.getInstance().getTime()),
                                CFFCommitFields.DATE
                        );

                        cffCommitVendedor.Add("CLI_SEQ", cliSeq, CFFCommitFields.INTEGER);
                        cffCommitVendedor.Add(
                                "FUN_SEQ",
                                getData().getExtras().getInt(CFFApplicationContext.Companion.getARG_FUN_SEQ()),
                                CFFCommitFields.INTEGER
                        );
                        cffCommitVendedor.Add("CVE_IND_PREF", 1, CFFCommitFields.INTEGER);

                        String returnSaved = androidSocket.CommitTable(
                                cffCommitFields,
                                cffCommitVendedor
                        );

                        if (!returnSaved.toUpperCase().contains("ERRO")) {
                            ContentValues values = new ContentValues();
                            values.put("CLI_SEQ", cliSeq);
                            dbPrePedido.update("CLIENTE", values, "CLI_SEQ = " + cClienteSync.getInt(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_SEQ()), null);
                            dbPrePedido.update("PRE_VENDA", values, "CLI_SEQ = " + cClienteSync.getInt(CFFDbPrePedido.Companion.getCLIENTE_FIELD_CLI_SEQ()), null);
                        }
                    } else {
                        ContentValues values = new ContentValues();
                        values.put("CLI_SEQ", dsVerificaCliente.getInteger("CLI_SEQ"));
                        dbPrePedido.update("CLIENTE", values, "CLI_SEQ = " + dsVerificaCliente.getInteger("CLI_SEQ"), null);
                        dbPrePedido.update("PRE_VENDA", values, "CLI_SEQ = " + dsVerificaCliente.getInteger("CLI_SEQ"), null);
                    }
                }
            } else {
                return new Pair(false, "Nenhum Cliente a ser sincronizado");
            }


        }catch(Exception ex){
            return new Pair(false, "Ocorreu um erro ao sincronizar clientes");
        }

        return new Pair( true, "Clientes sincronizados com sucesso!");
    }
    public void SincronizaCliente(){
        final ProgressDialog progressDialog = new ProgressDialog(this);

        NetworkTasks task = new NetworkTasks(
                self -> {
                    progressDialog.setMessage(getString(R.string.process_inicia_sincronizacao));
                    progressDialog.setTitle("Mensagem do Sistema.");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    try {
                        CFFApplicationContext.Companion
                                .BackupDatabase(getApp().getApplicationContext());
                        getWindow().addFlags(
                                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                },
                (self, params) -> {
                    try {
                        Pair retornoSinc =  CadastraCliente();
                        return new NetworkTasks.TaskResponse(retornoSinc.first, retornoSinc.second);
                    } catch (Exception ex) {
                        return new NetworkTasks.TaskResponse(false,
                                funcoes.getStringValue(ex.getMessage()));
                    }
                },
                (aVoid, self) -> {
                    progressDialog.dismiss();
                    if ((Boolean) aVoid.getResponse()) {
                        Snackbar.make(viewPager, aVoid.getStatus().toString(),
                                Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                    } else {
                        Snackbar.make(viewPager, aVoid.getStatus().toString(),
                                Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                    }

                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
        );
        task.Execute();
    }

    public void SincronizaPreVenda(){
        final ProgressDialog progressDialog = new ProgressDialog(this);

        NetworkTasks task = new NetworkTasks(
                self -> {
                    progressDialog.setMessage(getString(R.string.process_inicia_sincronizacao));
                    progressDialog.setTitle("Mensagem do Sistema.");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    try {
                        CFFApplicationContext.Companion
                                .BackupDatabase(getApp().getApplicationContext());
                        getWindow().addFlags(
                                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                },
                (self, params) -> {
                    try {
                        Pair retornoSinc = Sincroniza(getApp());
                        return new NetworkTasks.TaskResponse(retornoSinc.first, retornoSinc.second);
                    } catch (Exception ex) {
                        return new NetworkTasks.TaskResponse(false,
                                funcoes.getStringValue(ex.getMessage()));
                    }
                },
                (aVoid, self) -> {
                    progressDialog.dismiss();
                    if ((Boolean) aVoid.getResponse()) {
                        Snackbar.make(viewPager, aVoid.getStatus().toString(),
                                Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                    } else {
                        Snackbar.make(viewPager, aVoid.getStatus().toString(),
                                Snackbar.LENGTH_LONG)
                                .setAction("action", null)
                                .show();
                    }

                    TextView txvPedidos = (TextView) navigationView.getHeaderView(0)
                            .findViewById(R.id.txvPedidos);
                    if (txvPedidos != null) {
                        Cursor cPreVendasSinc = getApp().getDbPrePedido().rawQuery(
                                "SELECT COUNT(PVE_SEQ) FROM PRE_VENDA WHERE SYNC_STATUS <> 2",
                                null);
                        try {
                            if (cPreVendasSinc.getCount() > 0) {
                                cPreVendasSinc.moveToFirst();
                                txvPedidos.setText(String.format(new Locale("pt", "BR"),
                                        "%d Pedido(s) não sincronizados",
                                        cPreVendasSinc.getInt(0)));
                            }
                        } finally {
                            cPreVendasSinc.close();
                        }
                    }

                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
        );
        task.Execute();
    }

    public void RemovePedidoIncompleto(){

        final Cursor cPedidosIncompletos = getApp().getDbPrePedido()
                .rawQuery("SELECT * FROM PRE_VENDA WHERE SYNC_STATUS = 0 AND CLI_SEQ IS NULL", null);

        if (cPedidosIncompletos.getCount() > 0) {
            while (cPedidosIncompletos.moveToNext()) {
                getApp().getDbPrePedido().delete("PRE_VENDA", "PVE_SEQ = ?",
                        new String[]{cPedidosIncompletos.getString(
                                CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())});
                getApp().getDbPrePedido().delete("ITEM_PRE_VENDA", "PVE_SEQ = ?",
                        new String[]{cPedidosIncompletos.getString(
                                CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())});
                getApp().getDbPrePedido()
                        .delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?", new String[]{
                                cPedidosIncompletos.getString(
                                        CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())});

                TextView txvPedidos12 = navigationView.getHeaderView(0)
                        .findViewById(R.id.txvPedidos);
                if (txvPedidos12 != null) {
                    Cursor cPreVendasSinc = getApp().getDbPrePedido().rawQuery(
                            "SELECT COUNT(PVE_SEQ) FROM PRE_VENDA WHERE SYNC_STATUS <> 2",
                            null);
                    try {
                        if (cPreVendasSinc.getCount() > 0) {
                            cPreVendasSinc.moveToFirst();
                            txvPedidos12.setText(String.format(new Locale("pt", "BR"),
                                    "%d Pedido(s) não sincronizados",
                                    cPreVendasSinc.getInt(0)));
                        }
                    } finally {
                        cPreVendasSinc.close();
                    }
                }
            }
        }

        final Cursor cPedidosEdicao = getApp().getDbPrePedido()
                .rawQuery("SELECT * FROM PRE_VENDA WHERE SYNC_STATUS = 0 AND NOT CLI_SEQ IS NULL", null);

        if (cPedidosEdicao.getCount() > 0) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.msg_sistema);
            dialog.setMessage("Existem pedidos em edição. Abandonar alterações ?");
            dialog.setPositiveButton("Sim", (dialog1, which) -> {
                if (cPedidosEdicao.getCount() > 0) {
                    while (cPedidosEdicao.moveToNext()) {
                        getApp().getDbPrePedido().delete("PRE_VENDA", "PVE_SEQ = ?",
                                new String[]{cPedidosEdicao.getString(
                                        CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())});
                        getApp().getDbPrePedido().delete("ITEM_PRE_VENDA", "PVE_SEQ = ?",
                                new String[]{cPedidosEdicao.getString(
                                        CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())});
                        getApp().getDbPrePedido()
                                .delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = ?", new String[]{
                                        cPedidosEdicao.getString(
                                                CFFDbPrePedido.Companion.getPREVENDA_FIELD_PVE_SEQ())});

                        TextView txvPedidos12 = navigationView.getHeaderView(0)
                                .findViewById(R.id.txvPedidos);
                        if (txvPedidos12 != null) {
                            Cursor cPreVendasSinc = getApp().getDbPrePedido().rawQuery(
                                    "SELECT COUNT(PVE_SEQ) FROM PRE_VENDA WHERE SYNC_STATUS <> 2",
                                    null);
                            try {
                                if (cPreVendasSinc.getCount() > 0) {
                                    cPreVendasSinc.moveToFirst();
                                    txvPedidos12.setText(String.format(new Locale("pt", "BR"),
                                            "%d Pedido(s) não sincronizados",
                                            cPreVendasSinc.getInt(0)));
                                }
                            } finally {
                                cPreVendasSinc.close();
                            }
                        }
                    }
                }
                dialog1.dismiss();
            });
            dialog.setNegativeButton("Não", (dialog12, which) -> dialog12.dismiss());
            dialog.show();
        }
    }

    public String OrcamentoString(){
        if(preVenda == null) return "";

        int cSpace = 27;

        String separador = new String(new char[cSpace]).replace("\0", "-")+"\n";

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));

        StringBuilder str = new StringBuilder();
        str.append("*No celular, gire a tela p/ ver melhor*\n");
        str.append("```");
        str.append(separador);
        str.append(funcoes.alinhaTexto("VENDEDOR:", 13, "E")+funcoes.alinhaTexto(getData().getStringExtra(CFFApplicationContext.Companion.getARG_FUN_NOM()), cSpace-13, "E")+"\n");
        str.append(funcoes.alinhaTexto("DATA PEDIDO:", 13, "E")+funcoes.alinhaTexto(sdf.format(preVenda.getPveDat()), cSpace-13, "E")+"\n");
        str.append(funcoes.alinhaTexto("CLIENTE:", 13, "E")+funcoes.alinhaTexto(preVenda.getCliNom(), cSpace-13, "E")+"\n");
        str.append(funcoes.alinhaTexto("QTD. ITENS:", 13, "E")+funcoes.alinhaTexto(funcoes.getFloatFormated(String.valueOf(preVenda.getPveQtdItens()), 2), cSpace-13, "E")+"\n");
        str.append(separador);
        str.append(funcoes.alinhaTexto("ITEM", 27, "E") + "\n");
        str.append(funcoes.alinhaTexto("VALOR", 9 ,"E") + funcoes.alinhaTexto("QTDE.", 9 ,"C") + funcoes.alinhaTexto("TOTAL", 9, "D")+"\n");
        str.append(separador);
        for(EcfItemPreVenda item : preVenda.getEcfItensPreVenda()){
            str.append(item.getProDscResum()+"\n");
            str.append(funcoes.alinhaTexto(funcoes.getFloatFormated(String.valueOf(item.getIpvValUnit()),2), 9, "E") + funcoes.alinhaTexto(funcoes.getFloatFormated(String.valueOf(item.getIpvQtd()),2), 9, "C") + funcoes.alinhaTexto(funcoes.getFloatFormated(String.valueOf(item.getIpvValLiq()),2), 9, "D")+"\n");
        }
        str.append(separador);
        str.append(funcoes.alinhaTexto("TOTAL:", 15, "E")+funcoes.alinhaTexto("```    *"+funcoes.getFloatFormated(String.valueOf(preVenda.getPveValLiq()),2), cSpace-8, "D")+"*");
        return str.substring(0);
    }

    public void onClickShare(String text) {

        Intent waIntent = new Intent();
        waIntent.setAction(Intent.ACTION_SEND);
        waIntent.setType("text/plain");
        waIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(waIntent, "Share with"));

    }


}

