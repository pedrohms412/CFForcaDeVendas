package com.compu.android.forcadevendas

import android.os.Bundle
import android.os.Message
import android.view.MenuItem
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import com.compu.android.fragments.CFFFragmentActivity
import com.google.android.material.snackbar.Snackbar
import java.util.Locale

class DadosClienteActivity : CFFFragmentActivity() {


    val qCliente = "SELECT C.CLI_NOM, C.CLI_NOM_FANTASIA, C.CLI_NOM_LOGRAD, C.CLI_NUM_LOGRAD,"+
                   "       C.CLI_DSC_COMPL_LOGRAD, C.CLI_NOM_BAIRRO, C.CLI_UF, C.CLI_CEP,"+
                   "       C.CLI_NOM_CIDADE, C.CLI_NUM_FONE_RES, C.CLI_NUM_FONE_COM,"+
                   "       C.CLI_NUM_CELULAR, C.CLI_NOM_EMAIL, C.CLI_NUM_CPF_CNPJ "+
                   "  FROM CLIENTE C WHERE C.CLI_SEQ = %d"

    override fun onCreate(arg0: Bundle?) {
        super.onCreate(arg0)
        setContentView(R.layout.dados_cliente_activity)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        InitInterface(arg0, toolbar, "Dados do Cliente", true, false, true, false)
        val cCliente = app.dbPrePedido!!.rawQuery(String.format(Locale("pt", "BR"), qCliente, data.extras!!.getInt("CLI_SEQ")), null)
        if(cCliente.count > 0){
            cCliente.moveToFirst()
            CFFFragmentActivity.LoadDataSet(findViewById<ViewGroup>(android.R.id.content).getChildAt(0) as ViewGroup , cCliente)
        } else {
            Snackbar.make(findViewById<ViewGroup>(android.R.id.content).getChildAt(0), "Nenhum registro encontrado", Snackbar.LENGTH_LONG)
                    .setAction("action", null)
                    .show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item?.let {
            val id = it.itemId
            if (id == android.R.id.home) {
                setResult(0)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun HandleCallMessage(msg: Message?) { }

}