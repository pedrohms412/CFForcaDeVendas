package com.compu.android.forcadevendas.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.compu.android.adapter.ItemAdapter2;
import com.compu.android.adapter.ListTextObjectAdapter2;
import com.compu.android.application.CFFApplicationContext;
import com.compu.android.forcadevendas.MainActivity;
import com.compu.android.forcadevendas.R;
import com.compu.android.fragments.CFFFragment;
import com.compu.android.persistencia.EcfFormaCondPagamPreVenda;
import com.compu.android.util.CFFActivityUtil;
import com.compu.lib.funcoes;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class FragmentRecebimento extends CFFFragment {

    private View MainView;
    private DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
    private EditText edtFormaPagamento = null;
    private EditText edtCondPagamento = null;
    private EditText edtRecebTotal = null;
    private EditText edtRecebValor = null;
    private EditText edtValorCondicao = null;
    private ListView lstFormaPagamento = null;
    private Menu menu;
    public static boolean utiliza_condicao_pre_definida;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setMainView(inflater.inflate(R.layout.recebimentopedido, container, false));

        setHasOptionsMenu(true);

        return getMainView();
    }

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    @Override
    protected void initComponents() {
        if(getMainView() != null){
            edtFormaPagamento = getMainView().findViewById(R.id.edtFormaPagamento);
            edtCondPagamento = getMainView().findViewById(R.id.edtCondPagamento);
            edtRecebTotal = getMainView().findViewById(R.id.edtRecebTotal);
            edtRecebValor = getMainView().findViewById(R.id.edtRecebValor);
            lstFormaPagamento = getMainView().findViewById(R.id.lstFormaPagamento);
            edtFormaPagamento.setOnTouchListener(OnTouchGeraPagamento );
            edtCondPagamento.setOnTouchListener(OnTouchGeraPagamento);
        }
    }

    private View.OnTouchListener OnTouchGeraPagamento = new View.OnTouchListener() {

        @SuppressLint("NewApi")
        public boolean onTouch(View v, MotionEvent event) {
            // TODO Auto-generated method stub
//            if(getData().getExtras().getInt("SYNC_STATUS") == 3)
//                return false;
//            if(getData().getExtras().getInt("PRO_SEQ") > 0
//                    || getData().getExtras().getInt("IPV_SEQ") > 0){
//                Toast.makeText(getActivity(), "Voce tem um produto em edicao", Toast.LENGTH_SHORT).show();
//                return false;
//            }
            if(MainActivity.preVenda.getSyncStatus() == 2) return false;
            SQLiteDatabase dbTemp = getCFFApp().getDbPrePedido();
            try{
                switch(v.getId()){
                    case R.id.edtFormaPagamento:
//					if(getData().getExtras().getInt("PVE_SEQ") == 0)
//						return false;
//					if(getData().getExtras().getDouble("PVE_VAL_TOT") <= 0.0)
//						return false;

                        switch(event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                if(utiliza_condicao_pre_definida) {
                                    Cursor cCliente = dbTemp.rawQuery(String.format(new Locale("pt", "BR"), "SELECT * FROM CLIENTE WHERE CLI_SEQ = %d", getData().getExtras().getInt("CLI_SEQ")), null);
                                    if (cCliente.getCount() > 0) {
                                        cCliente.moveToFirst();
                                        if (cCliente.getInt(16) != 0 && cCliente.getInt(17) != 0) {
                                            getData().putExtra("FOR_PAG_SEQ", cCliente.getInt(16));
                                            getData().putExtra("CON_PAG_SEQ", cCliente.getInt(17));
                                            GeraPrazoPagamento(String.valueOf(getData().getExtras().getDouble("PVE_VAL_TOT")));
                                            getData().putExtra("FOR_PAG_SEQ", 0);
                                            getData().putExtra("CON_PAG_SEQ", 0);
                                            utiliza_condicao_pre_definida = false;
                                            break;
                                        }
                                    }
                                }

                                if((!edtRecebValor.getText().toString().equals("") && !edtRecebValor.getText().toString().equals("")) && NumberFormat.getInstance(new Locale("pt", "BR")).parse(edtRecebValor.getText().toString()).doubleValue() > 0.0) {
                                    if (!(CarregaFormaPagamento()))
                                        Toast.makeText(getActivity(), getString(R.string.err_carga_form_pag), Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                        break;
                    case R.id.edtCondPagamento:
                        dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
                        try {
//						if(getData().getExtras().getInt("FOR_PAG_SEQ") == 0 || getData().getExtras().getInt("PVE_SEQ") == 0 ||
//								getData().getExtras().getDouble("PVE_VAL_TOT") <= 0.0)
//							return false;
                        } catch (Exception e1) {
                            return false;
                        }
                        switch(event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                if(!(CarregaCondicaoPagamento()))
                                    Toast.makeText(getActivity(), getString(R.string.err_carga_cond_pag), Toast.LENGTH_SHORT).show();
                                break;

                        }
                        break;
                }
            } catch(Exception ex) {
                new CFFActivityUtil().GeraLogErro(getActivity(), ex, this.getClass().toString());
            } finally {

            }
            return false;
        }
    };

    public static FragmentRecebimento newInstance(Bundle args){
        if(args == null)
            args = new Bundle();

        FragmentRecebimento fragment = new FragmentRecebimento();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getCFFActivity().getMenuInflater().inflate(R.menu.recebimento_pedido, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {


        super.onPrepareOptionsMenu(menu);
    }

    public boolean CarregaCondicaoPagamento(){
        SQLiteDatabase dbTemp = null;
        dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
        String qCondPagamento =	String.format(
                "SELECT * FROM CONDICAO_PAGAMENTO WHERE FOR_PAG_SEQ = %s"
                , getData().getExtras().getInt("FOR_PAG_SEQ"));
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        final Cursor cCondPagamento = dbTemp.rawQuery(qCondPagamento, null);
        if(cCondPagamento.getCount() <= 0)
            return false;
        final String[] condPagamento = new String[cCondPagamento.getCount()];
        while(cCondPagamento.moveToNext()){
            condPagamento[cCondPagamento.getPosition()] = cCondPagamento.getString(1);
        }
        dialog.setTitle("Condição Recebimento:");
        dialog.setItems(condPagamento, new DialogInterface.OnClickListener() {

            @SuppressWarnings("deprecation")
            public void onClick(DialogInterface arg0, int arg1) {
                if(MainActivity.preVenda.getSyncStatus() == 2) return;
                edtCondPagamento.setText(condPagamento[arg1]);
                edtValorCondicao = new EditText(getActivity());
                edtValorCondicao.setTag("valor");
                edtValorCondicao.setKeyListener(DigitsKeyListener.getInstance("0123456789,."));
                edtValorCondicao.setRawInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
                edtValorCondicao.setText(edtRecebValor.getText());
                edtValorCondicao.setOnTouchListener(new View.OnTouchListener() {

                    public boolean onTouch(View v, MotionEvent event) {
                        // TODO Auto-generated method stub
                        ((EditText) v).setSelectAllOnFocus(true);
                        ((EditText) v).selectAll();
                        return false;
                    }
                });
                cCondPagamento.moveToPosition(arg1);
                getData().putExtra("CON_PAG_SEQ", cCondPagamento.getInt(0));
                AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
                dialog.setTitle("Digite o valor da condição");
                dialog.setView(edtValorCondicao);
                edtRecebValor.selectAll();
                edtFormaPagamento.requestFocus();
                dialog.setOnShowListener(dialog1 -> {
                    // TODO Auto-generated method stub
//							try {
//								MenuItem mnuIncluir = menu.findItem(R.id.mnuIncluirReceb);
//								if (df.parse(edtRecebValor.getText().toString()).doubleValue() <= 0.0) {
//									mnuIncluir.setIcon(R.drawable.new_pb);
//								} else {
//									mnuIncluir.setIcon(R.drawable.new_color);
//								}
//							} catch (Exception ignored){}
                    edtValorCondicao.selectAll();
                    edtValorCondicao.requestFocus();
                    edtValorCondicao.postDelayed(() -> {
                        InputMethodManager imm = (InputMethodManager) getCFFActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(edtValorCondicao, 0);
                    }, 50);
                });
                dialog.setButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        try{
                            GeraPrazoPagamento(edtValorCondicao.getText().toString());
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edtRecebValor.getWindowToken(), 0);
                            edtValorCondicao = null;
                            getData().putExtra("FOR_PAG_SEQ", 0);
                            getData().putExtra("CON_PAG_SEQ", 0);
                        }catch(Exception ex){
                            new CFFActivityUtil().GeraLogErro(getActivity(), ex, this.getClass().toString());
                        }
                    }
                });
                dialog.setOnDismissListener(dialog12 -> {
                    getData().putExtra("FOR_PAG_SEQ", 0);
                    getData().putExtra("CON_PAG_SEQ", 0);
                    edtFormaPagamento.setText("");
                    edtCondPagamento.setText("");
                });
                dialog.show();
            }
        });
        dialog.show();
        return true;
    }

    public void GeraPrazoPagamento(String valor){
        SQLiteDatabase dbTemp = null;
        String qDscPagamento = String.format(
                "SELECT F.FOR_PAG_DSC, C.CON_PAG_DSC" +
                        "	FROM FORMA_PAGAMENTO F, CONDICAO_PAGAMENTO C "+
                        " WHERE F.FOR_PAG_SEQ = C.FOR_PAG_SEQ"+
                        "   AND F.FOR_PAG_SEQ = %s "+
                        "   AND C.CON_PAG_SEQ = %s", getData().getExtras().getInt("FOR_PAG_SEQ"), getData().getExtras().getInt("CON_PAG_SEQ") );

        dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
        Cursor cDscPagamento = dbTemp.rawQuery(qDscPagamento, null);
        cDscPagamento.moveToFirst();
        if(getData().getExtras().getInt("FOR_PAG_SEQ") > 0 && getData().getExtras().getInt("CON_PAG_SEQ") > 0 && edtValorCondicao == null ){
            valor = edtRecebTotal.getText().toString();
            edtFormaPagamento.setText(cDscPagamento.getString(0));
            edtCondPagamento.setText(cDscPagamento.getString(1));
        }
        NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
        try {
            if(nf.parse(valor).floatValue()
                    > nf.parse(edtRecebValor.getText().toString()).floatValue()){
                CFFActivityUtil.GeraLogErro(getActivity(), new Exception(getString(R.string.err_inf_valor_pagamento_maior)), this.getClass().toString());
                return;
            }
        } catch (ParseException e) {
            CFFActivityUtil.GeraLogErro(getActivity(), new Exception(getString(R.string.err_parse_float)), this.getClass().toString());
            return;
        }
        List<ListTextObjectAdapter2> lstObject = new ArrayList<ListTextObjectAdapter2>();
        ItemAdapter2 iAdapter = (ItemAdapter2)lstFormaPagamento.getAdapter();
        try{
            String qPrazo =
                    "SELECT * FROM PRAZO_PAGAMENTO WHERE CON_PAG_SEQ = " + getData().getExtras().getInt("CON_PAG_SEQ");
            Cursor cPrazo = dbTemp.rawQuery(qPrazo, null);
            while(cPrazo.moveToNext()){
                Double fValorPrazo = 0.0;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
                GregorianCalendar calendar = new GregorianCalendar();
                String qFcp =
                        "SELECT COUNT(*) FROM FORMA_COND_PAGAM_PRE_VENDA";
                Cursor cFcp = dbTemp.rawQuery(qFcp, null);
                cFcp.moveToFirst();

                ContentValues values = new ContentValues();
                values.put("FCP_PRIMARY", cFcp.getInt(0) + 1);
                values.put("PVE_SEQ", MainActivity.preVenda.getPveSeq());
                values.put("FCP_SEQ", cFcp.getInt(0) + 1);
                values.put("CON_PAG_SEQ", getData().getExtras().getInt("CON_PAG_SEQ"));
                values.put("FOR_PAG_SEQ", getData().getExtras().getInt("FOR_PAG_SEQ"));

                EcfFormaCondPagamPreVenda formaCondicaoPagamento = new EcfFormaCondPagamPreVenda();
                formaCondicaoPagamento.setFcpPrimary(cFcp.getInt(0) + 1);
                formaCondicaoPagamento.setPveSeq(MainActivity.preVenda.getPveSeq());
                formaCondicaoPagamento.setFcpSeq(cFcp.getInt(0) + 1);
                formaCondicaoPagamento.setConPagSeq(getData().getExtras().getInt("CON_PAG_SEQ"));
                formaCondicaoPagamento.setForPagSeq(getData().getExtras().getInt("FOR_PAG_SEQ"));

                if(cPrazo.getPosition() == cPrazo.getCount()-1 &&
                        nf.parse(valor).doubleValue() == nf.parse(edtRecebValor.getText().toString()).doubleValue() &&
                        cPrazo.getCount() > 1){
                    Double fValorNegociado =
                            nf.parse(valor).doubleValue()*cPrazo.getDouble(4) / 100;
                    BigDecimal bd = new BigDecimal(fValorNegociado);
                    bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                    Double fDiferenca = 0.0;
                    Cursor cTemp = dbTemp.rawQuery(
                            "SELECT SUM(FCP_VAL_NEGOC) FROM FORMA_COND_PAGAM_PRE_VENDA " +
                                    "	WHERE PVE_SEQ = " + getData().getExtras().getInt("PVE_SEQ") + " AND CON_PAG_SEQ = " + getData().getExtras().getInt("CON_PAG_SEQ"), null);
                    cTemp.moveToFirst();
                    BigDecimal bdValNegoc = new BigDecimal(cTemp.getDouble(0) + bd.doubleValue());
                    if(cTemp.getDouble(0)+bd.doubleValue() != bd.doubleValue()){
                        if(bdValNegoc.doubleValue()+bd.doubleValue() > nf.parse(valor).doubleValue()){
                            fDiferenca = nf.parse(valor).doubleValue() - (bdValNegoc.doubleValue());
                        }else if(bdValNegoc.doubleValue()+bd.doubleValue() < nf.parse(valor).doubleValue()){
                            fDiferenca = nf.parse(valor).doubleValue() - (bdValNegoc.doubleValue()) ;
                        }
                    }
                    fValorPrazo = bd.doubleValue()+fDiferenca;
                    values.put("FCP_VAL_NEGOC",fValorPrazo);
                }else{
                    fValorPrazo = nf.parse(valor).doubleValue()*cPrazo.getDouble(4) / 100;
                    BigDecimal bdValorPrazo = new BigDecimal(fValorPrazo);
                    bdValorPrazo = bdValorPrazo.setScale(2,BigDecimal.ROUND_HALF_EVEN);
                    fValorPrazo =  bdValorPrazo.doubleValue();
                    values.put("FCP_VAL_NEGOC", bdValorPrazo.doubleValue());
                }
                calendar.add(GregorianCalendar.DAY_OF_MONTH, cPrazo.getInt(3));
                values.put("FCP_DAT_VENC", sdf.format(calendar.getTime()));
                values.put("FCP_NUM_BANCO", 0);
                values.put("FCP_NUM_AGENCIA", 0);
                values.put("FCP_NUM_CONTA", "");
                values.put("FCP_NUM_CHEQUE", "");
                values.put("FCP_NOM_TITULAR", "");
                values.put("FCP_VAL_LIQ_EMP", nf.parse(edtRecebTotal.getText().toString()).doubleValue()/cPrazo.getCount());

                MainActivity.preVenda.getEcfFormaCondPagamPreVendas().add(formaCondicaoPagamento);
                dbTemp.insert("FORMA_COND_PAGAM_PRE_VENDA", null, values);

                formaCondicaoPagamento.setFcpDatVenc(calendar.getTime());
                formaCondicaoPagamento.setFcpNumBanco(0);
                formaCondicaoPagamento.setFcpNumAgencia(0);
                formaCondicaoPagamento.setFcpValLiqEmp(nf.parse(edtRecebTotal.getText().toString()).doubleValue()/cPrazo.getCount());

                if(lstFormaPagamento.getAdapter() == null){
                    lstObject.add(new ListTextObjectAdapter2(getData().getExtras().getInt("CON_PAG_SEQ"), cDscPagamento.getString(1),
                            funcoes.getFloatFormated(String.valueOf(fValorPrazo),2)));
                    iAdapter = new ItemAdapter2(getActivity(), R.layout.listitemprovider, lstObject);
                }else{
                    iAdapter = (ItemAdapter2)lstFormaPagamento.getAdapter();
                    iAdapter.add(new ListTextObjectAdapter2(getData().getExtras().getInt("CON_PAG_SEQ"), cDscPagamento.getString(1),
                            funcoes.getFloatFormated(String.valueOf(fValorPrazo),2)));
                }
            }
            edtRecebValor.setText(String.format(new Locale("pt", "BR"), "%.2f", nf.parse(edtRecebValor.getText().toString()).doubleValue() -
                    nf.parse(valor).doubleValue()));


        }catch(Exception ex){
            CFFActivityUtil.GeraLogErro(getActivity(), ex, this.getClass().toString());
        }finally{
            dbTemp.close();
        }
        lstFormaPagamento.setAdapter(iAdapter);
        edtFormaPagamento.setText("");
        edtCondPagamento.setText("");
    }

    public void limpa(){
        SQLiteDatabase dbTemp = null;
        try{
            dbTemp =  ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
            if(getData().getExtras().getInt("PVE_SEQ") == 0 || lstFormaPagamento.getAdapter() == null)
                return;
            Cursor cFormaCondPagamPreVenda = dbTemp.rawQuery("SELECT * FROM FORMA_COND_PAGAM_PRE_VENDA WHERE PVE_SEQ = " + getData().getExtras().getInt("PVE_SEQ"), null);
            if(cFormaCondPagamPreVenda.getCount() > 0){
                dbTemp.delete("FORMA_COND_PAGAM_PRE_VENDA", "PVE_SEQ = " + getData().getExtras().getInt("PVE_SEQ"), null);
            }
            lstFormaPagamento.setAdapter(null);
            edtRecebValor.setText(funcoes.getFloatFormated(String.valueOf(getData().getExtras().getDouble("PVE_VAL_LIQ")),2));
            edtRecebTotal.setText(funcoes.getFloatFormated(String.valueOf(getData().getExtras().getDouble("PVE_VAL_LIQ")),2));
            edtFormaPagamento.setText("");
            edtCondPagamento.setText("");
            getData().putExtra("FOR_PAG_SEQ", 0);
            getData().putExtra("CON_PAG_SEQ", 0);
//			MenuItem miIncluir = menu.findItem(R.id.mnuIncluirReceb);
//			miIncluir.setIcon(R.drawable.new_color);

        }catch(Exception ex){
            CFFActivityUtil.GeraLogErro(getActivity(), ex, this.getClass().toString());
        }finally{
            dbTemp.close();
        }
    }

    public boolean CarregaFormaPagamento(){
        SQLiteDatabase dbTemp = null;
        dbTemp = ((CFFApplicationContext)getActivity().getApplication()).getDbPrePedido();
        if(getData().getExtras().getInt("FOR_PAG_SEQ") > 0 && getData().getExtras().getInt("CON_PAG_SEQ") > 0)
            return false;
        String qFormaPagamento = "SELECT FOR_PAG_SEQ, FOR_PAG_DSC FROM FORMA_PAGAMENTO";
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        final Cursor cFormaPagamento = dbTemp.rawQuery(qFormaPagamento, null);
        if(cFormaPagamento.getCount() <= 0)
            return false;
        final String[] formasPagamento = new String[cFormaPagamento.getCount()];
        while(cFormaPagamento.moveToNext()){
            formasPagamento[cFormaPagamento.getPosition()] = cFormaPagamento.getString(1);
        }
        dialog.setTitle("Forma Recebimento:");
        dialog.setItems(formasPagamento, (arg0, arg1) -> {
            cFormaPagamento.moveToPosition(arg1);
            getData().putExtra("FOR_PAG_SEQ",cFormaPagamento.getInt(0));
            edtFormaPagamento.setText(formasPagamento[arg1]);
        });
        if(Build.VERSION.SDK_INT > 16)
            dialog.setOnDismissListener(dialog1 -> {
                if(getData().getIntExtra("FOR_PAG_SEQ",0)!=0){
                    if(!(CarregaCondicaoPagamento()))
                        Toast.makeText(getActivity(), getString(R.string.err_carga_cond_pag), Toast.LENGTH_SHORT).show();
                }
            });
        dialog.show();
        return true;
    }

    public View getMainView() {
        return MainView;
    }

    public void setMainView(View mainView) {
        MainView = mainView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.mni_novo){
            if(MainActivity.preVenda.getSyncStatus() == 2) return false;
            boolean condicaoCliente = false;
            if(utiliza_condicao_pre_definida) {
                Cursor cCliente = getCFFApp().getDbPrePedido().rawQuery(String.format(new Locale("pt", "BR"), "SELECT * FROM CLIENTE WHERE CLI_SEQ = %d", getData().getExtras().getInt("CLI_SEQ")), null);
                if (cCliente.getCount() > 0) {
                    cCliente.moveToFirst();
                    if (cCliente.getInt(16) != 0 && cCliente.getInt(17) != 0) {
                        getData().putExtra("FOR_PAG_SEQ", cCliente.getInt(16));
                        getData().putExtra("CON_PAG_SEQ", cCliente.getInt(17));
                        GeraPrazoPagamento(String.valueOf(getData().getExtras().getDouble("PVE_VAL_TOT")));
                        getData().putExtra("FOR_PAG_SEQ", 0);
                        getData().putExtra("CON_PAG_SEQ", 0);
                        utiliza_condicao_pre_definida = false;
                        condicaoCliente = true;
                    }
                }
            }

            if(!condicaoCliente) {
                try {
                    if ((!edtRecebValor.getText().toString().equals("") && !edtRecebValor.getText().toString().equals("")) && NumberFormat.getInstance(new Locale("pt", "BR")).parse(edtRecebValor.getText().toString()).doubleValue() > 0.0) {
                        if (!(CarregaFormaPagamento()))
                            Toast.makeText(getActivity(), getString(R.string.err_carga_form_pag), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ignored) {
                }
            }
        }

        if(id == R.id.mni_remover){
            if(MainActivity.preVenda.getSyncStatus() == 2) return false;
            if ( lstFormaPagamento != null ){
                lstFormaPagamento.setAdapter(null);
                edtRecebValor.setText(edtRecebTotal.getText().toString());
                edtFormaPagamento.setText("");
                edtCondPagamento.setText("");
                getData().putExtra("FOR_PAG_SEQ", 0);
                getData().putExtra("CON_PAG_SEQ", 0);
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
