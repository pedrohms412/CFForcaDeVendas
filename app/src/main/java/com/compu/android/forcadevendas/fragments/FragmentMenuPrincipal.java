package com.compu.android.forcadevendas.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.compu.android.adapter.GenericTextObjectAdapter;
import com.compu.android.adapter.ItemAdapter1;
import com.compu.android.forcadevendas.R;
import com.compu.android.fragments.CFFFragment;

public class FragmentMenuPrincipal extends CFFFragment {
	
	private ViewGroup menu;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		initVars();
		
		return menu;
	}

    @Override
    protected void HandleCallMessage(Message msg) {

    }

    private void initVars(){
		
//		menu = (ViewGroup)getLayoutInflater(null).inflate(R.layout.layoutsidemenu, null);
        menu = null;
		
		WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		menu.setLayoutParams(lp);

		ListView menuLayout = new ListView(getActivity().getBaseContext());
		menuLayout =  new ListView(getActivity().getBaseContext());
		menuLayout.setTag("menu");
		ArrayList<GenericTextObjectAdapter> obj = new ArrayList<GenericTextObjectAdapter>();
		obj.add(new GenericTextObjectAdapter(0, getResources().getString(R.string.mnu_carga)));
		obj.add(new GenericTextObjectAdapter(1, getResources().getString(R.string.mnu_sincronizar)));
		
		menuLayout.setAdapter(new ItemAdapter1(getActivity().getBaseContext(), R.layout.layoutsidemenu, obj));
		menuLayout.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
//				ServerTasks tasks = new ServerTasks(getBaseContext());
//				if(arg3 == 0){
//					tasks.execute(0);
//				}
//				if(arg3 == 1){
//					tasks.execute(1);
//				}
			}
		});
		
		menu = new LinearLayout(getActivity().getBaseContext());
		menu.addView(menuLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
	}

	@Override
	public void initComponents() {
		// TODO Auto-generated method stub
		
	}
	
	

}
