package com.compu.android.forcadevendas.fragments;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.compu.android.forcadevendas.R;
import com.compu.android.fragments.CFFFragment;

public class FragmentMainActivity extends CFFFragment {
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragmentmainactivity, container, false);
	}

	@Override
	protected void HandleCallMessage(Message msg) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initComponents() {
		// TODO Auto-generated method stub

	}

}
